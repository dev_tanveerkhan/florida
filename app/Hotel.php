<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    public function location()
	{
    	return $this->belongsTo(Location::class);
	}
	public function subLoca()
	{
    	 return $this->belongsTo(SubLocation::class, 'sublocation');

	}
	public function disneyResort()
	{
    	 return $this->belongsTo(DisneyResort::class, 'disney_resort_id');

	}
	public function hotelDet()
	{
    	return $this->hasOne(HotelDet::class);
	}

}
