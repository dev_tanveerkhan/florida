<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.index');
    }

    /**
     * Show the application change password field.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function managePassword()
    {
        $getData = User::where('id','=',Auth::User()->id)->first();
        return view('admin.managePassword.index',compact('getData'));
    }

    /**
     * Update password field.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function PasswordUpdate(Request $request , User $user)
    {
        $this->validate($request,[
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user->password = Hash::make($request->password);
        $user->update();
        return redirect()->route('admin.manage-password')->with('success','Password Updated Successfullly '); 
    }
}
