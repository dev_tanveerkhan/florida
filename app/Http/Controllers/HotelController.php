<?php

namespace App\Http\Controllers;
use App\DisneyResort;
use App\Hotel;
use App\Location;
use App\SubLocation;
use App\Facility;
use Storage;
use App\HotelDet;
use App\whereToStay;
use App\ExploreFlorida;
use Illuminate\Http\Request;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotel = Hotel::orderBy('id','DESC')->paginate(10);
        return view('admin.hotels.index',compact('hotel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $disneyResortType = DisneyResort::select('title','id')->where('status',1)->get();
        $location = Location::where('status','=',1)->get();
        $sublocation = SubLocation::where('status','=',1)->get();
        $facility = Facility::select('name')->orderBy('id','DESC')->get();
        return view('admin.hotels.create',compact('location','facility','sublocation','disneyResortType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'location_id' =>'required',
            'name' =>'required',
            'image' =>'required',
            'price' =>'required',
            'days' =>'required',
            'saving' =>'required',
            'rating' =>'required',
            'type' =>'required',
            'multi_images' =>'required',
            'description' =>'required',
            'facility' =>'required',
            'nearby' =>'required',
            'map_view' =>'required',
        ]);
        $hotel = new Hotel;
        if($request->hasFile('image')){
            $image_path = $request->file('image')->store('hotel');
            $hotel->image = $image_path;
        }
        $hotel->location_id = $request->location_id;
        $hotel->disney_resort_id = $request->disney_resort_id;
        $hotel->sublocation = $request->sublocation;
        $hotel->name = $request->name;
        $hotel->price = $request->price;
        $hotel->days = $request->days;
        $hotel->saving = $request->saving;
        $hotel->rating = $request->rating;
        $hotel->type = $request->type;
        $hotel->save();

        /*HOTEL DETAILS*/

        if($request->hasFile('multi_images')){
            $multi_images = array();
            foreach ($request->file('multi_images') as $mulimage) {
                $image_path = $mulimage->store('hotel');                
                $multi_images[]=$image_path;
            }
        }
        $getHotelid = Hotel::where('location_id','=',$request->location_id)
                    ->where('name','=',$request->name)->where('price','=',$request->price)
                    ->orderBy('id','DESC')->first();
        $hotelDet = new HotelDet;
        $hotelDet->location_id = $request->location_id;
        $hotelDet->sublocation = $request->sublocation;
        $hotelDet->hotel_id = $getHotelid->id;
        $hotelDet->images = json_encode($multi_images);
        $hotelDet->description = $request->description;
        $hotelDet->facility = json_encode($request->facility);
        $hotelDet->near_by = json_encode($request->nearby);
        $hotelDet->map_view = $request->map_view;
        $hotelDet->departure_date = json_encode($request->departure_date);
        $hotelDet->airport = json_encode($request->airport);
        $hotelDet->board = $request->board;
        $hotelDet->guests = $request->guests;
        $hotelDet->save();
        return redirect()->route('admin.hotels')->with('success','Hotel Add Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function show(Hotel $hotel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function edit(Hotel $hotel)
    {
        $disneyResortType = DisneyResort::select('title','id')->where('status',1)->get();
        $sublocation = SubLocation::where('status','=',1)->get();
        $facility = Facility::select('name')->orderBy('id','DESC')->get();
        $hotelDet = HotelDet::where('hotel_id','=',$hotel->id)->first();
        $location = Location::select('id','name')->where('status','=',1)->get();
        return view('admin.hotels.create',compact('disneyResortType','hotel','location','hotelDet','facility','sublocation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotel $hotel)
    {
        $this->validate($request,[
            'location_id' =>'required',
            'name' =>'required',
            'price' =>'required',
            'days' =>'required',
            'saving' =>'required',
            'type' =>'required',
            'description' =>'required',
            'facility' =>'required',
            'nearby' =>'required',
            'rating' =>'required',
            'map_view' =>'required',
        ]);
        $oldFilename = $hotel->image;
        if($request->hasFile('image')){
            if(isset($oldFilename) && !empty($oldFilename)){
            Storage::delete($oldFilename);
            }
        }
        if($request->hasFile('image')){
            $image_path = $request->file('image')->store('hotel');
            $hotel->image = $image_path;
        }

        $hotel->location_id = $request->location_id;
        $hotel->disney_resort_id = $request->disney_resort_id;
        $hotel->sublocation = $request->sublocation;
        $hotel->name = $request->name;
        $hotel->price = $request->price;
        $hotel->days = $request->days;
        $hotel->saving = $request->saving;
        $hotel->type = $request->type;
        $hotel->rating = $request->rating;
        $hotel->update();

        /*UPDATE HOTEL DETAILS*/

        $aryNewImage = array();
        $hotelDetUpdate =  HotelDet::findOrFail($request->hotelDetId);
        if($request->hasFile('multi_images')) {
            if (!empty($hotelDetUpdate->images)) {
                $aryImage = json_decode($hotelDetUpdate->images);
                foreach ($aryImage as $key => $value) {
                    $aryNewImage[] = $value;
                }            
                foreach ($request->file('multi_images') as $mulimage) {
                    $image_path = $mulimage->store('hotel');                
                    $aryNewImage[]=$image_path;
                }
                $hotelDetUpdate->images = json_encode($aryNewImage);
            }
        }
        $hotelDetUpdate->sublocation = $request->sublocation;
        $hotelDetUpdate->location_id = $request->location_id;
        $hotelDetUpdate->description = $request->description;
        $hotelDetUpdate->facility = json_encode($request->facility);
        $hotelDetUpdate->near_by = json_encode($request->nearby);
        $hotelDetUpdate->map_view = $request->map_view;
        $hotelDetUpdate->departure_date = json_encode($request->departure_date);
        $hotelDetUpdate->airport = json_encode($request->airport);
        $hotelDetUpdate->board = $request->board;
        $hotelDetUpdate->guests = $request->guests;
        $hotelDetUpdate->update();
        return redirect()->route('admin.hotels')->with('success','Hotel Update Successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotel $hotel)
    {
        $hotelDet = HotelDet::where('hotel_id',$hotel->id)->first();
        foreach ($hotelDet->images as $value) {
            Storage::delete($value);
        }
        $hotelDet->delete();
        Storage::delete($hotel->image);
        $hotel->delete();
        return redirect()->route('admin.hotels')->with('success','Hotel Deleted Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hotel  $hotelDet
     * @return \Illuminate\Http\Response
     */
    public function singleImageDestroy(Request $request)
    {
        $aryResponse = array(); 
        $media_id = $request->media_id;
        $media_image = $request->media_image;

        $singleRowData = HotelDet::where('id', $media_id)->first();
        $aryImage = json_decode($singleRowData->images);            
        $aryNewImage = array();
        foreach ($aryImage as $key => $value) {
            if ($value!=$media_image) {
               $aryNewImage[] = $value;
            } else {                
                $image_path = storage_path().$value;
                Storage::delete($image_path);
            }   
        }        
        $mediaData = json_encode($aryNewImage);
        $result = HotelDet::where("id", '=',  $media_id)
        ->update(['images'=> $mediaData]);
        if ($result>0) {
           $aryResponse['status']='success';
        } else {
            $aryResponse['status']='failed';
        }
        echo json_encode($aryResponse);

    }

     /**
     * Change the specified resource from storage.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function status(Hotel $hotel)
    {
        $hotel->status=$hotel->status==1?0:1;
        $hotel->update();
        return redirect()->route('admin.hotels')->with('success','Hotel Status Changed Successfully');
    }

     /**
     * Change the specified resource from storage.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function hotDeals(Hotel $hotel)
    {
        $hotel->hot_deal = $hotel->hot_deal == 1?0:1;
        $hotel->update();
        return redirect()->route('admin.hotels')->with('success','Hot Deals  Status Changed Successfully');
    }

     /**
     * Change the specified resource from storage.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function fav(Hotel $hotel)
    {
        $hotel->fav = $hotel->fav == 1?0:1;
        $hotel->update();
        return redirect()->route('admin.hotels')->with('success','Fav Status Changed Successfully');
    }

     /**
     * Change the specified resource from storage.
     *
     * @param  \App\Hotel  $id
     * @return \Illuminate\Http\Response
     */
    public function viewHotelDet($id)
    {
        /*$info = HotelDet::where('hotel_id','=',$id)->first();
        return view('admin.hotels.hotel-detail',compact($info));*/
    }

    /*======================================================= */
    /*====================EXPLORE FLORIDA==================== */
    /*======================================================= */
     /**
     * View the specified resource from storage.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function exploreFlorida()
    {
        $exploreFlorida = ExploreFlorida::orderBy('id','DESC')->get();
        return view('admin.exploreFlorida.index',compact('exploreFlorida'));
    }
    public function exploreFloridaEdit(ExploreFlorida $id)
    {
        return view('admin.exploreFlorida.edit',compact('id'));
    }
    public function exploreFloridaUpdate(Request $request, ExploreFlorida $id)
    {
        $this->validate($request,[
            'title' =>'required',
            'price' =>'required',
        ]);
        $oldFilename = $id->image;
        if($request->hasFile('image')){
            if(isset($oldFilename) && !empty($oldFilename)){
            Storage::delete($oldFilename);
            }
        }
        if($request->hasFile('image')){
            $image_path = $request->file('image')->store('public/hotel');
            $id->image = $image_path;
        }
        $id->title = $request->title;
        $id->price = $request->price;
        $id->update();
        return redirect()->route('admin.exploreFlorida')->with('success','Explore Florida Updated Successfully');
    }
    public function exploreFloridaStatus(ExploreFlorida $id)
    {
        $id->status = $id->status==1?0:1;
        $id->update();
        return redirect()->route('admin.exploreFlorida')->with('success','Explore Florida Status Changed Successfully');
    }
    public function exploreFloridaCreate()
    {
        return view('admin.exploreFlorida.edit');
    }
    public function exploreFloridaStore(Request $request)
    {   
        $this->validate($request,[
            'title' =>'required',
            'price' =>'required',
            'image' =>'required',
        ]);
        
        if($request->hasFile('image')){
            $image_path = $request->file('image')->store('public/hotel');
        }
        $explorFlorida = new ExploreFlorida;
        $explorFlorida->image = $image_path;
        $explorFlorida->title = $request->title;
        $explorFlorida->price = $request->price;
        $explorFlorida->save();
        return redirect()->route('admin.exploreFlorida')->with('success','Explore Florida Stored Successfully'); 
    }
    public function exploreFloridaDestroy(ExploreFlorida $explor)
    {
        $explor->delete();
        return redirect()->route('admin.exploreFlorida')->with('success','Explore Florida Deleted Successfully'); 
    }


    /*====================================================*/
    /*==============WHEREE TO STAY MANAGE=================*/
    /*====================================================*/
    public function whereToStay()
    {
        $whereToStay = whereToStay::orderBy('id','DESC')->get();
        return view('admin.exploreFlorida.where_to_stay',compact('whereToStay'));
    }
    public function whereToStayCreate()
    {
        $location = Location::select('id','name')->where('base_name','like','%florida%')->where('status','=',1)->get();
        return view('admin.exploreFlorida.where_to_stay_create',compact('location'));
    }
    public function whereToStayStore(Request $request)
    {
        $this->validate($request,[
            'title' =>'required',
            'price' =>'required',
            'image' =>'required',
        ]);
        
        if($request->hasFile('image')){
            $image_path = $request->file('image')->store('public/hotel');
        }
        $explorFlorida = new whereToStay;
        $explorFlorida->image = $image_path;
        $explorFlorida->title = $request->title;
        $explorFlorida->price = $request->price;
        $explorFlorida->save();
        return redirect()->route('admin.whereToStay')->with('success','Where To Stay Stored Successfully'); 
    }


    public function whereToStayEdit(whereToStay $whereTo)
    {
        $location = Location::select('id','name')->where('status','=',1)->get();
        return view('admin.exploreFlorida.where_to_stay_create',compact('whereTo','location'));
    }

    public function whereToStayUpdate(Request $request, whereToStay $whereTo)
    {
        $this->validate($request,[
            'title' =>'required',
            'price' =>'required',
        ]);
        $oldFilename = $whereTo->image;
        if($request->hasFile('image')){
            if(isset($oldFilename) && !empty($oldFilename)){
            Storage::delete($oldFilename);
            }
        }
        if($request->hasFile('image')){
            $image_path = $request->file('image')->store('public/hotel');
            $whereTo->image = $image_path;
        }
        $whereTo->title = $request->title;
        $whereTo->price = $request->price;
        $whereTo->update();
        return redirect()->route('admin.whereToStay')->with('success','Where To Stay Updated Successfully');
    }
    public function whereToStayStatus(whereToStay $whereToStay)
    {
        $whereToStay->status = $whereToStay->status==1?0:1;
        $whereToStay->update();
        return redirect()->route('admin.whereToStay')->with('success','Where To Stay Status Changed Successfully');
    }
    public function whereToStayDestroy(whereToStay $whereToStay)
    {
        Storage::delete($whereToStay->image);
        $whereToStay->delete();
        return redirect()->route('admin.whereToStay')->with('success',' Where To Stay Deleted Successfully');
    }

}
