<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelDetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_dets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('location_id');
            $table->integer('sublocation')->default(0);
            $table->integer('hotel_id');
            $table->text('images');
            $table->text('description');
            $table->text('facility');
            $table->text('near_by');
            $table->text('map_view');
            $table->text('departure_date')->nullable();
            $table->text('airport')->nullable();
            $table->text('board')->nullable();
            $table->text('guests')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1:Active,0:Deactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_dets');
    }
}
