-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 23, 2019 at 09:01 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `holidayflorida`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking_adds`
--

CREATE TABLE `booking_adds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active,0:Deactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `booking_adds`
--

INSERT INTO `booking_adds` (`id`, `title`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Beat the booking frenzy and get a $200 Disney Gift card with each booking!', 'Free Disney Dining & Drinks is super popular and there’s a limited number of special offer rooms across Disneys Resorts. Register your interest with us and book before 3rd July to qualify!', 1, NULL, '2019-06-23 05:40:10');

-- --------------------------------------------------------

--
-- Table structure for table `callbacks`
--

CREATE TABLE `callbacks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1:Approved,0:Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `callbacks`
--

INSERT INTO `callbacks` (`id`, `code`, `number`, `status`, `created_at`, `updated_at`) VALUES
(2, '+353', 874589796768, 0, '2019-06-15 18:30:00', '2019-06-18 12:50:10'),
(3, '+44', 874589796768, 0, '2019-06-15 18:30:00', '2019-06-18 12:50:06');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phone_number` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Deactive,1:Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `phone_number`, `email`, `location`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 97856342, 'info@holidayf', '71-75 Shelton Street, CoventGarden, London, England, WC2H 9JQ', 'Garden, London, England, WC2H 9JQ 71-75 Shelton Street, Covent\r\nGarden, London, England, WC2H 9JQ 71-75 Shelton Street, Covent\r\nGarden, London, England, WC2H 9J', 1, '2019-06-06 18:30:00', '2019-06-15 03:20:13');

-- --------------------------------------------------------

--
-- Table structure for table `disney_resorts`
--

CREATE TABLE `disney_resorts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active,0:Deactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `disney_resorts`
--

INSERT INTO `disney_resorts` (`id`, `title`, `image`, `price`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Disney\'s Caribbean Beach Resort', 'public/disney/aMLsxZoI6KUvjgS92ZyvYYln1KUNocmA46yrFTH8.jpeg', '2333', 'Disney\'s Caribbean Beach Resor   Disney\'s Caribbean Beach Resor  Disney\'s Caribbean Beach Resor  Disney\'s Caribbean Beach Resor  Disney\'s Caribbean Beach Resor  Disney\'s Caribbean Beach Resor  Disney\'s Caribbean Beach Resor  Disney\'s Caribbean Beach Resor  Disney\'s Caribbean Beach Resor', 1, '2019-06-22 07:26:36', '2019-06-22 07:26:36'),
(2, 'Disney\'s Caribbean Beach Resort', 'public/disney/IOvZBfKHZ8RogTZexJWPZrrsp2gzlHRdyaCzhMpK.jpeg', '2333', 'sbvjhf j hfgh g hg g h jt gbg jfdbgjhdfbg gdfg bfbg hgfhhgfgjgjghjg h', 1, '2019-06-22 07:28:24', '2019-06-22 07:28:24'),
(3, 'Disney\'s Caribbean Beach Resort', 'public/disney/cyUB8QPBDnhibfg3phkBKtrCjMlLZHpYpvkmrUqK.jpeg', '2333', 'accascasca', 1, '2019-06-23 02:50:11', '2019-06-23 03:07:59');

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active,0:Deactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Swimming pool', 1, '2019-06-20 12:17:49', '2019-06-20 12:29:47'),
(2, 'Non-smoking rooms', 1, '2019-06-20 12:19:03', '2019-06-20 12:19:03'),
(3, 'Free parking', 1, '2019-06-20 12:19:16', '2019-06-23 05:41:38'),
(4, 'Fitness centre', 1, '2019-06-20 12:19:31', '2019-06-20 12:19:31'),
(5, 'Restaurant', 1, '2019-06-20 12:19:45', '2019-06-20 12:19:45'),
(6, 'Facilities for disabled guests', 1, '2019-06-20 12:19:59', '2019-06-20 12:19:59'),
(8, 'Outdoor pool', 1, '2019-06-20 12:21:32', '2019-06-20 12:21:32'),
(9, 'In-Room Celebrations', 1, '2019-06-20 12:24:18', '2019-06-20 12:24:18'),
(10, 'Disney Bus Transportation', 1, '2019-06-20 12:24:30', '2019-06-20 12:24:30'),
(11, 'Disneys Magical Express', 1, '2019-06-20 12:24:53', '2019-06-23 05:41:34'),
(12, 'Disney Signature Services', 1, '2019-06-20 12:25:09', '2019-06-20 12:25:09'),
(13, 'Currency Exchange', 1, '2019-06-20 12:25:21', '2019-06-20 12:25:21'),
(14, 'Free WiFi', 1, '2019-06-20 12:27:48', '2019-06-23 05:41:31'),
(15, 'Wifi', 1, '2019-06-20 12:27:57', '2019-06-20 12:27:57'),
(16, 'Free Breakfast', 1, '2019-06-20 12:28:30', '2019-06-20 12:30:19');

-- --------------------------------------------------------

--
-- Table structure for table `follow_ons`
--

CREATE TABLE `follow_ons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `facebook` text COLLATE utf8_unicode_ci NOT NULL,
  `instagram` text COLLATE utf8_unicode_ci NOT NULL,
  `tweet` text COLLATE utf8_unicode_ci NOT NULL,
  `pinterest` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `follow_ons`
--

INSERT INTO `follow_ons` (`id`, `facebook`, `instagram`, `tweet`, `pinterest`, `created_at`, `updated_at`) VALUES
(1, 'www.facebook.com', 'www.instagram.com', 'www.tweeter.com', 'www.pinterest.com', '2019-06-15 18:30:00', '2019-06-16 03:50:20');

-- --------------------------------------------------------

--
-- Table structure for table `free_disney_dining_contents`
--

CREATE TABLE `free_disney_dining_contents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active,0:Deactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `free_disney_dining_contents`
--

INSERT INTO `free_disney_dining_contents` (`id`, `title`, `sub_title`, `image`, `url`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Free Disney Quick Service Dining', 'plan & $200 Gift Card when you book 5 Nights or more & Disney Park Tickets at select', 'public/disney/JWie7ET5OkyxZHMYFZIVHMJZnYsBzlbMF9ICYnLn.png', '4', 1, '2019-06-21 18:30:00', '2019-06-23 03:54:01'),
(2, 'Free Disney Dining', 'plan & $200 Gift Card when you book 5 Nights or more & Disney Park Tickets at select', 'public/disney/jIoHkaKepdhbLGlhZ34Bry7IQt8WodivipdjwDma.png', '5', 1, '2019-06-21 18:30:00', '2019-06-23 04:00:44'),
(3, 'Free Breakfast', 'for all when you book 5 nights or more & Disney Tickets at a', 'public/disney/l2SB6b4ed6RI8eUqp8CsaG24Ew4sbRwjfcRbovTr.png', '3', 1, '2019-06-21 18:30:00', '2019-06-23 04:00:42');

-- --------------------------------------------------------

--
-- Table structure for table `free_disney_dining_titals`
--

CREATE TABLE `free_disney_dining_titals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active,0:Deactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `free_disney_dining_titals`
--

INSERT INTO `free_disney_dining_titals` (`id`, `title`, `sub_title`, `image`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Free Disney Dining 2020 is here!', 'Book before 02nd July 2019 and get  FREE Dine + $200 Disney Giftcard', 'public/disney/r7urmwA7fFo0FKUEXlbBHLgx5DcekeOEDrXUAUga.jpeg', 1, NULL, '2019-06-23 03:52:33');

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE `hotels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `location_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `days` int(11) NOT NULL,
  `saving` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Hotels,2:Villas,3:Disney,4:Universal',
  `hot_deal` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Deactive,1:Active',
  `fav` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:not,1:yes',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active,0:Deactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`id`, `location_id`, `name`, `image`, `price`, `days`, `saving`, `type`, `hot_deal`, `fav`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Avanti International Resort', 'public/hotel/v46mQXEt2CHRpe5aoWC3NaPtiSHMw7qLF0XuwiR9.jpeg', 766, 5, 55, 1, 0, 1, 1, '2019-06-15 08:53:38', '2019-06-18 12:47:10'),
(2, 2, 'Avanti International', 'public/hotel/2k4SVCghMDuw1G5LqkWTwUn0L3Bcgh5E7PfPxdIB.jpeg', 342, 3, 22, 2, 0, 1, 1, '2019-06-23 10:08:09', '2019-06-23 10:08:18');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_dets`
--

CREATE TABLE `hotel_dets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `location_id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `images` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `facility` text COLLATE utf8_unicode_ci NOT NULL,
  `near_by` text COLLATE utf8_unicode_ci NOT NULL,
  `map_view` text COLLATE utf8_unicode_ci NOT NULL,
  `departure_date` text COLLATE utf8_unicode_ci,
  `airport` text COLLATE utf8_unicode_ci,
  `board` text COLLATE utf8_unicode_ci,
  `guests` text COLLATE utf8_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active,0:Deactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hotel_dets`
--

INSERT INTO `hotel_dets` (`id`, `location_id`, `hotel_id`, `images`, `description`, `facility`, `near_by`, `map_view`, `departure_date`, `airport`, `board`, `guests`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '[\"public\\/hotel\\/q8TBARLIPvPOqNurpg6uSrNNY8hInxdozRArpQpw.jpeg\",\"public\\/hotel\\/NXI8TbprnsOIn2BrTxHqoif4zYwaBxEkOnh2KxPl.jpeg\",\"public\\/hotel\\/C82WfdD7ogtZYCRmWxXI3hirImPQFuASpXSTSOt5.jpeg\",\"public\\/hotel\\/E8Sk8E5pzzDGs7V6VM2QCE2uIbFS096M4x4ggJY0.jpeg\"]', 'gsregreg r grgr grgreger', '[\"Free Breakfast\",\"Free WiFi\",\"Currency Exchange\",\"Free parking\",\"Non-smoking rooms\",\"Swimming pool\"]', '[\"new\",\"CDSCDSCDS\"]', 'http://localhost:8000/admin/hotels/create', '[\"2019-06-12\",\"2019-06-20\"]', '[\"Airport 1\",\"Airport 2\"]', 'Disney\'s FREE Quick', '02 Adults 02 Children', 1, '2019-06-15 08:53:38', '2019-06-20 12:43:15'),
(2, 2, 2, '[\"public\\/hotel\\/Hs9meQUA1ycKZMQX64OiN4HIbXvMopukBU3Ukw5x.jpeg\",\"public\\/hotel\\/5eNBSMJTucKiFN9AYnNw8UySX7VWGf80MyzURiPZ.jpeg\"]', 'dccce', '[\"Free Breakfast\",\"Free WiFi\",\"Currency Exchange\",\"Disney Signature Services\"]', '[\"gfdgdfgdfgfdgdf\",\"ddsdsdssdfds\"]', 'http://localhost:8000/admin/hotels/create', '[\"2019-06-17\",\"2019-06-09\"]', '[\"new\",\"Airport 1\"]', 'LON', '2 children', 1, '2019-06-23 10:08:09', '2019-06-23 10:08:09');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active,0:Deactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `sub_location`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Orlando', NULL, 'Whether it’s a trip for families, couples, single travelers or a group of friends, International Drive is the perfect destination thanks to incredible diversity that ensures an unforgettable vacation filled with unique experiences for every visitor. For instance, if thrill-seeking is your style, you won’t be able to get enough of our theme parks, water parks and attractions, but there’s a lot more for you to discover, including dining, shopping, ecotourism and more. So, get out and explore it all — there’s no wrong way to experience International Drive!', 1, '2019-06-09 01:17:44', '2019-06-09 01:17:44'),
(2, 'Miami', NULL, NULL, 1, '2019-06-09 01:17:53', '2019-06-09 01:17:53');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_06_07_164217_create_contacts_table', 2),
(10, '2019_06_08_200713_create_locations_table', 4),
(14, '2019_06_15_092010_create_quotes_table', 7),
(15, '2019_06_09_054304_create_hotel_dets_table', 8),
(16, '2019_06_09_053111_create_hotels_table', 9),
(17, '2019_06_16_051837_create_callbacks_table', 10),
(18, '2019_06_16_083143_create_follow_ons_table', 11),
(19, '2019_06_17_151234_create_sliders_table', 12),
(20, '2019_06_20_170216_create_facilities_table', 13),
(21, '2019_06_20_170635_create_facilities_table', 14),
(22, '2019_06_22_054452_create_free_disney_dining_titals_table', 15),
(23, '2019_06_22_054536_create_free_disney_dining_contents_table', 15),
(24, '2019_06_22_085119_create_disney_resorts_table', 16),
(25, '2019_06_23_100401_create_booking_adds_table', 17),
(26, '2019_06_23_113753_create_slider_infos_table', 18);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@gmail.com', '$2y$10$CUWmUqYhj4T0SKhxI7KoW.waCoj2PfWKaWHC0qZsSteSZQRUO/JSi', '2019-06-06 00:52:34'),
('testing85000@gmail.com', '$2y$10$.UtfxE9e9iIXIai1kDm42uSOwGhrWEH.bbLScjl/aue/C2t2W9/Rm', '2019-06-06 01:13:27');

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE `quotes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` bigint(20) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1:enquiry,2:quote',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `viewed` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1:viewed,0:NotViewed',
  `status` tinyint(4) NOT NULL COMMENT '0:Active,1:Deactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active,2:Deactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `image`, `title`, `sub_title`, `start_price`, `hotel_id`, `status`, `created_at`, `updated_at`) VALUES
(3, 'public/slider/0UFwSoKhNxNYszJj3G6e9ioTUCUZcgQQ9M2oT5JR.jpeg', 'Disney\'s Caribbean Beach Resort', 'Disney\'s Caribbean Beach Resort', '7672', 1, 1, '2019-06-17 13:56:13', '2019-06-18 12:49:47'),
(4, 'public/slider/b0N7Qip3FmzaZRJRyEDZDmHOL1ZCYpt0zcqWrmxH.jpeg', 'Hotel Beach Resort', 'Disney\'s Caribbean Beach Resort', '898', 1, 1, '2019-06-21 23:20:54', '2019-06-21 23:20:54');

-- --------------------------------------------------------

--
-- Table structure for table `slider_infos`
--

CREATE TABLE `slider_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Slider Info,2:LoveFlorida',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Active,0:Deactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slider_infos`
--

INSERT INTO `slider_infos` (`id`, `image`, `title`, `description`, `type`, `status`, `created_at`, `updated_at`) VALUES
(2, 'public/slider/IfzynczGddWu6df8VNhGdUrg7UXw8JBtCF6NZhtb.png', 'Book with confidence', 'Your holidays are ATOL & TTA protected backed up by our commitment to making sure you have the best holiday ever.', 2, 1, '2019-06-23 07:39:13', '2019-06-23 08:19:26'),
(3, 'public/slider/rtjphUitTQG23QjvkM6EMmoqnp03Q1LDlLC8a82k.png', 'Florida, your way', 'We have over 10 years experience putting together dream holidays to the sunshine state & beyond.', 2, 1, '2019-06-23 08:11:22', '2019-06-23 08:17:17'),
(4, 'public/slider/fshTYTxS1mrc2kXVMlHMbdrqW5IsYTiJkBkTWOFs.png', 'Safe & Secure booking', 'Your holidays are ATOL & TTA protected backed up with 5 star rated customer service. We have got your back.', 1, 1, '2019-06-23 08:13:24', '2019-06-23 08:13:24'),
(5, 'public/slider/xl7JO3v9GTL8pc4u222WUjlMaoS6X0kBS85cE8r1.png', 'Exceptional Value', 'We are small & mighty and are able to offer highly competative rates you will struggle to find elsewhere', 1, 1, '2019-06-23 08:14:23', '2019-06-23 08:14:23'),
(6, 'public/slider/eoZ8tvSCrecgFHxKIa6Sm4y6sHt4Y62LiDSgd2yW.png', 'For the love of Florida', 'Our friendly Florida Specialists have over 10 years experience putting together dream holidays to the sunshine state & beyond', 1, 1, '2019-06-23 08:15:06', '2019-06-23 08:15:06'),
(7, 'public/slider/VYj9wLxOrK7D1gAxAzSw1cT42S9ydgsFvr2myb4M.png', 'Book now, pay later.', 'Secure your holiday from as little as £75 per person with flexible payment options.', 2, 1, '2019-06-23 08:17:45', '2019-06-23 08:18:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_image` text COLLATE utf8_unicode_ci,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `profile_image`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', NULL, NULL, '$2y$10$G697.gIOxpusy6Xv6ZU98eMj/cLLeFGIosJVJc8ssWegxXSeIoMn2', NULL, '2019-06-06 00:30:05', '2019-06-11 12:09:39'),
(2, 'admin', 'testing85000@gmail.com', NULL, NULL, '$2y$10$gjZPcXlMs9dRRh/EH/KDGOs4gOscRWJ3GFrzl2JHxy2pWJvmYtoLe', NULL, '2019-06-06 00:30:05', '2019-06-06 00:30:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking_adds`
--
ALTER TABLE `booking_adds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `callbacks`
--
ALTER TABLE `callbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `disney_resorts`
--
ALTER TABLE `disney_resorts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follow_ons`
--
ALTER TABLE `follow_ons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `free_disney_dining_contents`
--
ALTER TABLE `free_disney_dining_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `free_disney_dining_titals`
--
ALTER TABLE `free_disney_dining_titals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotels`
--
ALTER TABLE `hotels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel_dets`
--
ALTER TABLE `hotel_dets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `quotes`
--
ALTER TABLE `quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_infos`
--
ALTER TABLE `slider_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking_adds`
--
ALTER TABLE `booking_adds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `callbacks`
--
ALTER TABLE `callbacks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `disney_resorts`
--
ALTER TABLE `disney_resorts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `follow_ons`
--
ALTER TABLE `follow_ons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `free_disney_dining_contents`
--
ALTER TABLE `free_disney_dining_contents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `free_disney_dining_titals`
--
ALTER TABLE `free_disney_dining_titals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `hotels`
--
ALTER TABLE `hotels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `hotel_dets`
--
ALTER TABLE `hotel_dets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `quotes`
--
ALTER TABLE `quotes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `slider_infos`
--
ALTER TABLE `slider_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
