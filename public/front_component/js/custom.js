$(window).on("load",function(){
    $('.preloader-wrapper').fadeOut();
    $('.preloader-wrapper').addClass('hide');
});

$(document).ready(function() {
  $('.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    items:3,
    nav:true,
    dots: false,
    mergeFit:true,
    navText:["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
    responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 2,
          nav: true
        },
        1200: {
          items: 3,
          nav: true,
          loop: false,
          margin: 20
        }
      }
  });
});

$('.owl-card-img-slider').owlCarousel({
    items:1,
    lazyLoad:true,
    loop:true,
    margin:10,
    dots: false,
    nav: true,
    navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
    
});
jQuery(document).ready(function() {

// Tooltips Initialization
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});

//Bottum to top button  
  var btn = $('#bottim-to-top-btn');

  $(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
      btn.addClass('show');
    } else {
      btn.removeClass('show');
    }
  });

  btn.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({scrollTop:0}, '300');
  });  
});


$(document).ready(function () {
      $("#datepicker").datepicker({
          minDate: 0
      });
  });

