@extends('admin.layout.admin-base')
@section('title', 'add hotel')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    {{-- ==================================================== --}}
    {{-- =====================EDIT HOTELS==================== --}}
    @if(!empty($hotel))
      <section class="content-header">
        <h1>
          Edit Hotel
          <small><i class="fa fa-Hotel"></i></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Edit Hotel</li>
        </ol>
      </section>
       <!-- Main content -->
      <section class="content">
        {{-- ====error message=== --}}
        @if ($errors->any())
          @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ $error }}
            </div>
          @endforeach
        @endif
        {{-- ====error message=== --}}
        <div class="row">
          <form class="form-horizontal image-form" action="{{route('admin.hotel.edit',$hotelDet->id)}}" method="post" enctype="multipart/form-data">
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Hotel</h3>
            </div>
            <div class="box-body">
              {{ csrf_field() }}
              <input type="hidden" name="hotelDetId" value="{{$hotelDet->id}}">
              <div class="form-group">
                <label class="col-sm-2 control-label">Hotel Location</label>
                <div class="col-sm-10">
                  <select class="form-control" name="location_id" required>
                    <option value="{{$hotel->location_id}}" selected>{{$hotel->location->name}}</option>
                    @foreach($location as $result)
                      <option value="{{$result->id}}">{{$result->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Hotel Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" value="{{$hotel->name}}" name="name">
                </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label">Hotel Sub-Location</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="sublocation">
                      <option value="{{$hotel->sublocation}}">@if(!empty($hotel->subLoca->name)){{$hotel->subLoca->name}} @else Choose...@endif</option>
                      @foreach($sublocation as $result)
                        <option value="{{$result->id}}">{{$result->name}}</option>
                      @endforeach
                    </select>
                  </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Image</label>
                <div class="col-sm-8">
                  <input type="file" class="form-control" name="image">
                </div>
                <div class="col-sm-2">
                  <img class="img-thumbnail" src="{{asset('storage/'.$hotel->image)}}">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Price</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" value="{{$hotel->price}}" name="price">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Days</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" value="{{$hotel->days}}" name="days">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Set Saving Amount</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" value="{{$hotel->saving}}" name="saving">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Hotel Type</label>
                <div class="col-sm-10">
                  <select class="form-control hotel_type" name="type">
                    <option value="{{$hotel->type}}" selected>
                      @if($hotel->type==1) Hotels 
                      @elseif($hotel->type==2) Villas 
                      @elseif($hotel->type==3) Disney 
                      @elseif($hotel->type==4) Universal 
                      @endif </option>
                    <option value="1">Hotels</option>
                    <option value="2">Villas</option>
                    <option value="3">Disney</option>
                    <option value="4">Universal</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label">Hotel Rating</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="rating" value="{{old('type')}}" required>
                      @if(!empty($hotel->rating))
                      <option value="{{$hotel->rating}}">{{$hotel->rating}} Star</option>
                      @else
                      <option value="">Choose...</option>
                      @endif
                      <option value="1">1 Star</option>
                      <option value="2">2 Star</option>
                      <option value="3">3 Star</option>
                      <option value="4">4 Star</option>
                      <option value="5">5 Star</option>
                    </select>
                  </div>
                </div>
              <div class="form-group disneyResort" style="display: none;">
                <label class="col-sm-2 control-label">Select Disney Resort Type</label>
                <div class="col-sm-10">
                  <select class="form-control" name="disney_resort_id">
                    @if($hotel->disney_resort_id!=0)
                    <option value="{{$hotel->disney_resort_id}}" selected>{{$hotel->disneyResort->title}}</option> @endif
                    @foreach($disneyResortType as $drtData)
                      <option value="{{$drtData->id}}">{{$drtData->title}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>

          {{-- ===============EDIT HOTELS DETAILS================ --}}
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Hotel Details</h3>
            </div>
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Descripton</label> 
                  <div class="col-sm-10">
                    <textarea class="form-control" name="description" rows="4" placeholder="Enter ...">
                      {{$hotelDet->description}}
                    </textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Facilities</label>
                  <div class="col-sm-10">
                    <select class="form-control select2" name="facility[]" multiple="multiple" data-placeholder="Select Facility">
                      @php
                       $afacility=json_decode($hotelDet->facility);
                      @endphp
                      @foreach($afacility as $allfaciliy)
                      <option value="{{$allfaciliy}}" selected>{{$allfaciliy}}</option>
                      @endforeach
                      @foreach($facility as $faciResult)
                        <option value="{{$faciResult->name}}">{{$faciResult->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Board</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$hotelDet->board}}" placeholder="Enter Board" name="board">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Guests</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$hotelDet->guests}}" placeholder="Enter Guests" name="guests">
                  </div>
                </div>

                <div class="form-group results-div">
                  @php
                    $aNearby=json_decode($hotelDet->near_by);
                  @endphp
                  @if(!empty($aNearby))
                  @foreach($aNearby as $key=>$allplace)
                  @if($key ==0)<label class="col-sm-2 control-label">Add Nearby Place</label>@endif
                  <div class="remove-div">
                    <div class="@if($key ==0) @else col-sm-offset-2 @endif col-sm-8">
                      <input type="text" name="nearby[]" value="{{$allplace}}" class="form-control" placeholder="Add Nearby Place With KM">
                    </div>
                    <div class="col-sm-2">
                      <button type="button" class="btn btn-danger remove" onclick="remove($(this))">X</button>
                    </div>
                  </div>
                  @endforeach
                  @endif
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Add More Nearby Place</label>
                  <div class="col-sm-2">
                    <button type="button" class="btn btn-primary add-more" onclick="moreuniversity($(this))">Add More NearBy Place</button>
                  </div><br>
                </div>

                  @php
                    $aDepartureDate=json_decode($hotelDet->departure_date);
                    @endphp
                <div class="form-group dept-date">
                  @if(!empty($aDepartureDate))
                  @forelse($aDepartureDate as $key=>$dd)
                    @if($key ==0)<label class="col-sm-2 control-label">Departure Date</label>@endif
                  <div class="remove-dept">
                    <div class="@if($key ==0) @else col-sm-offset-2 @endif col-sm-8">
                      <input type="date" name="departure_date[]" value="{{$dd}}" class="form-control add-dept" placeholder="Enter Departure Date">
                    </div>
                     <div class="col-sm-2" >
                      <button type="button" class="btn btn-danger remove-dept" onclick="removeDept($(this))">X</button>
                    </div>
                  </div>
                  @empty
                  @endforelse
                  @endif
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Add Departure Date</label>
                    <div class="col-sm-2">
                      <button type="button" class="btn btn-primary add-more-dept" onclick="depatureDate($(this))">Add More Departure Date</button>
                    </div><br><br>
                </div>
                
                @if(!empty($hotelDet->airport))
                <div class="form-group airport-div">
                  @php
                    $aAirport=json_decode($hotelDet->airport);
                  @endphp
                  @if(!empty($aAirport))
                  @foreach($aAirport as $key=>$air)
                    @if($key ==0)<label class="col-sm-2 control-label">Airport</label>@endif
                  <div class="remove-airport">
                    <div class="@if($key ==0) @else col-sm-offset-2 @endif col-sm-8">
                      <input type="text" name="airport[]" value="{{$air}}" class="form-control add-airport" placeholder="Enter Airport Name">
                    </div>
                     <div class="col-sm-2" >
                      <button type="button" class="btn btn-danger remove-dept" onclick="removeAirport($(this))">X</button>
                    </div>
                  </div>
                  @endforeach
                  @endif
                </div>
                @endif
                <div class="form-group">
                  <label class="col-sm-2 control-label">Add More Airport</label>
                    <div class="col-sm-2">
                      <button type="button" class="btn btn-primary add-more-airport" onclick="airport($(this))">Add More Airport</button>
                    </div><br><br>
                </div>



                <div class="form-group">
                  <label class="col-sm-2 control-label">Map Url</label>
                  <div class="col-sm-10">
                    <input type="url" value="{{$hotelDet->map_view}}" class="form-control" placeholder="Enter Map Url" name="map_view">
                  </div>
                </div>
                {{-- IMAGE SHOWING --}}
                <div class="form-group">
                  <label class="col-sm-2 control-label">Multiple Images</label>
                  <div class="col-sm-10">
                    <input type="file" class="form-control" placeholder="Upload Image" name="multi_images[]" multiple>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">
                    {{ __('UPDATE') }}
                </button>
              </div>
            </div>    
        </form>
      </div>


      <div class="box box-warning">
        <div class="box-body">
              <div class="row image-box"> 
                <div class="col-md-12">
                  <div class="alert alert-success hide" id="image-msg">
                    <p><i class="icon fa fa-check"></i> Image removed successfully ...</p>
                  </div>
                </div>                  
                  @php ($aImageUrl=json_decode($hotelDet->images))
                  @foreach($aImageUrl as $allImage)
                  <div class="col-sm-2 center image-main-div">
                    <form class="image-form" method="post" action="{{route('singleImageDestroy')}}">
                      {{csrf_field()}}
                      <input type="hidden" name="media_id" value="{{$hotelDet->id}}">
                      <input type="hidden" name="media_image" value="{{$allImage}}">
                    <p class="cross-img-btn">               
                      <button type="button" class="btn btn-danger delete-image" title="Remove">×</button>
                    </p>
                  <?php $imagePath = storage_path().$allImage ?>
                        @if(File::exists($imagePath))                   
                            <img src="{{ asset('storage/'.$allImage) }}" class="img img-responsive img-thumbnail">
                        @else
                            <img src="{{ asset('storage/'.$allImage) }}" class="img img-responsive img-thumbnail"> 
                        @endif 
                    </form>
                  </div>
                @endforeach
              </div>
          </div>    
        </div>
    </section>

    {{-- ==================================================== --}}
    {{-- ====================ADD HOTELS===================== --}}
    @else
      <section class="content-header">
        <h1>
          Add Hotel
          <small><i class="fa fa-Hotel"></i></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Add Hotel</li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content">
        {{-- ====error message=== --}}
        @if ($errors->any())
          @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ $error }}
            </div>
          @endforeach
        @endif
        {{-- ====error message=== --}}
        <div class="row">
          <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Add Hotel</h3>
            </div>
              <div class="box-body">
                {{ csrf_field() }}
                <div class="form-group">
                  <label class="col-sm-2 control-label">Hotel Location</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="location_id" required>
                      <option value="">Choose...</option>
                      @foreach($location as $result)
                        <option value="{{$result->id}}">{{$result->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Hotel Sub-Location</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="sublocation">
                      <option value="">Choose...</option>
                      @foreach($sublocation as $result)
                        <option value="{{$result->id}}">{{$result->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Hotel Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{old('name')}}" placeholder="For Ex: Villas of Grand Cypress" name="name">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Image</label>
                  <div class="col-sm-10">
                    <input type="file" class="form-control" placeholder="Upload Image" value="{{old('image')}}" name="image">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Price</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="Hotel Price" value="{{old('price')}}" name="price">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Days</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" value="{{old('days')}}" placeholder="Hotel Days" name="days">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Set Save Amount</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" value="{{old('saving')}}" placeholder="For Ex:- You Save £89" name="saving">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Hotel Type</label>
                  <div class="col-sm-10">
                    <select class="form-control hotel_type" name="type" value="{{old('type')}}">
                      <option value="">Choose...</option>
                      <option value="1">Hotels</option>
                      <option value="2">Villas</option>
                      <option value="3">Disney</option>
                      <option value="4">Universal</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Hotel Rating</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="rating" value="{{old('type')}}" required>
                      <option value="">Choose...</option>
                      <option value="1">1 Star</option>
                      <option value="2">2 Star</option>
                      <option value="3">3 Star</option>
                      <option value="4">4 Star</option>
                      <option value="5">5 Star</option>
                    </select>
                  </div>
                </div>
                <div class="form-group disneyResort" style="display: none;">
                  <label class="col-sm-2 control-label">Select Disney Resort Type</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="disney_resort_id">
                      <option value="">~~Select Disney Resort~~</option>
                      @foreach($disneyResortType as $drtData)
                        <option value="{{$drtData->id}}">{{$drtData->title}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                </div>
              </div>

          {{-- ===============ADD HOTELS DETAILS================ --}}
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Add Hotel Details</h3>
            </div>
              <div class="box-body">
                {{ csrf_field() }}
                
                <div class="form-group">
                  <label class="col-sm-2 control-label">Select Multiple Image</label>
                  <div class="col-sm-10">
                    <input type="file" class="form-control" placeholder="Upload Image" value="{{old('multi_images[]')}}" name="multi_images[]" multiple>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Descripton</label> 
                  <div class="col-sm-10">
                    <textarea class="form-control" name="description" rows="4" placeholder="Enter ...">{{old('description')}}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Facilities</label>
                  <div class="col-sm-10">
                    <select class="form-control select2" value="{{old('facility[]')}}" name="facility[]" multiple="multiple" data-placeholder="Select Facility">
                      @foreach($facility as $faciResult)
                        <option value="{{$faciResult->name}}">{{$faciResult->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Map View</label>
                  <div class="col-sm-10">
                    <input type="url" class="form-control" value="{{old('map_view')}}" placeholder="Enter Map Url" name="map_view">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Board</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{old('board')}}" placeholder="Enter Board" name="board">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Guests</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{old('guests')}}" placeholder="Enter Guests" name="guests">
                  </div>
                </div>
                <div class="form-group results-div">
                  <label class="col-sm-2 control-label">Add Nearby Place</label>
                  <div class="col-sm-8">
                    <input type="text" name="nearby[]" value="{{old('nearby[]')}}" class="form-control add-form" placeholder="Add Nearby Palce with Km">
                  </div>
                  <div class="col-sm-2">
                    <button type="button" class="btn btn-primary add-more" onclick="moreuniversity($(this))">Add More</button>
                  </div><br><br>
                </div>
                <div class="form-group dept-date">
                  <label class="col-sm-2 control-label">Depature Date</label>
                  <div class="col-sm-8">
                    <input type="date" name="departure_date[]" value="{{old('departure_date[]')}}" class="form-control add-dept" placeholder="Enter Departure Date">
                  </div>
                  <div class="col-sm-2">
                    <button type="button" class="btn btn-primary add-more-dept" onclick="depatureDate($(this))">Add More</button>
                  </div><br><br>
                </div>
                <div class="form-group airport-div">
                  <label class="col-sm-2 control-label">Airport</label>
                  <div class="col-sm-8">
                    <input type="text" name="airport[]" value="{{old('airport[]')}}" class="form-control add-airport" placeholder="Enter Airport Name">
                  </div>
                  <div class="col-sm-2">
                    <button type="button" class="btn btn-primary add-more-airport" onclick="airport($(this))">Add More</button>
                  </div><br><br>
                </div>

              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">
                    {{ __('ADD NEW') }}
                </button>
              </div>
            </div>
          </form>
        </div>
      </section>
    @endif
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script>
    function moreuniversity(obj) {    
    $(".results-div").append('<div class="remove-div"><div class="col-sm-8 col-sm-offset-2"><input type="text" name="nearby[]" class="form-control" placeholder="Add Nearby Place With KM"></div><div class="col-sm-2" ><button type="button" class="btn btn-danger remove" onclick="remove($(this))">X</button></div></div>');  
    }
    function remove(obj) {
      obj.parents('div.remove-div').remove();
    }

    function depatureDate(obj) {    
    $(".dept-date").append('<div class="remove-dept"><div class="col-sm-8 col-sm-offset-2"><input type="date" name="departure_date[]" class="form-control" placeholder="Enter Departure Date"></div><div class="col-sm-2" ><button type="button" class="btn btn-danger remove-dept" onclick="removeDept($(this))">X</button></div></div>');  
    }
    function removeDept(obj) {
      obj.parents('div.remove-dept').remove();
    }

    function airport(obj) {    
    $(".airport-div").append('<div class="remove-airport"><div class="col-sm-8 col-sm-offset-2"><input type="text" name="airport[]" class="form-control" placeholder="Enter Airport Name"></div><div class="col-sm-2" ><button type="button" class="btn btn-danger remove-dept" onclick="removeAirport($(this))">X</button></div></div>');  
    }
    function removeAirport(obj) {
      obj.parents('div.remove-airport').remove();
    }
  </script>

@endsection