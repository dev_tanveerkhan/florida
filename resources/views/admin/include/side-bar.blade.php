@php
  use App\Quote;
  use App\Hotel;
  $notViewList = Quote::where('enquiry_type',2)->where('viewed','=',0)->count();
  $countHotel = Hotel::count();
@endphp  

  <!-- Left side column. contains the logo and sidebar -->

  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="{{ (Request::is('admin') ? 'active' : '') }}">
          <a href="{{ route('admin.home') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="{{ (Request::is('admin/manage-password') ? 'active' : '') }} {{ (Request::is('admin/manage-password/*') ? 'active' : '') }}">
          <a href="{{route('admin.manage-password')}}">
            <i class="fa fa-user-secret"></i>
            <span>Manage Password</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right"><i class="fa fa-key"></i></span>
            </span>
          </a>
        </li>
        <li class="{{ (Request::is('admin/location') ? 'active' : '')}} {{(Request::is('admin/location/*') ? 'active' : '')}} treeview">
          <a href="#">
            <i class="fa fa-location-arrow"></i>
            <span>Location's</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">2</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ (Request::is('admin/location') ? 'active' : '')}}"><a href="{{route('admin.location')}}"><i class="fa fa-circle-o"></i> Locaton's</a></li>
            <li class="{{ (Request::is('admin/location/sub') ? 'active' : '')}}"><a href="{{route('admin.subLocation')}}"><i class="fa fa-circle-o"></i> Sub Location's</a></li>
          </ul>
        </li>
        <li class="{{ (Request::is('admin/hotels') ? 'active' : '') }} {{ (Request::is('admin/hotels/*') ? 'active' : '') }}">
          <a href="{{route('admin.hotels')}}">
            <i class="fa fa-h-square"></i>
            <span>Hotels</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">{{$countHotel}}
              </span>
            </span>
          </a>
        </li>
        <li class="{{ (Request::is('admin/mix-it-up') ? 'active' : '') }} {{ (Request::is('admin/mix-it-up/*') ? 'active' : '') }}">
          <a href="{{route('admin.mixItUp')}}">
            <i class="fa fa-superpowers"></i>
            <span>Mix It Up</span>
            <span class="pull-right-container"></span>
          </a>
        </li>
        <li class="{{ (Request::is('admin/contact-info') ? 'active' : '') }} {{ (Request::is('admin/contact-info/*') ? 'active' : '') }}">
          <a href="{{route('admin.contactus')}}">
            <i class="fa fa-location-arrow"></i>
            <span>Update Contact Info</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="{{ (Request::is('admin/quote') ? 'active' : '') }} {{ (Request::is('admin/quote/*') ? 'active' : '') }}">
          <a href="{{route('admin.quote')}}">
            <i class="fa fa-envelope"></i>
            <span>Manage Quote</span>
            <span class="pull-right-container">
              <span class="label label-danger pull-right">{{$notViewList}}</span>
            </span>
          </a>
        </li>
        <li class="{{ (Request::is('admin/call-back') ? 'active' : '') }} {{ (Request::is('admin/call-back/*') ? 'active' : '') }}">
          <a href="{{route('admin.callBack')}}">
            <i class="fa fa-phone"></i>
            <span>Call Back</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="{{ (Request::is('admin/follow-on') ? 'active' : '') }} {{ (Request::is('admin/follow-on/*') ? 'active' : '') }}">
          <a href="{{route('admin.follow-on')}}">
            <i class="fa fa-share"></i>
            <span>Follow On</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="{{ (Request::is('admin/slider') ? 'active' : '') }} {{ (Request::is('admin/slider/*') ? 'active' : '') }}">
          <a href="{{route('admin.slider')}}">
            <i class="fa fa-picture-o"></i>
            <span>Slider</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="{{ (Request::is('admin/facility') ? 'active' : '') }} {{ (Request::is('admin/facility/*') ? 'active' : '') }}">
          <a href="{{route('admin.facility')}}">
            <i class="fa fa-handshake-o"></i>
            <span>Hotel Facility</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="{{ (Request::is('admin/free-disney-dining') ? 'active' : '') }} {{ (Request::is('admin/free-disney-dining/*') ? 'active' : '') }}">
          <a href="{{route('admin.freeDisneyDining')}}">
            <i class="fa fa-cutlery"></i>
            <span>Free Disney Dining</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="{{ (Request::is('admin/disney-resort') ? 'active' : '') }} {{ (Request::is('admin/disney-resort/*') ? 'active' : '') }}">
          <a href="{{route('admin.disneyResort')}}">
            <i class="fa fa-fort-awesome"></i>
            <span>Disney Resort</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>       
        <li class="{{ (Request::is('admin/book-ad') ? 'active' : '') }} {{ (Request::is('admin/book-ad/*') ? 'active' : '') }}">
          <a href="{{route('admin.bookingAdd')}}">
            <i class="fa fa-telegram"></i>
            <span>Booking Ad</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="{{ (Request::is('admin/slider-info') ? 'active' : '') }} {{ (Request::is('admin/slider-info/*') ? 'active' : '') }}">
          <a href="{{route('admin.sliderinfo')}}">
            <i class="fa fa-sliders"></i>
            <span>Static Information</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="{{ (Request::is('admin/explore-florida') ? 'active' : '') }} {{ (Request::is('admin/explore-florida/*') ? 'active' : '') }}">
          <a href="{{route('admin.exploreFlorida')}}">
            <i class="fa fa-picture-o"></i>
            <span>Explore Florida</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="{{ (Request::is('admin/where-to-stay') ? 'active' : '') }} {{ (Request::is('admin/where-to-stay/*') ? 'active' : '') }}">
          <a href="{{route('admin.whereToStay')}}">
            <i class="fa fa-home"></i>
            <span>Where To Stay</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>

        <li class="{{ (Request::is('admin/parks-passes') ? 'active' : '') }} {{ (Request::is('admin/parks-passes/*') ? 'active' : '') }}">
          <a href="{{route('admin.parksPasses')}}">
            <i class="fa fa-ticket"></i>
            <span>Parks And Passes</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>

        <li class="{{ (Request::is('admin/static-pages') ? 'active' : '') }} {{ (Request::is('admin/static-pages/*') ? 'active' : '') }}">
          <a href="{{route('admin.staticPages')}}">
            <i class="fa fa-file-text"></i>
            <span>Static Pages</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>

        <li class="{{ (Request::is('admin/offers') ? 'active' : '') }} {{ (Request::is('admin/offers/*') ? 'active' : '') }}">
          <a href="{{route('admin.offer')}}">
            <i class="fa fa-gift"></i>
            <span>Offers</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>