@extends('admin.layout.admin-base')
@section('title', 'Home Page')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section><br>
    <section class="content">
    <div class="callout callout-success" style="padding: 20px;">
      <h4><i class="fa fa-dashboard"></i> Welcome To The Admin Panel</h4>
    </div>
    <!-- Main content -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection