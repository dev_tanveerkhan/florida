@php
	use App\FollowOn;
	use App\Contact;
	$followon = FollowOn::first();
	$contactInfo = Contact::where('status',1)->first();
@endphp
<!--Footer--><br><br>

	<footer class="container page-footer special-color-dark darken-3">
		<!-- Footer Elements -->
		<h3 class="text-center text-white mr-auto p-3 h5">Need help? We’re only a call away - {{$contactInfo->phone_number}}</h3>
		<div class="row">
			<div class="col-md-4 mx-auto">
			    <h5 class="text-strong text-uppercase mt-3 mb-4"><a href="{{url('/')}}"><img class="img-fluid" src="{{asset('img/log.png')}}" alt="logo-image" style="height: 50px;"></a></h5>
			    <p>
			    	{{$contactInfo->phone_number}} ,<a href="mailto:info{{$contactInfo->email}}">{{$contactInfo->email}}</a><br>{{$contactInfo->location}}
				</p>
			</div>
			<hr class="clearfix w-100 d-md-none">
			<div class="col-md-2 mx-auto">
			    <h5 class="text-strong footer-links text-uppercase mt-3 mb-4">Links</h5>
			    <ul class="list-unstyled">	
			      <li>
			        <a href="{{route('aboutUs')}}">About Us</a>
			      </li>
			      <li>
			        <a href="{{route('termsConditions')}}">Terms & Conditions</a>
			      </li>
			      <li>
			        <a href="{{route('contactUs')}}">Contact Us</a>
			      </li>
			      <li>
			        <a href="{{route('privacyPolicy')}}">Privacy Policy</a>
			      </li>
			    </ul>
			</div>
			<hr class="clearfix w-100 d-md-none">

			<div class="col-md-2 mx-auto">
			<!-- Links -->
			    <h5 class="text-strong footer-links text-uppercase mt-3 mb-4">Links</h5>
			    <ul class="list-unstyled">
			        <li>
			          <a href="javascript::void(0)" data-toggle="modal" data-target="#fcm-getQuote">Get a Quote</a>
			        </li>
			        <li>
			          <a href="{{route('showAllExploreFlorida')}}">Explore Florida</a>
			        </li>
			        <li>
			          <a href="{{route('universalHoliday')}}">Universal Holidays</a>
			        </li>
			        <li>
			          <a href="{{route('disneyHoidays')}}">Disney Holidays</a>
			        </li>
			    </ul>
			</div>

			<hr class="clearfix w-100 d-md-none">
			<div class="col-md-2 mx-auto">
			<!-- Links -->
			<h5 class="text-strong footer-links text-uppercase mt-3 mb-4">Links</h5>
			      <ul class="list-unstyled">
			      	<li>
			          <a href="{{route('showAllHotels')}}">Florida Hotels</a>
			        </li>
			        <li>
			          <a href="{{route('showAllExploreFlorida')}}">Rest of USA</a>
			        </li>
			        <li>
			          <a href="{{route('showAllHotDeal')}}">Hot Deals</a>
			        </li>
			        <li>
			          <a href="{{route('showAllParksTicket')}}">Parks & Tickets</a>
			        </li>
			        <li>
			          <a href="{{route('showAllVillas')}}">Florida Villas</a>
			        </li>
			      </ul>
			</div>
		</div>
		<!-- Footer Links -->
		<div class="row">
			<div class="col-md-12 py-2">
				<div class="mb-5 flex-center">
					<a class="fb-ic" href="{{$followon->facebook}}" target="_blank">
						<i class="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
					</a>
					<a class="tw-ic" href="{{$followon->twitter}}" target="_blank">
						<i class="fab fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
					</a>
					<a class="gplus-ic" href="{{$followon->pinterrest}}" target="_blank">
						<i class="fab fa-google-plus-g fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
					</a>
					<a class="ins-ic" href="{{$followon->instagram}}" target="_blank">
						<i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
					</a>
				</div>
			</div>
		</div>
		<!-- Footer Elements -->
		<!-- Copyright -->
		<div class="footer-copyright text-center py-3">© 2020 Copyright:
			<a href="http://www.floridaCalling.co.uk"> FloridaCalling.co.uk </a>
		</div>
		<!-- Copyright -->
	</footer>
	<!--Footer-->
@include('pages.models.models')