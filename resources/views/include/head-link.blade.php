<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Florida Calling|@yield('title')</title>
	<!-- MDB icon -->
	<link rel="icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
	<!-- Select 2 -->
	<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
	<!-- Google Fonts Roboto -->
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('front_component/css/bootstrap.min.css')}}">
	<!-- Material Design Bootstrap -->
	<link rel="stylesheet" href="{{asset('front_component/css/mdb.min.css')}}">
	<!-- Daetpiker -->
	<link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="Stylesheet"
        type="text/css" />
	<!-- Custom -->
	<link rel="stylesheet" type="text/css" href="{{asset('front_component/css/custom.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('front_component/css/owl.carosel.css')}}">
	<a href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/grabbing.png"></a>
	<!-- Media Query -->
	<link rel="stylesheet" type="text/css" href="{{asset('front_component/css/media_query.css')}}">
</head>