@php
	use App\Location;
	use App\SubLocation;
	use App\Contact;
	use App\FollowOn;
	$followon = FollowOn::first();
	$contactInfo = Contact::where('status',1)->first();
	$usaLoc = Location::where('base_name','like','%usa%')->where('status',1)->get();
	$floLoc = Location::where('base_name','like','%florida%')->where('status',1)->get();
	$SubLoc = SubLocation::where('status',1)->get();
@endphp

<header>
	<a id="bottim-to-top-btn"><i class="fa fa-chevron-up fa-2x text-white flex-center"></i></a>
	<div class="container" id="top-section">
		<div class="row top-head-content text-center pt-2 pb-2">
			<div class="col-md-3 logo text-center">
				<a href="{{asset('/')}}"> <img src="{{asset('img/log.png')}}" alt="logo-image" class="img-fluid"></a>
			</div>
			<div class="col-md-6 links text-center text-white">
				<!--Google +-->
				<a class="btn-floating btn-lg btn-danger" href="{{$followon->pinterest}}"><i class="fab fa-google-plus-g"></i></a>
				<!--Facebook-->
				<a class="btn-floating btn-lg btn-primary" href="{{$followon->facebook}}"><i class="fab fa-facebook-f"></i></a>
				<!--Twitter-->
				<a class="btn-floating btn-lg btn-danger" href="{{$followon->instagram}}"><i class="fab fa-instagram"></i></a>
				<!--Instagram-->
				<a class="btn-floating btn-lg btn-info" href="{{$followon->twitter}}"><i class="fab fa-twitter"></i></a>
				<!--Linkedin-->
				<a class="btn-floating btn-lg btn-warning" href="{{route('getAquote')}}"><i class="fa fa-envelope"></i> Get a Quote</a>
			</div>
			<div class="col-md-3 flex-center phone-call-time"  data-toggle="tooltip" data-placement="bottom" title="Monday-Sunday: 9AM-9PM">
				<a class="text-center mr-2"><img src="{{asset('front_component/image/phone-call.png')}}" style="height: auto;width: 45px;">
				</a>
				<span class="h5">{{$contactInfo->phone_number}}</span>
				
				{{-- 
				<div class="row">	
					<div class="col-md-4 head-phone-call">
						<a class="btn-floating btn-lg bg-color text-white"><i class="fas fa-phone-volume"></i></a>
					</div>
					<div class="col-md-8 head-call-span text-center">
						<span class="head-call strong-title">Call us 24x7 on <br> {{$contactInfo->phone_number}}</span>
					</div>
				</div> --}}
			</div>
		</div>
		<!--Navbar-->
		<nav class="navbar navbar-expand-lg navbar-dark nav-color border-round">
		  	<!-- Navbar brand -->
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
		      aria-expanded="false" aria-label="Toggle navigation">
		      	<span class="navbar-toggler-icon"></span>
		  	</button>
		  	<!-- Collapsible content -->
		  	<div class="collapse navbar-collapse" id="basicExampleNav">
		      	<!-- Links -->
		      	<ul class="navbar-nav mr-auto">
		      		<li class="nav-item">
		              	<a class="nav-link" href="{{route('showAllHotels')}}">Hotels</a>
		      		</li>
		          	<li class="nav-item dropdown">
		              	<a class="nav-link dropdown-toggle" id="navbarDropdown1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Disney World</a>
		              	<div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdown1">
		                  	<a class="dropdown-item" href="{{route('disneyHoidays')}}">Disney Hotels-List</a>
		                  	<a class="dropdown-item" href="{{route('parksTickets',['name'=>"disney"])}}">Park & Ticket</a>
		                  	<a class="dropdown-item" href="{{route('otherOffers',['name'=>'disney'])}}">Others Attractions</a>
		              	</div>
		          	</li>
		          	<li class="nav-item dropdown">
		              	<a class="nav-link dropdown-toggle" id="navbarDropdown1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Universal holidays</a>
		              	<div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdown1">
		                  	<a class="dropdown-item" href="{{route('universalHoliday')}}">Universal Hotels-List</a>
		                  	<a class="dropdown-item" href="{{route('parksTickets',['name'=>"universal"])}}">Park & Ticket</a>
		                  	<a class="dropdown-item" href="{{route('otherOffers',['name'=>'universal'])}}">Banner Ads Promoting Universal</a>
		              	</div>
		          	</li>
		          	<!-- Dropdown -->
		          	<li class="nav-item dropdown multi-level-dropdown">
			          	<a href="#" id="menu" data-toggle="dropdown" class="nav-link dropdown-toggle">Florida Destinations</a>
			          	<ul class="dropdown-menu">
			          		@foreach($floLoc as $key=> $allFloloc)
			            	<li class="dropdown-item dropdown-submenu">
			              		<a href="{{ route('viewAllFlorida',['id'=>$allFloloc->id,'name'=> preg_replace('/\s+/', '-',$allFloloc->name)]) }}" 
			              			@foreach($SubLoc as $allSubLo) 
			              				@if($allSubLo->location_id==$allFloloc->id) class="dropdown-toggle" @endif
			              			@endforeach >
			              			{{ $allFloloc->name }}
			              		</a>
		              			<ul class="dropdown-menu">
		              				@foreach($SubLoc as $allSubLoc)
			              				@if($allFloloc->id==$allSubLoc->location_id)
				                			<li class="dropdown-item">
				                  				<a href="{{ route('viewSubFloridaDeta',['id'=>$allSubLoc->id, 'name'=> preg_replace('/\s+/','-',$allSubLoc->name), 'locname'=> preg_replace('/\s+/','-',$allFloloc->name)]) }}" class="text-black">{{ $allSubLoc->name }}</a>
				                			</li>
			                			@endif
		                			@endforeach
				              	</ul>
				              	
					        </li>
					    	@endforeach
					    </ul>
			        </li>

			       
			        <li class="nav-item dropdown multi-level-dropdown">
			          	<a href="#" id="menu" data-toggle="dropdown" class="nav-link dropdown-toggle">USA</a>
			          	<ul class="dropdown-menu">
			          		@foreach($usaLoc as $key=>$allUsaloc)
			          		<li class="dropdown-item dropdown-submenu">
			              		<a href="{{ route('viewAllUsa',['id'=>$allUsaloc->id,'name'=> preg_replace('/\s+/', '-',$allUsaloc->name)]) }}" 
			              			@foreach($SubLoc as $allSubLo) 
			              				@if($allUsaloc->id==$allSubLo->location_id) class="dropdown-toggle" @endif
			              			@endforeach >
			              			{{ $allUsaloc->name }}
			              		</a>
			              		<ul class="dropdown-menu">
		              				@foreach($SubLoc as $allSubLoc)
			              				@if($allUsaloc->id==$allSubLoc->location_id)
				                			<li class="dropdown-item">
				                  				<a href="{{ route('viewSubFloridaDeta',['id'=>$allSubLoc->id, 'name'=> preg_replace('/\s+/','-',$allSubLoc->name), 'locname'=> preg_replace('/\s+/','-',$allUsaloc->name)]) }}" class="text-black">{{ $allSubLoc->name }}</a>
				                			</li>
			                			@endif
		                			@endforeach
				              	</ul>
					        </li>
					    	@endforeach
					    </ul>
			        </li>

		      		<li class="nav-item">
		              	<a class="nav-link" href="{{route('showAllHotDeal')}}">Hot Deals</a>
		      		</li>
		      		<li class="nav-item">
		              	<a class="nav-link" href="{{route('showAllVillas')}}">Villas</a>
		      		</li>
		      		<li class="nav-item">
		              	<a class="nav-link" href="{{route('showAllParksTicket')}}">Parks & Passes</a>
		      		</li>
		      	</ul>
		    </div>
		</nav>
		<!--/.Navbar-->
	</div>
	<div class="preloader-wrapper">
	    <div class="preloader">
	        <img src="{{asset('front_component/img/AjaxLoader.gif')}}" alt="NILA">
	    </div>
	</div>
</header>