<!-- jQuery -->
<script type="text/javascript" src="{{asset('front_component/js/jquery.min.js')}}"></script>
<!-- select 2 -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="{{asset('front_component/js/popper.min.js')}}"></script>
<!-- Date picker-->
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{asset('front_component/js/bootstrap.min.js')}}"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{asset('front_component/js/mdb.min.js')}}"></script>
<!-- OWL Carocial -->
<script type="text/javascript" src="{{asset('front_component/js/owl.carousel.min.js')}}"></script>
<!-- Custom -->
<script type="text/javascript" src="{{asset('front_component/js/custom.js')}}"></script>

<script type="text/javascript">
	{{-- SELECT 2 --}}
	// In your Javascript (external .js resource or <script> tag)
	$(document).ready(function() {
	    $('.select2').select2();
	});

	//Searchings
  	function searchLocation(){
      location=document.filterForm.filterSelect.options[document.filterForm.filterSelect.selectedIndex].value;
 	}
  	function searchprice(){
      location=document.sortprice.sortpricename.options[document.sortprice.sortpricename.selectedIndex].value
  	}

  	//Contact steps
  	$(document).ready(function () {
  		//Contact steps 1
  		$(document).on('click','#step1',function () {
			$('.step1').removeClass('hide');
			for (var step = 1; step <=6; step++) {
				if (step>1) {
					$('.step'+step).addClass('hide');
					$('#step'+step).parent().removeClass('completed');
					$('#step'+step).parent().addClass('active');
				}
			}
			for(var step=1; step<=7; step++){
	  			$('.multipleStops'+step).addClass('hide');
	  		}
		});

		//slect holiday type
		$('#enquiry-form input').on('change', function() {
		   var holidayType = $('input[name=holiday_type]:checked', '#enquiry-form').val(); 
		   if(holidayType=='package' || holidayType=='accommodation') {
		   		$('#accommodationbox').removeClass('hide');
		   		$('#numberofdestinatios').addClass('hide');
		   		$('#cruise').addClass('hide');

		   		$(".preferred_cruise option:selected").prop("selected", false);
		   		$(".cabin_type option:selected").prop("selected", false);
		   		$(".number_stops option:selected").prop("selected", false);

		   }
		   else if(holidayType=='mix-it-up'){
		   		$('#accommodationbox').addClass('hide');
		   		$('#numberofdestinatios').removeClass('hide');
		   		$('#cruise').addClass('hide');

		   		$(".accommodation_type option:selected").prop("selected", false);
		   		$(".preferred_cruise option:selected").prop("selected", false);
		   		$(".cabin_type option:selected").prop("selected", false);
		   		
		   }
		   else if(holidayType=='flight_vehicle'){
		   		$('#accommodationbox').addClass('hide');
		   		$('#numberofdestinatios').addClass('hide');
		   		$('#cruise').addClass('hide');	
		   		
		   		$(".accommodation_type option:selected").prop("selected", false);
		   		$(".preferred_cruise option:selected").prop("selected", false);
		   		$(".cabin_type option:selected").prop("selected", false);
		   		$(".number_stops option:selected").prop("selected", false);
		   }
		   else if (holidayType=='cruise_stay') {
		   		$('#accommodationbox').addClass('hide');
		   		$('#numberofdestinatios').addClass('hide');
		   		$('#cruise').removeClass('hide');

		   		$(".accommodation_type option:selected").prop("selected", false);
		   		$(".number_stops option:selected").prop("selected", false);
		   }
		});

		$(document).on('click','#nextStep1',function () {
			//onclick step 2
			$(document).on('click','#step2',function () {
				var numberStops = $(".number_stops option:selected").val();
				for (var step = 1; step <=6; step++) {
					if (numberStops==1) {
						if (step==2) {
							$('.step'+step).removeClass('hide');
						}else{
							$('.step'+step).addClass('hide');
						}
					}else{
						if (step==numberStops-1) {
							$('.multipleStops'+step).removeClass('hide');
							$('.step'+step).addClass('hide');
						}else{
							$('.step'+step).addClass('hide');
						}
					}
				}
			});
			var holidayType = $('input[name=holiday_type]:checked', '#enquiry-form').val();
			if(holidayType=='package' || holidayType=='accommodation') {
				var selectedText = $(".accommodation_type option:selected").val();
				if (selectedText) {
					$('.step1').addClass('hide');
					$('.step2').removeClass('hide');
					$('.accommodation-error').removeClass('show');
				}else{
					$('.accommodation-error').addClass('show');
				}
	   		}

	   		if(holidayType=='mix-it-up') {
	   			var selectedText = $(".number_stops option:selected").val();
	   			if (selectedText) {
	   				if (selectedText==1) {
		   				$('.step2').removeClass('hide');
	   				}
	   				else if(selectedText==2){
	   					for (var step = 1; step <=6; step++) {
							$('.step'+step).addClass('hide');
						}
	   					$('.multipleStops1').removeClass('hide');	
	   				}
	   				else if(selectedText==3){
	   					for (var step = 1; step <=6; step++) {
							$('.step'+step).addClass('hide');
						}
	   					$('.multipleStops2').removeClass('hide');	
	   				}
	   				else if(selectedText==4){
	   					for (var step = 1; step <=6; step++) {
							$('.step'+step).addClass('hide');
						}
	   					$('.multipleStops3').removeClass('hide');	
	   				}
	   				else if(selectedText==5){
	   					for (var step = 1; step <=6; step++) {
							$('.step'+step).addClass('hide');
						}
	   					$('.multipleStops4').removeClass('hide');	
	   				}
	   				else if(selectedText==6){
	   					for (var step = 1; step <=6; step++) {
							$('.step'+step).addClass('hide');
						}
	   					$('.multipleStops5').removeClass('hide');	
	   				}
	   				else if(selectedText==7){
	   					for (var step = 1; step <=6; step++) {
							$('.step'+step).addClass('hide');
						}
	   					$('.multipleStops6').removeClass('hide');	
	   				}
	   				$('.step1').addClass('hide');
					$('.number-stops-error').removeClass('show');
				}else{
					$('.number-stops-error').addClass('show');
				}
	   		}

	   		if(holidayType=='flight_vehicle') {
				$('.step1').addClass('hide');
				$('.step2').removeClass('hide');
	   		}

	   		if(holidayType=='cruise_stay') {
				var selectedText = $(".preferred_cruise option:selected").val();
				var selectedText1 = $(".cabin_type option:selected").val();
				if (selectedText && selectedText1) {
					$('.step1').addClass('hide');
					$('.step2').removeClass('hide');
					$('.preferred-cruise-error, .cabin-type-error').removeClass('show');
				}else{
					$('.preferred-cruise-error, .cabin-type-error').addClass('show');
				}
	   		}
	   		
   			$('#step2').parent().removeClass('active');
			$('#step2').parent().addClass('completed');


		});

		//Step 2
		$(document).on('click','#nextStep2',function () {
			//onclick step 3
			$(document).on('click','#step3',function () {
				for (var step = 1; step <=6; step++) {
					if (step==3) {
						$('.step'+step).removeClass('hide');
					}else{
						$('.step'+step).addClass('hide');
					}
				}
			});

			var selectedText = $("select[name='dest_name[]']").map(function(){return $(this).val();}).get();
			var aDestName = selectedText.filter(function (el) { return el != null && el != ""; });
			if (aDestName.length==0) {
				$('.dest-name-error').removeClass('show');
			}else{
				$('.dest-name-error').addClass('show');
			}

			var selectedText = $("select[name='stay_time[]']").map(function(){return $(this).val();}).get();
			var aStayTime = selectedText.filter(function (el) { return el != null && el != ""; });
			if (aStayTime.length==0) {
				$('.stay-time-error').removeClass('show');
			}else{
				$('.stay-time-error').addClass('show');
			}
			if (aStayTime.length >0 && aDestName.length>0) {
				$('.step1').addClass('hide');
				$('.step2').addClass('hide');
				$('.step3').removeClass('hide');

	   			$('#step3').parents().removeClass('active');
				$('#step3').parents().addClass('completed');
				$('#step2').parents().removeClass('active');
				$('#step2').parents().addClass('completed');
	   		}

		});

		//Step 2 with multi city
		$(document).on('click','#nextStep21,#nextStep22,#nextStep23,#nextStep24,#nextStep25,#nextStep26',function () {
			//onclick step 3
			$(document).on('click','#step3',function () {
				for (var step = 1; step <=6; step++) {
					if (step==3) {
						$('.step'+step).removeClass('hide');
					}else{
						$('.step'+step).addClass('hide');
					}
				$('.multipleStops'+step).addClass('hide');
				}
			});

			
			var numberStops = $(".number_stops option:selected").val();
			var selectedText = $("select[name='dest_name[]']").map(function(){return $(this).val();}).get();
			var aDestName = selectedText.filter(function (el) { return el != null && el != ""; });
			if (aDestName.length>0) {
				$('.dest-names-error').removeClass('show');
			}else{
				$('.dest-names-error').addClass('show');
			}

			var selectedText = $("select[name='stay_time[]']").map(function(){return $(this).val();}).get();
			var aStayTime = selectedText.filter(function (el) { return el != null && el != ""; });
			if (aStayTime.length>0) {
				$('.stay-times-error').removeClass('show');
			}else{
				$('.stay-times-error').addClass('show');
			}
			if (aStayTime.length>1 && aDestName.length>1) {
				$('.step1').addClass('hide');
				$('.step2').addClass('hide');
				$('.step3').removeClass('hide');
				var multiStopHide = numberStops-1;
				$('.multipleStops'+multiStopHide).addClass('hide');

	   			$('#step3').parents().removeClass('active');
				$('#step3').parents().addClass('completed');
				$('#step2').parents().removeClass('active');
				$('#step2').parents().addClass('completed');
	   		}

		});

		//Step 3
		$(document).on('click','#nextStep3',function () {
			//onclick step 4
			$(document).on('click','#step4',function () {
				for (var step = 1; step <=6; step++) {
					if (step==4) {
						$('.step'+step).removeClass('hide');
					}else{
						$('.step'+step).addClass('hide');
					}
				}
			});
			var selectedText = $(".departure_date").val();
			if (selectedText) {
				$('.departure-date-error').removeClass('show');
			}else{
				$('.departure-date-error').addClass('show');
			}

			var selectedText = $(".flexible_date option:selected").val();
			if (selectedText) {
				$('.date-flexibilty-error').removeClass('show');
			}else{
				$('.date-flexibilty-error').addClass('show');
			}

			var selectedText = $(".flying_from option:selected").val();
			if (selectedText) {
				$('.flying-from-error').removeClass('show');
			}else{
				$('.flying-from-error').addClass('show');
			}

			if (selectedText) {
				for (var step = 1; step <=5; step++) {
					if (step ==4) {
						$('.step'+step).removeClass('hide');
					}
					else{
						$('.step'+step).addClass('hide');
					}
				}
				$('#step2').parents().removeClass('active');
				$('#step2').parents().addClass('completed');
	   			$('#step3').parents().removeClass('active');
				$('#step3').parents().addClass('completed');
				$('#step4').parents().removeClass('active');
				$('#step4').parents().addClass('completed');
	   		}
		});
		
		//Step 4
		$(document).on('click','#nextStep4',function () {
			//onclick step 5
			$(document).on('click','#step5',function () {
				for (var step = 1; step <=6; step++) {
					if (step==5) {
						$('.step'+step).removeClass('hide');
					}else{
						$('.step'+step).addClass('hide');
					}
				}
			});
			var selectedText = $(".number_adults option:selected").val();
			if (selectedText) {
				$('.number-adults-error').removeClass('show');
			}else{
				$('.number-adults-error').addClass('show');
			}

			if (selectedText) {
				for (var step = 1; step <=7; step++) {
					if (step ==5) {
						$('.step'+step).removeClass('hide');
					}
					else{
						$('.step'+step).addClass('hide');
					}
				}
				$('#step2').parents().removeClass('active');
				$('#step2').parents().addClass('completed');
	   			$('#step3').parents().removeClass('active');
				$('#step3').parents().addClass('completed');
				$('#step4').parents().removeClass('active');
				$('#step4').parents().addClass('completed');
				$('#step5').parents().removeClass('active');
				$('#step5').parents().addClass('completed');
	   		}

		});

		//Step 5
		$(document).on('click','#nextStep5',function () {
			var selectedText = $(".email-add").val();
			var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			if (selectedText && selectedText.match(mailformat)) {
				$('.email-error').removeClass('show');
			}else{
				$('.email-error').addClass('show');
			}

			if (selectedText) {
				for (var step = 1; step <=7; step++) {
					if (step ==6) {
						$('.step'+step).removeClass('hide');
					}
					else{
						$('.step'+step).addClass('hide');
					}
				}
				$('#step2').parents().removeClass('active');
				$('#step2').parents().addClass('completed');
	   			$('#step3').parents().removeClass('active');
				$('#step3').parents().addClass('completed');
				$('#step4').parents().removeClass('active');
				$('#step4').parents().addClass('completed');
				$('#step5').parents().removeClass('active');
				$('#step5').parents().addClass('completed');
	   		}

		});

		//Step 6
		$(document).on('click','#nextStep6',function () {
			var selectedText = $(".first-name").val();
			if (selectedText) {
				$('.first-name-error').removeClass('show');
			}else{
				$('.first-name-error').addClass('show');
			}

			var selectedText1 = $(".last-name").val();
			if (selectedText1) {
				$('.last-name-error').removeClass('show');
			}else{
				$('.last-name-error').addClass('show');
			}

			var selectedText2 = $(".mobile_numbber").val();
			var phonepattern = /^[-+]?\d+$/;
			if (selectedText2.match(phonepattern)) {
				$('.mobile-numbber-error').removeClass('show');
			}else{
				$('.mobile-numbber-error').addClass('show');
			}

			if (selectedText && selectedText1 && selectedText2) {
				for (var step = 1; step <=6; step++) {
					if (step ==6) {
						$('.step'+step).removeClass('hide');
					}
					else{
						$('.step'+step).addClass('hide');
					}
				}
				$('#step2').parents().removeClass('active');
				$('#step2').parents().addClass('completed');
	   			$('#step3').parents().removeClass('active');
				$('#step3').parents().addClass('completed');
				$('#step4').parents().removeClass('active');
				$('#step4').parents().addClass('completed');
				$('#step5').parents().removeClass('active');
				$('#step5').parents().addClass('completed');

				$('#enquiry-form').submit();
	   		}

		});

	});
</script>