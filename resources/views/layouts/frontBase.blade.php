<!DOCTYPE html>
<html lang="en">
	@include('include.head-link')
<body>
	@include('include.header')
	@include('pages.slider')
	@yield('content')
	@include('include.footer')
	@include('include.script')
</body>
</html>