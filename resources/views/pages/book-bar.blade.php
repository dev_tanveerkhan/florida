		<br>
		<div  class="container booking-ad">
			<!-- AD WITH LOGIN CREENTIAL -->
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="card">
					    <h5 class="card-header bg-color white-text text-center py-4">
					        <strong>For The Joy of Florida</strong>
					    </h5>
					    <!--Card content-->
					    <div class="card-body px-lg-5 mt-4">
					        <!-- Form -->
					        <form class="text-center" style="color: #757575;" method="get" id="enquiry-form" action="{{route('contactUs')}}">
					            <!-- Name -->
					            <input type="text" name="name" class="form-control mb-4" placeholder="Name">
							    <!-- Email -->
							    <input type="email" name="email" class="form-control mb-4" placeholder="E-mail">
							    <!-- Number -->
							    <input type="number" name="phone_number" class="form-control mb-4" placeholder="Phone Number">
					            <!-- Sign in button -->
					            <button class="btn text-white bg-btn-color btn-block rounded" type="submit">Enquire Now</button>
					        </form>
					        <!-- Form -->
					    </div>
					</div>
				</div>
				<div class="col-12 col-md-8">
					<div class="view">
					  <img src="{{asset('front_component/image/adbg.jpg')}}" class="img-thumb" alt="placeholder">
					  <div class="mask text-center p-3 waves-effect waves-light">
					    <p class="white-text h2 strong-title">{{$bookingAdd->title}}</p><br>
						<p class="white-text h6">{{$bookingAdd->description}}</p>
					  </div>
					</div>
				</div>
				<!-- Material form subscription -->
			</div>
		</div>
			