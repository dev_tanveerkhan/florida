@extends('layouts.subPageBase')
@section('title','Contact')
@section('content')
    <div class="container contact-page-sec">
        <div class="view contact-head-bgimg zoom">
            <img src="{{asset('front_component/image/contact.jpg')}}" class="img-fluid" alt="placeholder">
            <div class="mask flex-center waves-effect waves-light">
                <p class="white-text h1 strong-title text-center">~Contact Us~ </p>
            </div>
        </div>
        <div class="headings-contactus">
            <h2 class="h1-responsive font-weight-bold text-center my-4">Contact us</h2>
            <p class="text-center w-responsive mx-auto mb-5">{{$contactInfo->description}}</p>
        </div>
        <div class="contact-form-section">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="text-Black text-center align-items-center py-5 px-4">
                        <h5><img class="rounded-circle z-depth-2 mx-auto" style="max-height:6em;" src="{{asset('front_component/image/icon-media-contacts.png')}}"></h5>
                        <h3 class="pt-2"><strong>{{ $contactInfo->phone_number }}</strong></h3>
                        <p>Available everyday 9am - 9pm</p>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="text-black-strong text-center align-items-center py-5 px-4">
                        <h5><img class="rounded-circle z-depth-2 mx-auto" style="max-height:6em;" src="{{asset('front_component/image/icon-media-mail.png')}}"></h5>
                        <h3 class="pt-2"><strong>{{ $contactInfo->email }}</strong></h3>
                        <p>Mail FloridaCalling experts to get latest information</p>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="text-black text-center align-items-center py-5 px-4">
                        <h5><img class="rounded-circle z-depth-2 mx-auto" style="max-height:6em;" src="{{asset('front_component/image/icon-location.png')}}"></h5>
                        <h3 class="pt-2"><strong>Location</strong></h3>
                        <p>{{ $contactInfo->location }}</p>
                    </div>
                </div>
            </div>
            <br>
            <p class="h2 strong-title text-center"> Be Safe With Us </p>
            <div class="row">
                @foreach($loveFlorida as $luvflo)
                <div class="col-12 col-md-4 ">
                    <div class="text-black-strong text-center align-items-center py-5 px-4">
                        <h5><img class="rounded-circle z-depth-2 mx-auto" style="max-height:6em;" src="{{asset('storage/'.$luvflo->image)}}"></h5>
                        <h3 class="pt-2"><strong>{{ $luvflo->title }}</strong></h3>
                        <p>{{$luvflo->description}}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection