<div class="common-section row dining-section">
	<div class="col-md-7">
		<div class="dining-content">
			<h2 class="title title-left">{{$fddTitle->title}}</h2>
			<h5>{{$fddTitle->sub_title}}</h5>
			@foreach($fddContent as $con)
				<div class="dining-col">
					<figure><img src="{{asset('storage/'.$con->image)}}" alt="" /></figure>
					<p>
						<span>{{$con->title}}</span>{{$con->sub_title}}
						<a href="{{route('disneyHolidaysHotels',['id'=>$con->url,'name'=>preg_replace('/\s+/', '-',$con->disneyResort->title)])}}">{{$con->disneyResort->title}}</a>
					</p>
				</div>
			@endforeach
		</div>
	</div>
	<div class="col-md-5">
		<div class="dining-img">
			<img  src="{{asset('storage/'.$fddTitle->image)}}" alt="" />
		</div>
	</div>
</div>