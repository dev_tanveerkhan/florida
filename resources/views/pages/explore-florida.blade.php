
<br><br>
		<div class="container destination">
			<div class="text-center">
				<h1 class="title-row text-color strong-title"><strong> ~Explore Florida~ </strong></h1>
			</div>
			<div class="row">
				@foreach($exploreFlorida as $explFlorida)
				<div class="col-6 col-md-4">
					<div class="view desti-crd rounded zoom">
					  <img src="{{asset('storage/'.$explFlorida->image)}}" class="img-fluid" alt="Sample image with waves effect.">
					    <a href="
							@if(strtolower($explFlorida->title)=='hotel'){{route('showAllHotels')}} 
							@elseif(strtolower($explFlorida->title)=='villas'){{route('showAllVillas')}} 
							@elseif(strtolower($explFlorida->title)=='disney holiday'){{route('disneyHoidays')}} 
							@elseif(strtolower($explFlorida->title)=='universal holiday'){{route('universalHoliday')}}
							@elseif(strtolower($explFlorida->title)=='parks & tickets'){{route('showAllParksTicket')}}
							@elseif(strtolower($explFlorida->title)=='hot deals'){{route('showAllHotDeal')}}
							@endif">
						    <div class="mask text-center waves-effect waves-light rgba-white-slight">	
							    <div class="mask-txt">
								    <p class="h3 desti-expo-flo-title strong-title">{{$explFlorida->title}}</p>
								    <span class="price-tag">Starting from £{{$explFlorida->price}} pp</span>
							    </div>
						    </div>
					    </a>
					</div>
				</div>
				@endforeach
			</div>
		</div>