<br><br>
<div class="container card-slider">
	<!--Carousel Wrapper-->
	<div class="text-center">
		<h1 class="title-row text-color strong-title"><strong> ~ Favourite Stays in Florida ~ </strong></h1>
	</div>
	<div class="owl-carousel owl-theme">
	    @foreach($favStay as $favsty)
	    <div class="item">
	    	<div class="card card-cascade narrower">
			  <div class="view view-cascade overlay">
			    <img class="card-img-top" src="{{asset('storage/'.$favsty->image)}}"
			      alt="Card image cap">
			    <a>
			      <div class="mask rgba-white-slight"></div>
			    </a>
			  </div>
			  <!-- Grid column -->
				<div>
					<nav>
					  <ol class="breadcrumb text-color">
					    <li class="breadcrumb-item">
					    	@if($favsty->type==1) Hotel
							@elseif($favsty->type==2)  Villas
							@elseif($favsty->type==3) Disney
							@elseif($favsty->type==4) Universal
							@endif
					    </li>
					    <li class="breadcrumb-item">
					    	@if(!empty($favsty->location->name)) <span>{{$favsty->location->name}}</span> @endif
					    </li>
					    <li class="breadcrumb-item">
					    	@php
								$avg_rating = $favsty->rating;
								$non_rating = 5-$favsty->rating;
							@endphp
							@if(isset($avg_rating) && $avg_rating>0)
								@for($i=0; $i<$avg_rating; $i++)
									<span class="fa fa-star checked"></span>
								@endfor
								@for($j=0; $j<$non_rating; $j++)
									<span class="fa fa-star unchecked"></span>
								@endfor
							@endif
					    </li>
					  </ol>
					</nav>
				</div>
			  <div class="card-body card-body-cascade">
			  	<h3 class="text-color-red strong-title"><i class="fas fa-wallet"></i>Prices From £{{$favsty->price}} pp </h3>
			    <h4 class="font-weight-bold card-title">{{ $favsty->name }}</h4>
			    <p class="card-text">
			    	@php 
						$facility = json_decode($favsty->hotelDet->facility);
					@endphp
					@foreach($facility as $key=>$faci)
						@if($key<=3){{ $faci }}<strong> , </strong>@endif
					@endforeach ....
				</p>
			    <a class="btn bg-color text-white rounded" href="{{route('moreDetails',$favsty->id)}}">More Details-</a>
			  </div>
			</div>
			<!-- Card Narrower -->
	    </div>
	    @endforeach
	</div>
</div>