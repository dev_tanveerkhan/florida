@extends('layouts.subPageBase')
@section('title','Contact')
@section('content')
	<style type="text/css">
		/* Hide the browser's default radio button */
		.container input[type=radio]{
		  position: absolute;
		  opacity: 0;
		  cursor: pointer;
		}
		input[type=text],input[type=email]{
		    height: 48px !important;
			font-size: larger !important;
		}
		/* Create a custom radio button */
		.checkmark {
		  position: absolute;
		  top: 0;
		  left: 0;
		  height: 35px;
		  width: 35px;
		  background-color: #fff;
		  border-radius: 3px;
		  border:2px solid #123c69;
		}


		/* Create the indicator (the dot/circle - hidden when not checked) */
		.checkmark:after {
		  content: "\2713 ";
		  position: absolute;
		  display: none;
		}

		/* Show the indicator (dot/circle) when checked */
		.container input:checked ~ .checkmark:after {
		  font-size: 25px;
		  font-weight: bold;
		  line-height: 22px;
		  display: block;
		  content: "\2713 ";
		}

		/* Style the indicator (dot/circle) */
		.container .checkmark:after {
		 	top: 9px;
			left: 6px;
			width: 8px;
			height: 8px;
			border-radius: 3px;
			background: white;
		}
	</style>
    <div class="container contact-page-sec">
        <br>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{url('/')}}" class="h5 text-color">Home</a></li>
            <li class="breadcrumb-item active"><a class="h5">Get A Quote</a></li>
          </ol>
        </nav>
        @if ($message = Session::get('success'))
	        <div class="alert alert-success">
	          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	          {{ $message }}
	        </div>
	      @endif
        <div class="headings-contactus">
            <h2 class="h1-responsive font-weight-bold text-left">Get A Quote</h2>
        </div>
        <!-- Horizontal Steppers -->
        <div class="row">
          <div class="col-md-12">
            <ul class="stepper stepper-horizontal">
              <li class="completed">
                <a id="step1">
                  <span class="circle"><i class="fa fa-globe"></i></span>
                </a>
              </li>
              <li class="active">
                <a id="step2">
                  <span class="circle"><i class="fa fa-map-marker-alt"></i></span>
                </a>
              </li>
              <li class="active">
                <a id="step3">
                  <span class="circle"><i class="fa fa-calendar-alt"></i></span>
                </a>
              </li>
              <li class="active">
                <a id="step4">
                  <span class="circle"><i class="fa fa-users"></i></span>
                </a>
              </li>
              <li class="active">
                <a id="step5">
                  <span class="circle"><i class="fa fa-comments"></i></span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <!-- /.Horizontal Steppers -->
 
        <!-- Form -->
        <form class="mt-4" style="color: #757575;" method="post" id="enquiry-form" action="{{route('storeAllQuateInfo')}}">
        	@csrf
        	<input type="hidden" name="enquiry_type" value="2">
	        <!-- Step 1 -->
	        <div class="step1">
	        	<div class="row">
	                 <div class="col-12 col-md-12">
	                    <div class="card">
	                        <h5 class="card-header bg-color white-text text-center">
	                            <strong>Holiday Type?</strong>
	                        </h5>
	                        <!--Card content-->
	                        <div class="row card-body px-lg-5 p-4">
	                            <!-- Name -->
	                            <div class="form-group col-6 col-md-4">
	                            	<label class="container ml-3 p-2  changeInfo">Package Holiday <i class="fa fa-info"  data-toggle="tooltip" data-placement="bottom" title="Choose this if you want to package including flight and accommodation"></i>
									  <input type="radio" name="holiday_type" value="package">
									  <span class="checkmark"></span>
									</label>
	                            </div>
	                            
	                            <div class="form-group col-6 col-md-4">
	                                <label class="container ml-3 p-2 changeInfo">Multi Center <i class="fa fa-info"  data-toggle="tooltip" data-placement="bottom" title="Choose this if you want to visit more then one destination"></i>
									  	<input type="radio" name="holiday_type" value="mix-it-up">
									  	<span class="checkmark"></span>
									</label>
	                            </div>

	                            <div class="form-group col-6 col-md-4">
									<label class="container ml-3 p-2 changeInfo">Flight + Vehicle <i class="fa fa-info"  data-toggle="tooltip" data-placement="bottom" title="Choose this if you want to flight and vehicle"></i>
									  	<input type="radio" name="holiday_type" value="flight_vehicle">
									  	<span class="checkmark"></span>
									</label>
	                            </div>

	                            <div class="form-group col-6 col-md-4">
									<label class="container ml-3 p-2 changeInfo">Cruise Stay <i class="fa fa-info"  data-toggle="tooltip" data-placement="bottom" title="Choose this for a package including cruise flights and accommodation"></i>
									  	<input type="radio" name="holiday_type" value="cruise_stay">
									  	<span class="checkmark"></span>
									</label>
	                            </div>

	                            <div class="form-group col-4">
									<label class="container ml-3 p-2 changeInfo">Accommodation Only <i class="fa fa-info"  data-toggle="tooltip" data-placement="bottom" title="Choose this if you want to only Accommodation"></i>
									  	<input type="radio" name="holiday_type" value="accommodation">
									  	<span class="checkmark"></span>
									</label>
	                            </div>
	                    	</div>

	                    	<div class="form-group p-4 hide" id="accommodationbox">
	                    		<label for="accommodation">Accommodation Type ? *</label>
	                    		<select class="browser-default custom-select custom-select-lg mb-3 border-color accommodation_type" name="accommodation_type">
								  <option selected value="">Select Accommodation Type</option>
								  <option value="villa">Villa</option>
								  <option value="hotel">Hotel</option>
								  <option value="apartment">Apartment</option>
								</select>
								<div class="invalid-feedback accommodation-error"><b>Please select accommodation type. </b></div>
	                    	</div>

	                    	<div class="form-group p-4 hide" id="numberofdestinatios">
	                    		<label for="How many Destinatios">How Many Destinatios *</label>
	                    		<select class="browser-default custom-select custom-select-lg mb-3 border-color number_stops" name="number_stops">
								  <option selected value="">How many stops?</option>
								  <option value="1">1 Stop</option>
								  <option value="2">2 Stop</option>
								  <option value="3">3 Stop</option>
								  <option value="4">4 Stop</option>
								  <option value="5">5 Stop</option>
								  <option value="6">6 Stop</option>
								  <option value="7">7 Stop</option>
								</select>
								<div class="invalid-feedback number-stops-error"><b>Please select number of stops</b></div>
	                    	</div>

	                    	<div class="row form-group p-4 hide" id="cruise">
	                    		<div class="col-md-6">
		                    		<label for="How many Destinatios">Preferred Cruise Line </label>
		                    		<select class="browser-default custom-select custom-select-lg mb-3 border-color preferred_cruise" name="preferred_cruise">
									  <option selected value="">No preference</option>
									  <option value="best_offer">Best Offer</option>
									  <option value="royal_carribbean">Royal Carribbean</option>
									  <option value="norwegian_cruise_line">Norwegian Cruise Line</option>
									  <option value="celebrity">Celebrity</option>
									  <option value="carnival">Carnival</option>
									  <option value="disney_cruises">Disney Cruises</option>
									</select>
									<div class="invalid-feedback preferred-cruise-error"><b>Please select preferred cruise line</b></div>
								</div>

								<div class="col-md-6">
		                    		<label for="cabintytype">Cabin Type </label>
		                    		<select class="browser-default custom-select custom-select-lg mb-3 border-color cabin_type" name="cabin_type">
									  <option selected value="">No preference</option>
									  <option value="inside">Inside</option>
									  <option value="outside">Outside</option>
									  <option value="balcony">Balcony</option>
									  <option value="suite">Suite</option>
									</select>
									<div class="invalid-feedback cabin-type-error"><b>Please select cabin type</b></div>
								</div>
	                    	</div>

	                         <div class="form-group pl-4">
                            	<a class="btn text-white bg-btn-color rounded" id="nextStep1" >NEXT STEP</a>
                            </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- /Step 1 -->

	        <!-- Step 2 -->
	        <div class="step2 hide">
	            <div class="row">
	                 <div class="col-12 col-md-12">
	                    <div class="card">
	                        <h5 class="card-header bg-color white-text text-center">
	                            <strong>Choose Destination</strong>
	                        </h5>
	                        <!--Card content-->
	                        <div class="card-body px-lg-5">
	                        	<div class="row form-group p-4 destinationRemove" id="nameofdestinatios">
	                        		<div class="col-md-6">
			                    		<label for="How many Destinatios">Where would you like to go *</label>
			                    		<select class="form-control custom-select-lg border-color select2 dest-name" name="dest_name[]">
										  <option selected value="">Select Destination</option>
										  @foreach($location as $loc)
										  	<option value="{{ $loc->location->name }}">{{$loc->location->name}}</option>
										  @endforeach
										</select>
										<div class="invalid-feedback dest-name-error"><b>Please select name of destination </b></div>
									</div>

									<div class="col-md-6">
										<label for="How many Destinatios">Length of stay *</label>
			                    		<select class="form-control custom-select-lg border-color select2 stay-time" name="stay_time[]">
										  <option selected value="">Select Length of stay</option>
										  @for($sty=1; $sty<=30; $sty++)
										  	<option value="{{$sty}}">{{$sty}} nights</option>
										  @endfor
										</select>
										<div class="invalid-feedback stay-time-error"><b>Please select length of stay</b></div>
									</div>
	                    		</div>

	                    		<div class="form-group p-4	">
	                            	<a class="btn text-white bg-btn-color rounded nextStep2" id="nextStep2">NEXT STEP</a>
	                            </div>

	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- /Step 2 -->

	        <!-- Step 3 -->
	        <div class="step3 hide">
	            <div class="row">
	                 <div class="col-12 col-md-12">
	                    <div class="card">
	                        <h5 class="card-header bg-color white-text text-center">
	                            <strong>Departure Date </strong>
	                        </h5>
	                        <!--Card content-->
	                        <div class="card-body px-lg-5">
		                        <div class="row form-group p-4">
		                        	
		                        	<div class="col-md-6">
			                    		<label for="How many Destinatios">Departure date </label>
			                    		<input class="form-control input-lg mb-3 border-color departure_date" type="text" name="departure_date" id="datepicker" placeholder="Enter Departure Date">
										<div class="invalid-feedback departure-date-error"><b>Please select Departure date</b></div>
									</div>

		                    		<div class="col-md-6">
			                    		<label for="How many Destinatios"> Are flexible on this date? </label>
			                    		<select class="browser-default custom-select custom-select-lg mb-3 border-color flexible_date" name="flexible_date">
										  <option selected value="">Select date flexibilty</option>
										  <option value="flexibilty +/-">Flexibilty +/-</option>
										  <option value="i am not flexible">i am not flexible</option>
										  @for($flex=1;$flex<=15; $flex++)
										  	<option value="+/- {{$flex}} day"> +/- {{ $flex }} day </option>
										  @endfor
										</select>
										<div class="invalid-feedback date-flexibilty-error"><b>Please select date flexibilty</b></div>
									</div>

		                    	</div>

		                    	<div class="row form-group p-4">
		                    		<div class="col-md-6">
			                    		<label for="cabintytype"> Preffered Departure Airport </label>
			                    		<select class="browser-default custom-select custom-select-lg mb-3 border-color flying_from" name="flying_from">
										  <option selected value="">Select an Airport</option>
										  <option value="Belfast">Belfast</option>
										  <option value="Birmingham">Birmingham</option>
										  <option value="Bristol">Bristol</option>
										  <option value="Dublin">Dublin</option>
										  <option value="Edinburgh">Edinburgh</option>
										  <option value="East Midlands">East Midlands</option>
										  <option value="Glasgow">Glasgow</option>
										  <option value="London Gatwick">London Gatwick</option>
										  <option value="London All">London All</option>
										  <option value="Manchester">Manchester</option>
										  <option value="Newcastle">Newcastle</option>
										</select>
										<div class="invalid-feedback flying-from-error"><b>Please select any airport</b></div>
									</div>

									<div class="col-md-6">
			                    		<label for="cabintytype"> Transport Required </label>
			                    		<select class="browser-default custom-select custom-select-lg mb-3 border-color transport-req" name="transport_req">
										  <option selected value="">Select transport</option>
										  <option value="non required">Non Required</option>
										  <option value="Return Transfers">Return Transfers</option>
										  <option value="Fully Camp Car Hire">Fully Camp Car Hire</option>
										</select>
										<div class="invalid-feedback transport-error"><b>Please select transport</b></div>
									</div>
		                    	</div>

	                         <div class="form-group p-4	">
                            	<a class="btn text-white bg-btn-color rounded" id="nextStep3" >NEXT STEP</a>
                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- /Step 3 -->

	        <!-- Step 4 -->
	        <div class="step4 hide">
	            <div class="row">
	                 <div class="col-12 col-md-12">
	                    <div class="card">
	                        <h5 class="card-header bg-color white-text text-center">
	                            <strong>Number of people </strong>
	                        </h5>
	                        <!--Card content-->
	                        <div class="card-body px-lg-5">
		                        <div class="form-group p-4">
		                        	<label for="How many Destinatios"> Number of Adults? </label>
		                    		<select class="browser-default custom-select custom-select-lg border-color number_adults" name="number_adults">
									  <option selected value="">Select Adults</option>
									  @for($adults=1;$adults<=30; $adults++)
									  	<option value="{{$adults}}">{{ $adults }} Adults </option>
									  @endfor
									</select>
									<div class="invalid-feedback number-adults-error"><b>Please select adults</b></div>
		                    	</div>

		                    	<div class="form-group p-4">
	                    			<label for="cabintytype"> Children under 12yrs old? </label>
		                    		<select class="browser-default custom-select custom-select-lg border-color number_children" name="number_children">
									  <option selected value="">Select a number</option>
									  @for($child=1;$child<=20; $child++)
									  	<option value="{{$child}}">{{ $child }} Children </option>
									  @endfor
									</select>
									<div class="invalid-feedback children-number-error"><b>Please select a number</b></div>
								</div>

		                    	<div class="form-group p-4">
	                    			<label for="cabintytype"> Infants under 2 yrs old?? </label>
		                    		<select class="browser-default custom-select custom-select-lg border-color number_infants" name="number_infants">
									  <option selected value="">Select a number</option>
									  @for($infant=1;$infant<=20; $infant++)
									  	<option value="{{$infant}}">{{ $infant }} Infants </option>
									  @endfor
									</select>
									<div class="invalid-feedback infants-number-error"><b>Please select a number</b></div>
								</div>
								
	                         <div class="form-group p-4">
                            	<a class="btn text-white bg-btn-color rounded" id="nextStep4" >NEXT STEP</a>
                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- /Step 4 -->

	        <!-- Step 5 -->
	        <div class="step5 hide">
	            <div class="row">
	                 <div class="col-12 col-md-12">
	                    <div class="card">
	                        <h5 class="card-header bg-color white-text text-center">
	                            <strong>Finishing touches</strong>
	                        </h5>
	                        <!--Card content-->
	                        <div class="card-body px-lg-5">
		                        <div class="form-group p-4">
		                        	<label for="How many Destinatios">Anything we've missed? </label>
		                    		<textarea id="notes" name="Notes" class="form-control border-color anything-missed"rows="3" cols="3" placeholder="Any other comments or requests? Do you have any preferred hotels or are you celebrating a special occasion like a honeymoon?" title=""></textarea>
		                    	</div>

		                    	<div class="form-group p-4">
	                    			<label for="email"> Email address* </label>
		                    		<input type="email" name="email" placeholder="Enter email address" class="form-control border-color email-add">
									<div class="invalid-feedback email-error"><b>Please enter valid email address!</b></div>
								</div>

		                         <div class="form-group p-4">
	                            	<a class="btn text-white bg-btn-color rounded" id="nextStep5" >NEXT STEP</a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- /Step 5 -->

	        <!-- Step 6 -->
	        <div class="step6 hide">
	            <div class="row">
	                 <div class="col-12 col-md-12">
	                    <div class="card">
	                        <h5 class="card-header bg-color white-text text-center">
	                            <strong>Finishing touches</strong>
	                        </h5>
	                        <!--Card content-->
	                        <div class="card-body px-lg-5">
		                        <div class="row form-group p-4">
		                        	<div class="col-md-6">
		                    			<label for="f_name"> First name* </label>
			                    		<input type="f_name" name="f_name" placeholder="Enter first name here" class="form-control border-color first-name">
										<div class="invalid-feedback first-name-error"><b>Please enter your first name here</b></div>
									</div>
		                        	
		                        	<div class="col-md-6">
			                        	<label for="l_name"> Last name* </label>
			                    		<input type="l_name" name="l_name" placeholder="Enter first name here" class="form-control border-color last-name">
										<div class="invalid-feedback last-name-error"><b>Please enter your last name here</b></div>
									</div>
								</div>


		                        <div class="form-group p-4">
		                        	<label for="mobile_numbber">Mobile Number* </label>
			                    	<input type="mobile_numbber" name="mobile_numbber" placeholder="Enter mobile number here" class="form-control border-color mobile_numbber">
									<div class="invalid-feedback mobile-numbber-error"><b>Please enter moobile number here</b></div>
								</div>

		                         <div class="form-group p-4">
	                            	<a class="btn text-white bg-btn-color rounded" id="nextStep6">Submit All Information</a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- /Step 6 -->

	        <!-- After Step 2 if multiple stops-->
	        <div class="multipleStops1 hide">
	            <div class="row">
	                 <div class="col-12 col-md-12">
	                    <div class="card">
	                        <h5 class="card-header bg-color white-text text-center">
	                            <strong>Choose Destination</strong>
	                        </h5>
	                        <!--Card content-->
	                        <div class="card-body px-lg-5">
	                        	@for($i=1; $i<=2; $i++)
	                        	<label class="text-color pl-4 h4">Destination Number {{$i}}</label>
	                        	<div class="row form-group p-4" id="nameofdestinatios">
	                        		<div class="col-md-6">
			                    		<label for="How many Destinatios">Where would you like to go *</label>
			                    		<select class="form-control custom-select-lg border-color select2 dest-names" name="dest_name[]">
										  <option selected value="">Select Destination</option>
										  @foreach($location as $loc)
										  	<option value="{{ $loc->location->name }}">{{$loc->location->name}}</option>
										  @endforeach
										</select>
										<div class="invalid-feedback dest-names-error"><b>Please select name of destination </b></div>
									</div>

									<div class="col-md-6">
										<label for="How many Destinatios">Length of stay *</label>
			                    		<select class="form-control custom-select-lg border-color select2 stay-times" name="stay_time[]">
										  <option selected value="">Select Length of stay</option>
										  @for($sty=1; $sty<=30; $sty++)
										  	<option value="{{$sty}}">{{$sty}} nights</option>
										  @endfor
										</select>
										<div class="invalid-feedback stay-times-error"><b>Please select length of stay</b></div>
									</div>
	                    		</div>
	                    		@endfor
	                    		<div class="form-group p-4">
	                            	<a class="btn text-white bg-btn-color rounded" id="nextStep21">NEXT STEP</a>
	                            </div>

	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="multipleStops2 hide">
	            <div class="row">
	                 <div class="col-12 col-md-12">
	                    <div class="card">
	                        <h5 class="card-header bg-color white-text text-center">
	                            <strong>Choose Destination</strong>
	                        </h5>
	                        <!--Card content-->
	                        <div class="card-body px-lg-5">
	                        	@for($i=1; $i<=3; $i++)
	                        	<label class="text-color pl-4 h4">Destination Number {{$i}}</label>
	                        	<div class="row form-group p-4" id="nameofdestinatios">
	                        		<div class="col-md-6">
			                    		<label for="How many Destinatios">Where would you like to go *</label>
			                    		<select class="form-control custom-select-lg border-color select2 dest-names" name="dest_name[]">
										  <option selected value="">Select Destination</option>
										  @foreach($location as $loc)
										  	<option value="{{ $loc->location->name }}">{{$loc->location->name}}</option>
										  @endforeach
										</select>
										<div class="invalid-feedback dest-names-error"><b>Please select name of destination </b></div>
									</div>

									<div class="col-md-6">
										<label for="How many Destinatios">Length of stay *</label>
			                    		<select class="form-control custom-select-lg border-color select2 stay-times" name="stay_time[]">
										  <option selected value="">Select Length of stay</option>
										  @for($sty=1; $sty<=30; $sty++)
										  	<option value="{{$sty}}">{{$sty}} nights</option>
										  @endfor
										</select>
										<div class="invalid-feedback stay-times-error"><b>Please select length of stay</b></div>
									</div>
	                    		</div>
	                    		@endfor
	                    		<div class="form-group p-4	">
	                            	<a class="btn text-white bg-btn-color rounded" id="nextStep22">NEXT STEP</a>
	                            </div>

	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="multipleStops3 hide">
	            <div class="row">
	                 <div class="col-12 col-md-12">
	                    <div class="card">
	                        <h5 class="card-header bg-color white-text text-center">
	                            <strong>Choose Destination</strong>
	                        </h5>
	                        <!--Card content-->
	                        <div class="card-body px-lg-5">
	                        	@for($i=1; $i<=4; $i++)
	                        	<label class="text-color pl-4 h4">Destination Number {{$i}}</label>
	                        	<div class="row form-group p-4" id="nameofdestinatios">
	                        		<div class="col-md-6">
			                    		<label for="How many Destinatios">Where would you like to go *</label>
			                    		<select class="form-control custom-select-lg border-color select2 dest-names" name="dest_name[]">
										  <option selected value="">Select Destination</option>
										  @foreach($location as $loc)
										  	<option value="{{ $loc->location->name }}">{{$loc->location->name}}</option>
										  @endforeach
										</select>
										<div class="invalid-feedback dest-names-error"><b>Please select name of destination </b></div>
									</div>

									<div class="col-md-6">
										<label for="How many Destinatios">Length of stay *</label>
			                    		<select class="form-control custom-select-lg border-color select2 stay-times" name="stay_time[]">
										  <option selected value="">Select Length of stay</option>
										  @for($sty=1; $sty<=30; $sty++)
										  	<option value="{{$sty}}">{{$sty}} nights</option>
										  @endfor
										</select>
										<div class="invalid-feedback stay-times-error"><b>Please select length of stay</b></div>
									</div>
	                    		</div>
	                    		@endfor
	                    		<div class="form-group p-4	">
	                            	<a class="btn text-white bg-btn-color rounded" id="nextStep23">NEXT STEP</a>
	                            </div>

	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="multipleStops4 hide">
	            <div class="row">
	                 <div class="col-12 col-md-12">
	                    <div class="card">
	                        <h5 class="card-header bg-color white-text text-center">
	                            <strong>Choose Destination</strong>
	                        </h5>
	                        <!--Card content-->
	                        <div class="card-body px-lg-5">
	                        	@for($i=1; $i<=5; $i++)
	                        	<label class="text-color pl-4 h4">Destination Number {{$i}}</label>
	                        	<div class="row form-group p-4" id="nameofdestinatios">
	                        		<div class="col-md-6">
			                    		<label for="How many Destinatios">Where would you like to go *</label>
			                    		<select class="form-control custom-select-lg border-color select2 dest-names" name="dest_name[]">
										  <option selected value="">Select Destination</option>
										  @foreach($location as $loc)
										  	<option value="{{ $loc->location->name }}">{{$loc->location->name}}</option>
										  @endforeach
										</select>
										<div class="invalid-feedback dest-names-error"><b>Please select name of destination </b></div>
									</div>

									<div class="col-md-6">
										<label for="How many Destinatios">Length of stay *</label>
			                    		<select class="form-control custom-select-lg border-color select2 stay-times" name="stay_time[]">
										  <option selected value="">Select Length of stay</option>
										  @for($sty=1; $sty<=30; $sty++)
										  	<option value="{{$sty}}">{{$sty}} nights</option>
										  @endfor
										</select>
										<div class="invalid-feedback stay-times-error"><b>Please select length of stay</b></div>
									</div>
	                    		</div>
	                    		@endfor
	                    		<div class="form-group p-4	">
	                            	<a class="btn text-white bg-btn-color rounded" id="nextStep24">NEXT STEP</a>
	                            </div>

	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="multipleStops5 hide">
	            <div class="row">
	                 <div class="col-12 col-md-12">
	                    <div class="card">
	                        <h5 class="card-header bg-color white-text text-center">
	                            <strong>Choose Destination</strong>
	                        </h5>
	                        <!--Card content-->
	                        <div class="card-body px-lg-5">
	                        	@for($i=1; $i<=6; $i++)
	                        	<label class="text-color pl-4 h4">Destination Number {{$i}}</label>
	                        	<div class="row form-group p-4" id="nameofdestinatios">
	                        		<div class="col-md-6">
			                    		<label for="How many Destinatios">Where would you like to go *</label>
			                    		<select class="form-control custom-select-lg border-color select2 dest-names" name="dest_name[]">
										  <option selected value="">Select Destination</option>
										  @foreach($location as $loc)
										  	<option value="{{ $loc->location->name }}">{{$loc->location->name}}</option>
										  @endforeach
										</select>
										<div class="invalid-feedback dest-names-error"><b>Please select name of destination </b></div>
									</div>

									<div class="col-md-6">
										<label for="How many Destinatios">Length of stay *</label>
			                    		<select class="form-control custom-select-lg border-color select2 stay-times" name="stay_time[]">
										  <option selected value="">Select Length of stay</option>
										  @for($sty=1; $sty<=30; $sty++)
										  	<option value="{{$sty}}">{{$sty}} nights</option>
										  @endfor
										</select>
										<div class="invalid-feedback stay-times-error"><b>Please select length of stay</b></div>
									</div>
	                    		</div>
	                    		@endfor
	                    		<div class="form-group p-4	">
	                            	<a class="btn text-white bg-btn-color rounded" id="nextStep25">NEXT STEP</a>
	                            </div>

	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="multipleStops6 hide">
	            <div class="row">
	                 <div class="col-12 col-md-12">
	                    <div class="card">
	                        <h5 class="card-header bg-color white-text text-center">
	                            <strong>Choose Destination</strong>
	                        </h5>
	                        <!--Card content-->
	                        <div class="card-body px-lg-5">
	                        	@for($i=1; $i<=7; $i++)
	                        	<label class="text-color pl-4 h4">Destination Number {{$i}}</label>
	                        	<div class="row form-group p-4" id="nameofdestinatios">
	                        		<div class="col-md-6">
			                    		<label for="How many Destinatios">Where would you like to go *</label>
			                    		<select class="form-control custom-select-lg border-color select2 dest-names" name="dest_name[]">
										  <option selected value="">Select Destination</option>
										  @foreach($location as $loc)
										  	<option value="{{ $loc->location->name }}">{{$loc->location->name}}</option>
										  @endforeach
										</select>
										<div class="invalid-feedback dest-names-error"><b>Please select name of destination </b></div>
									</div>

									<div class="col-md-6">
										<label for="How many Destinatios">Length of stay *</label>
			                    		<select class="form-control custom-select-lg border-color select2 stay-times" name="stay_time[]">
										  <option selected value="">Select Length of stay</option>
										  @for($sty=1; $sty<=30; $sty++)
										  	<option value="{{$sty}}">{{$sty}} nights</option>
										  @endfor
										</select>
										<div class="invalid-feedback stay-times-error"><b>Please select length of stay</b></div>
									</div>
	                    		</div>
	                    		@endfor
	                    		<div class="form-group p-4	">
	                            	<a class="btn text-white bg-btn-color rounded" id="nextStep26">NEXT STEP</a>
	                            </div>

	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- /Step 2 -->

	    </form>
		<!-- Form -->
	</div>
	
@endsection