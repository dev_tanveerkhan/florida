<div class="deal-section common-section">
	<h1 class="title">Hot Deals</h1>
	<div class="owl-carousel owl-deal">
		<!-- load hotDeals view -->
		@foreach($hotDeals as $hd)
		<div class="item">
			<div class="card-box">
				<div class="deal-thumb">
					<img src="{{asset('storage/'.$hd->image)}}" alt="" />
					<div class="card-info">
						<div class="fav-query bg-dark"> <span> {{ $hd->days }} nights from £ {{$hd->price}}<sub> pp </sub></span>
						</div>
					</div>
				</div>
				<div class="deal-content">
					<div class="hotel-crumb">
						<span>
							<span>
									@if($hd->type==1) Hotel
									@elseif($hd->type==2)  Villas
									@elseif($hd->type==3) Disney
									@elseif($hd->type==4) Universal
									@endif
								</span>
						</span>
						@if(!empty($hd->location->name)) <span>{{$hd->location->name}}</span> @endif 
						<span>
							@php
								$avg_rating = $hd->rating;
								$non_rating = 5-$hd->rating;
							@endphp
							@if(isset($avg_rating) && $avg_rating>0)
								@for($i=0; $i<$avg_rating; $i++)
									<i class="fa fa-star checked"></i>
								@endfor
								@for($j=0; $j<$non_rating; $j++)
									<i class="fa fa-star"></i>
								@endfor
							@endif
						</span>
					</div>
					<div class="deal-header">
						<h5><a href="{{route('moreDetails',$hd->id)}}">{{ $hd->name }}</a></h5>
						<p class="more">Enjoy 2 Free Nights, Resort Amenities & Much More</p>
						<a href="{{route('moreDetails',$hd->id)}}" class="badge badge-danger">More offers available here!</a>
					</div>
					<div class="travel-info">
						<div class="travel-icon">
							<span class="fa fa-calendar"></span>
							<p>
								Multiple </p>
						</div>
						<div class="travel-icon"> <span class="fa fa-plane"></span>
							<p>LON</p>
						</div>
						<div class="travel-icon"> <span class=" price">£{{ $hd->saving }} pp</span>
							<p>Save</p>
						</div>
					</div>
					<!--end travel-info-->
					<div class="deal-footer-btn">
						<a href="{{route('moreDetails',$hd->id)}}" class="badge badge-danger text-size20">More Details</a>
					</div>
				</div>
				<!--end deal ctn-->
			</div>
			<!--end deal-box-->
		</div>
		@endforeach
	</div>
	<div class="butoon-bottom">
		<a href="{{route('showAllHotDeal')}}" class="btn btn-red">View all our Hot Deals</a>
	</div>
</div>