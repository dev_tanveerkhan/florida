@extends('layouts.subPageBase')
@if(Request::is('rest-usa/*'))
@section('title','USA')
@elseif(Request::is('rest-florida/*'))
@section('title','FLORIDA')
@endif
@section('content')
<div class="content">
	<div class="banner">
	  	<div class="centered">
	  		<h2>{{$name}} Hot Deals</h2>
	  	</div>
		<img src="{{asset('front_component/images/contact-banner.jpg')}}" style="height: 300px;width: 100%;" alt=""/> 
	</div>
	<div class="tab-nav">
  		<ul>
    		<li><a href="
    			@if(Request::is('rest-usa/*'))
    				{{route('viewAllUsa',['id'=>$id->id,'name'=> preg_replace('/\s+/', '-',$name)])}} 
    			@elseif(Request::is('rest-florida/*'))
    				{{route('viewAllFlorida',['id'=>$id->id,'name'=> preg_replace('/\s+/', '-',$name)])}}
    			@elseif(Request::is('florida/*'))
    				{{ route('viewSubFloridaDeta',['id'=>$id->id, 'name'=> preg_replace('/\s+/','-',$name), 'locname'=> preg_replace('/\s+/','-',$locname)]) }} @endif">Overview</a></li>

    		  <li><a href="@if(Request::is('rest-usa/*'))
    				{{route('viewAllTypeHotels',['id'=>$id->id,'name'=> preg_replace('/\s+/', '-',$name)])}} 
    			@elseif(Request::is('rest-florida/*'))
    				{{route('viewAllTypeHotels',['id'=>$id->id,'name'=> preg_replace('/\s+/', '-',$name)])}}
    			@elseif(Request::is('florida/*'))
    				{{ route('viewAllTypeHotels',['id'=>$id->id, 'name'=> preg_replace('/\s+/','-',$name)]) }} @endif ">Hotels</a></li>

        	<li><a href="@if(Request::is('rest-usa/*'))
            {{route('viewAllTypeHotDeal',['id'=>$id->id,'name'=> preg_replace('/\s+/', '-',$name)])}} 
          @elseif(Request::is('rest-florida/*'))
            {{route('viewAllTypeHotDeal',['id'=>$id->id,'name'=> preg_replace('/\s+/', '-',$name)])}}
          @elseif(Request::is('florida/*'))
            {{ route('viewAllTypeHotDeal',['id'=>$id->id, 'name'=> preg_replace('/\s+/','-',$name)]) }} @endif ">Hot Deals</a></li>
    		<li><a href="{{route('showAllMixItUp')}}">Mix it up</a></li>
    	</ul>
  	</div>
	<div class="container"><br>
		<div class="deal-section common-section">
			<h1 class="title"> We have @if(!empty($allTypeHotels)) {{count($allTypeHotels)}} @endif hot deals for you</h1>
    </div>

  <div class="container">
    <div class="common-section hot-deal-section">
      <div class="hotel-filter-box" style="background: white;border: 1px solid white;border-radius: 7px;">
        <form action="#" id="hotel-search-form" method="get" style="padding: 15px;">
          <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-4">
              <div class="form_feild">
                <select name="location" id="location" onchange="submitLocation()">
                  <option value="0">Anywhere in Florida</option>
                  <option value="1">Orlando</option>
                  <option value="2">Miami</option>
                  <option value="3">Florida Keys</option>
                  <option value="4">Fort Lauderdale</option>
                  <option value="5">West Palm Beach</option>
                  <option value="6">Gulf Coast</option>
                  <option value="14">Universal</option>
                </select>
                <span class="down-arrow"><i class="fa fa-chevron-down"></i></span>
              </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 filter_right">
              <div class="rating-box btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn btn-secondary active">
                  <input type="radio" name="options" id="option1" autocomplete="off" checked> All
                </label>
                <label class="btn btn-secondary ">
                  <input type="radio" name="options" id="option2" autocomplete="off"> 5 <i class="fa fa-star"></i>
                </label>
                <label class="btn btn-secondary ">
                  <input type="radio" name="options" id="option3" autocomplete="off"> 4 <i class="fa fa-star"></i>
                </label>
                <label class="btn btn-secondary ">
                  <input type="radio" name="options" id="option4" autocomplete="off"> 3 <i class="fa fa-star"></i>
                </label>
                <input type="hidden" name="rating" id="rating" value="0">
              </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4">
              <div class="form_feild">
                <form name="sortprice">
                    <select class="form-control sortpriceDrop" name="sortpricename" onchange="searchprice()">
                      <option class="sortpricedropMain" value="" selected>Sort by Recomanded</option>
                      <option class="sortpricedropMain" value="{{route('sortPrice',['name'=>'hot-deals','for'=> 'low-to-high']) }}">Price Low to High</option>
                      <option class="sortpricedropMain" value="{{route('sortPrice',['name'=>'hot-deals','for'=> 'high-to-low']) }}">Price High to Low</option>
                    </select>
                </form>
              </div>
            </div>
          </div>
        </form>
      </div>

    <div class="row hotDealListing">
      @foreach($allTypeHotels as $hd)
      <div class="col-lg-4 col-md-6 col-sm-6 cmn-deal">
        <div class="item">
          <div class="card-box">
            <div class="deal-thumb">
              <img src="{{asset('storage/'.$hd->image)}}" alt="" />
              <div class="card-info">
                <div class="fav-query bg-dark"> <span> {{ $hd->days }} nights from £ {{$hd->price}}<sub> pp </sub></span>
                </div>
              </div>
            </div>
            <div class="deal-content">
              <div class="hotel-crumb">
                <span>
                  <span>
                      @if($hd->type==1) Hotel
                      @elseif($hd->type==2)  Villas
                      @elseif($hd->type==3) Disney
                      @elseif($hd->type==4) Universal
                      @endif
                    </span>
                </span>
                @if(!empty($hd->location->name)) <span>{{$hd->location->name}}</span> @endif 
                <span>
                  @php
                    $avg_rating = $hd->rating;
                    $non_rating = 5-$hd->rating;
                  @endphp
                  @if(isset($avg_rating) && $avg_rating>0)
                    @for($i=0; $i<$avg_rating; $i++)
                      <i class="fa fa-star checked"></i>
                    @endfor
                    @for($j=0; $j<$non_rating; $j++)
                      <i class="fa fa-star"></i>
                    @endfor
                  @endif
                </span>
              </div>
              <div class="deal-header">
                <h5><a href="{{route('moreDetails',$hd->id)}}">{{ $hd->name }}</a></h5>
                <p class="more">Enjoy 2 Free Nights, Resort Amenities & Much More</p>
                <a href="{{route('moreDetails',$hd->id)}}" class="badge badge-danger">More offers available here!</a>
              </div>
              <div class="travel-info">
                <div class="travel-icon">
                  <span class="fa fa-calendar"></span>
                  <p>
                    Multiple </p>
                </div>
                <div class="travel-icon"> <span class="fa fa-plane"></span>
                  <p>LON</p>
                </div>
                <div class="travel-icon"> <span class=" price">£{{ $hd->saving }} pp</span>
                  <p>Save</p>
                </div>
              </div>
              <!--end travel-info-->
              <div class="deal-footer-btn">
                <a href="{{route('moreDetails',$hd->id)}}" class="badge badge-danger text-size20">More Details</a>
              </div>
            </div>
            <!--end deal ctn-->
          </div>
        <!--end deal-box-->
        </div>
      </div>
      @endforeach
      <div class="butoon-bottom mt-5" id='load_more'>
        {{ $allTypeHotels->links() }}
      </div>
    </div>
  </div>
</div>
</div>
</div>
@endsection