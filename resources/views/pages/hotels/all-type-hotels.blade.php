@extends('layouts.subPageBase')
@if(Request::is('rest-usa/*'))
@section('title','USA')
@elseif(Request::is('rest-florida/*'))
@section('title','FLORIDA')
@endif
@section('content')
<div class="container top-head-bgimg">
    <div class="view">
        <img src="{{asset('front_component/image/contact-banner.jpg')}}" class="img-fluid" alt="placeholder">
        <div class="mask flex-center waves-effect waves-light rgba-teal-strong">
          <p class="white-text h1">{{$name}} Hotels</p>
        </div>
    </div>
    <!--Navbar-->
    <!-- <nav class="navbar navbar-expand nav-color navbar-dark border-round">
          <ul class="navbar-nav sub-navbar mx-auto">
              <li class="nav-item active">
                  <a class="nav-link" href="hotel_view.html">Features</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#">Pricing</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#">Pricing</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#">Pricing</a>
              </li>
          </ul>
    </nav> -->
    <!--/.Navbar-->
</div><br><br>
<div class="container item-listing">
  <!--Carousel Wrapper-->
  <div class="text-center">
    <h1 class="title-row"><strong> ~Explore Floridas~ </strong></h1>
  </div>
  <div class="row">
    <!-- Card Narrower -->
    @forelse($allTypeHotels as $sAllHotl)
      <div class="card card-cascade narrower col-md-4">
          <div class="view view-cascade overlay">
            @php 
              $aGetHotelImage = json_decode($sAllHotl->hotelDet->images);
            @endphp
            {{-- @foreach($aGetHotelImage as $allimage) --}}
              <img class="card-img-top" src="{{ asset('storage/'.$aGetHotelImage[0])}}" alt="Card image cap">
            {{-- @endforeach --}}
            <a>
                <div class="mask rgba-white-slight"></div>
            </a>
          </div>
          <!-- Grid column -->
        <div>
          <nav>
            <ol class="breadcrumb pink-text">
              <li class="breadcrumb-item">
              @if($sAllHotl->type==1) Hotel
              @elseif($sAllHotl->type==2)  Villas
              @elseif($sAllHotl->type==3) Disney
              @elseif($sAllHotl->type==4) Universal
              @endif
              </li>
              <li class="breadcrumb-item">
                @if(!empty($sAllHotl->location->name)) <span>{{$sAllHotl->location->name}}</span> @endif
              </li>
              <li class="breadcrumb-item">
                @php
                $avg_rating = $sAllHotl->rating;
                $non_rating = 5-$sAllHotl->rating;
              @endphp
              @if(isset($avg_rating) && $avg_rating>0)
                @for($i=0; $i<$avg_rating; $i++)
                  <span class="fa fa-star checked"></span>
                @endfor
                @for($j=0; $j<$non_rating; $j++)
                  <span class="fa fa-star unchecked"></span>
                @endfor
              @endif
              </li>
            </ol>
          </nav>
        </div>
        <div class="card-body card-body-cascade">
          <h3 class="pink-text"><i class="fas fa-wallet"></i> From £{{$sAllHotl->price}} pp </h3>
          <h4 class="font-weight-bold card-title">{{ $sAllHotl->name }}</h4>
          <p class="card-text">
            @php 
              $facility = json_decode($sAllHotl->hotelDet->facility);
            @endphp
            @foreach($facility as $key=>$faci)
              @if($key<=3){{ $faci }}<strong> , </strong>@endif
            @endforeach ....
          </p>
          <a class="btn btn-unique" href="{{route('moreDetails',$sAllHotl->id)}}">More Details-</a>
        </div>
      </div>
    @endforeach
    <!-- Card Narrower -->
  </div>
  <div class="pagination-links">
    {{ $allTypeHotels->links() }}
  </div>
</div>
{{-- <div class="content">
	<div class="banner">
	  	<div class="centered">
	  		<h2>{{$name}} Hotels</h2>
	  	</div>
		<img src="{{asset('front_component/images/contact-banner.jpg')}}" style="height: 300px;width: 100%;" alt=""/> 
	</div>
	<div class="tab-nav">
  		<ul>
    		<li><a href="
    			@if(Request::is('rest-usa/*'))
    				{{route('viewAllUsa',['id'=>$id->id,'name'=> preg_replace('/\s+/', '-',$name)])}} 
    			@elseif(Request::is('rest-florida/*'))
    				{{route('viewAllFlorida',['id'=>$id->id,'name'=> preg_replace('/\s+/', '-',$name)])}}
    			@elseif(Request::is('florida/*'))
    				{{ route('viewSubFloridaDeta',['id'=>$id->id, 'name'=> preg_replace('/\s+/','-',$name), 'locname'=> preg_replace('/\s+/','-',$locname)]) }} @endif">Overview</a></li>
    		
          <li><a href="@if(Request::is('rest-usa/*'))
    				{{route('viewAllTypeHotels',['id'=>$id->id,'name'=> preg_replace('/\s+/', '-',$name)])}} 
    			@elseif(Request::is('rest-florida/*'))
    				{{route('viewAllTypeHotels',['id'=>$id->id,'name'=> preg_replace('/\s+/', '-',$name)])}}
    			@elseif(Request::is('florida/*'))
    				{{ route('viewAllTypeHotels',['id'=>$id->id, 'name'=> preg_replace('/\s+/','-',$name)]) }} @endif ">Hotels</a></li>
        	
          <li><a href="@if(Request::is('rest-usa/*'))
            {{route('viewAllTypeHotDeal',['id'=>$id->id,'name'=> preg_replace('/\s+/', '-',$name)])}} 
          @elseif(Request::is('rest-florida/*'))
            {{route('viewAllTypeHotDeal',['id'=>$id->id,'name'=> preg_replace('/\s+/', '-',$name)])}}
          @elseif(Request::is('florida/*'))
            {{ route('viewAllTypeHotDeal',['id'=>$id->id, 'name'=> preg_replace('/\s+/','-',$name)]) }} @endif ">Hot Deals</a></li>
    		  
          <li><a href="{{route('showAllMixItUp')}}">Mix it up</a></li>
    	</ul>
  	</div>
	<div class="container"><br>
		<div class="deal-section common-section">
			<h1 class="title"> We have @if(!empty($allTypeHotels)) {{count($allTypeHotels)}} @endif hotels for you</h1>
    </div>

  <div class="common-section">
    <div class="container">
      <div class="hotel-filter-box" style="background: white;border: 1px solid white;border-radius: 7px;">
        <form action="#" id="hotel-search-form" method="get" style="padding: 15px;">
          <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6" style="padding: 0px;">
              <ul class="nav nav-pills mb-3 rating" id="pills-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" href="{{route('viewAllTypeHotels',['id'=>$id, 'name'=>$name])}}" role="tab" aria-controls="pills-home" aria-selected="true">All Ratings</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link"  href="{{route('searchRating',['star'=>3,'for'=>$name])}}" role="tab" aria-controls="pills-home" aria-selected="true"> 3 <i class="fa fa-star"></i> </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{route('searchRating',['star'=>4,'for'=>$name])}}" role="tab" aria-controls="pills-home" aria-selected="true"> 4 <i class="fa fa-star"></i> </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{route('searchRating',['star'=>5,'for'=>$name])}}" role="tab" aria-controls="pills-home" aria-selected="true"> 5 <i class="fa fa-star"></i> </a>
                  </li>
                </ul>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6" style="padding: 0px;">
              <div class="form_feild">
                <form name="sortprice">
                    <select class="form-control sortpriceDrop" name="sortpricename" onchange="searchprice()">
                      <option class="sortpricedropMain" value="" selected>Sort by Recomanded</option>
                      <option class="sortpricedropMain" value="{{route('sortPrice',['name'=>'hot-deals','for'=> 'low-to-high']) }}">Price Low to High</option>
                      <option class="sortpricedropMain" value="{{route('sortPrice',['name'=>'hot-deals','for'=> 'high-to-low']) }}">Price High to Low</option>
                    </select>
                </form>
              </div>
            </div>
          </div>
        </form>
      </div>

      <div id="hotel-list-content">
        @forelse($allTypeHotels as $sAllHotl)
        <div class="place-cell clearfix">
          <div class="place-slider ">
            <div class="place-hotel-crumb">
            </div>
            @php 
              $aGetHotelImage = json_decode($sAllHotl->hotelDet->images);
            @endphp
            <div class="owl-carousel owl-prod">
              @foreach($aGetHotelImage as $allimage)
                <img class="owl-lazy" data-src="{{ asset('storage/'.$allimage)}}" alt="Uploading image" />
              @endforeach
            </div>
          </div>
          <div class="place-crumb">
            <div class="place-left">
              <div class="hotel-crumb">
                <span>
                  @if($sAllHotl->type==1) Hotel
                  @elseif($sAllHotl->type==2)  Villas
                  @elseif($sAllHotl->type==3) Disney
                  @elseif($sAllHotl->type==4) Universal
                  @endif
                </span>
                <span>{{$sAllHotl->location->name}}</span>
                <span>@if($sAllHotl->sublocation!=0) {{$sAllHotl->subloca->name}} @endif</span>
                <span>
                @php
                  $avg_rating = $sAllHotl->rating;
                  $non_rating = 5-$sAllHotl->rating;
                @endphp
                @if(isset($avg_rating) && $avg_rating>0)
                  @for($i=0; $i<$avg_rating; $i++)
                    <i class="fa fa-star checked"></i>
                  @endfor
                  @for($j=0; $j<$non_rating; $j++)
                    <i class="fa fa-star"></i>
                  @endfor
                @endif
              </span>
              </div>
              <h4>{{$sAllHotl->name}}</h4>
              <h5 class="more"></h5>
              <p class="more">@if(!empty($sAllHotl->hotelDet->description)) {{$sAllHotl->hotelDet->description}} @endif</p>
              
            </div>
            <div class="place-right">
              <span class="text-red">Save up to £{{$sAllHotl->saving}} pp</span><br><br>
                <div class="price">
                  <h6>{{$sAllHotl->days}} nights from </h6>
                  <h4>
                    <span>£{{$sAllHotl->price}}</span>
                    <sub> pp</sub>
                  </h4>
                  <p class="inc">Inc. Flights</p>
                </div>
                <a href="{{route('moreDetails',$sAllHotl->id)}}" class="btn btn-blue ">More Details</a>
            </div>
          </div>
        </div>
        @empty
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>No Records Found!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @endforelse
      </div>
      <div class="butoon-bottom mt-5" id='load_more'>
        {{ $allTypeHotels->links() }}
      </div>
      
    </div>
  </div>
</div> --}}
@endsection