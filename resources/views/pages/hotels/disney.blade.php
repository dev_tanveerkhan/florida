@extends('layouts.subPageBase')
@section('title','Disney')
@section('content')
<br><br>
    <div class="container destination">
      <div class="text-center">
        <h1 class="title-row text-color strong-title"><strong> ~Disney Holidays~ </strong></h1>
      </div>
      <div class="row">
        @foreach($disney as $dis)
        <div class="col-md-4">
          <div class="view desti-crd rounded zoom">
            <img src="{{asset('storage/'.$dis->image)}}" class="img-fluid rounded" alt="Sample image with waves effect." style="max-height: 200px; width: 100%;">
              <a href="{{route('disneyHolidaysHotels',['name'=>preg_replace('/\s+/','-',$dis->title),'id'=>$dis->id])}}">
                <div class="mask flex-center m-auto text-center waves-effect waves-light"> 
                  <div class="mask-txt">
                    <p class="h3 strong-title">{{$dis->title}}</p>
                    <p class="h6">Starting from £{{$dis->price}} pp</p>
                  </div>
                </div>
              </a>
          </div>
        </div>
        @endforeach
      </div>
    </div>
@endsection