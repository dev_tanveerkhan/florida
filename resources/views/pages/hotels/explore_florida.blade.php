@extends('layouts.subPageBase')
@section('title','Explore Florida')
@section('content')

<div class="content">
	<div class="banner">
	    <div class="centered"><h2>Explore Florida</h2>
	      	<a href="" class="btn btn-blue ">View Orlando Offers</a>
	    </div>
	    <img src="{{asset('front_component/images/about-bnr.jpg')}}"> 
	</div>
  
	<div class="container">
		<div class="explore-flo-list common-section">
			<h2 class="title">~~Where to stay~~</h2>
			<div class="row">
				@foreach($whereTOStay as $whereTo)
				<div class="col-lg-4 col-sm-6 col-6 same-hight">
					<div class="explore-flo-box"> 
						@foreach($whereToLoc as $werToS)
							@if($whereTo->title == $werToS->name)
								<a href="{{ route('viewAllFlorida',['id'=>$werToS->id,'name'=> preg_replace('/\s+/', '-',$werToS->name)]) }}">
							@endif
						@endforeach
							<div class="overlay centered">
								<h2>{{$whereTo->title}}</h2>
							</div>
							<img class="img-fluid" src="{{asset('storage/'.$whereTo->image)}}" alt="" />
						</a> <span class="explore-flo-price">from £{{$whereTo->price}} pp</span> </div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
<!--end explore-flo-list-->
@endsection