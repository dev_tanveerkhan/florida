@extends('layouts.subPageBase')
@section('title','Hotels')
@section('content')
<div class="container top-head-bgimg">
  <div class="view">
      <img src="{{asset('front_component/image/contact-banner.jpg')}}" class="img-fluid" alt="placeholder">
      <div class="mask flex-center waves-effect waves-light rgba-teal-strong">
        <p class="white-text h1"> Hotels In Florida </p>
      </div>
  </div>
</div><br><br>

<!-- Filter Data  -->
<div class="container filter-container">
  <div class="hotel-filter-box">
    <div class="row">
        <div class="col-sm-12 col-md-4 col-lg-4">
            <form name="filterForm">
                <select class="form-control" name="filterSelect" onchange="searchLocation()">
                  <option value="" selected>~~ SELECT LOCATION ~~</option>
                  <option value="{{route('searchLocation',['name'=>'hotels','for'=> 'florida','id'=>'hotels']) }}">Anywhere in Florida</option>
                  @foreach($location as $nameLoc)
                    <option {{Request::is('search/hot-deals/') ? 'selected' : ''}} value="{{route('searchLocation',['name'=>'hotels','for'=> preg_replace('/\s+/', '-' ,strtolower($nameLoc->location->name) ),'locId'=> $nameLoc->location_id])}}"> {{ $nameLoc->location->name }} </option>
                  @endforeach
                </select>
            </form>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4">
          <ul class="nav nav-pills mb-3 rating font-weight-bold" id="pills-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link {{Request::is('hotels') ? 'bg-color' : ''}}" href="{{route('showAllHotels')}}" role="tab" aria-controls="pills-home" aria-selected="true">All Ratings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{Request::is('search-rating/3/*') ? 'bg-color' : ''}}"  href="{{route('searchRating',['star'=>3,'for'=>"hotels"])}}" role="tab" aria-controls="pills-home" aria-selected="true"> 3 <i class="fa fa-star"></i> </a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{Request::is('search-rating/4/*') ? 'bg-color' : ''}}" href="{{route('searchRating',['star'=>4,'for'=>"hotels"])}}" role="tab" aria-controls="pills-home" aria-selected="true"> 4 <i class="fa fa-star"></i> </a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{Request::is('search-rating/5/*') ? 'bg-color' : ''}}" href="{{route('searchRating',['star'=>5,'for'=>"hotels"])}}" role="tab" aria-controls="pills-home" aria-selected="true"> 5 <i class="fa fa-star"></i> </a>
            </li>
          </ul>
        </div>
      <div class="col-sm-12 col-md-4 col-lg-4">
        <form name="sortprice">
            <select class="form-control sortpriceDrop" name="sortpricename" onchange="searchprice()">
              <option class="sortpricedropMain" value="" selected>Sort by Recomanded</option>
              <option class="sortpricedropMain" value="{{route('sortPrice',['name'=>'hotels','for'=> 'low-to-high']) }}">Price Low to High</option>
              <option class="sortpricedropMain" value="{{route('sortPrice',['name'=>'hotels','for'=> 'high-to-low']) }}">Price High to Low</option>
            </select>
        </form>
      </div>
    </div>
  </div>
</div>
<br><br>

<div class="container">
  <!--Carousel Wrapper-->
  <div class="text-center">
    <h1 class="title-row text-color strong-title"><strong> ~Explore Floridas~ </strong></h1>
    <h6 class="text-danger text-center font-weight-small {{ Request::is('search/hotels/*')? 'show': 'hide' }}">{{ count($showAllHotels) }} Searched Result Found</h6>
  </div>
  <div class="row m-0">
    <!-- Card Narrower -->
    @forelse($showAllHotels as $sAllHotl)
    <div class="col-md-4">
      <div class="card card-cascade narrower">
        @php 
          $aGetHotelImage = json_decode($sAllHotl->hotelDet->images);
        @endphp
        <div class="view view-cascade overlay">
        <div class="owl-carousel owl-card-img-slider">
          @foreach($aGetHotelImage as $allimage)
              <img class="card-img-top" src="{{ asset('storage/'.$allimage)}}" alt="Card image cap">
            @endforeach
        </div>
          <a><div class="mask rgba-white-slight"></div></a>
        </div>
        <!-- Grid column -->
        <div>
          <nav>
            <ol class="breadcrumb text-color">
              <li class="breadcrumb-item">
              @if($sAllHotl->type==1) Hotel
              @elseif($sAllHotl->type==2)  Villas
              @elseif($sAllHotl->type==3) Disney
              @elseif($sAllHotl->type==4) Universal
              @endif
              </li>
              <li class="breadcrumb-item">
                @if(!empty($sAllHotl->location->name)) <span>{{$sAllHotl->location->name}}</span> @endif
              </li>
              <li class="breadcrumb-item">
                @php
                $avg_rating = $sAllHotl->rating;
                $non_rating = 5-$sAllHotl->rating;
              @endphp
              @if(isset($avg_rating) && $avg_rating>0)
                @for($i=0; $i<$avg_rating; $i++)
                  <span class="fa fa-star checked"></span>
                @endfor
                @for($j=0; $j<$non_rating; $j++)
                  <span class="fa fa-star unchecked"></span>
                @endfor
              @endif
              </li>
            </ol>
          </nav>
        </div>
        <div class="card-body card-body-cascade">
          <h3 class="text-color-red strong-title"><i class="fas fa-wallet"></i> From £{{$sAllHotl->price}} pp </h3>
          <h4 class="font-weight-bold card-title">{{ $sAllHotl->name }}</h4>
          <p class="card-text">
            @php 
              $facility = json_decode($sAllHotl->hotelDet->facility);
            @endphp
            @foreach($facility as $key=>$faci)
              @if($key<=3){{ $faci }}<strong> , </strong>@endif
            @endforeach ....
          </p>
          <a class="btn bg-color text-white rounded" href="{{route('moreDetails',$sAllHotl->id)}}">More Details-</a>
        </div>
      </div>
      </div>
    @endforeach
    <!-- Card Narrower -->
  </div>
  <div class="pagination-links">
    
  </div>
</div><br><br>

<div class="container card-slider">
  <!--Carousel Wrapper-->
  <div class="text-center">
    <h1 class="title-row text-color strong-title"><strong> ~ Hot Deals ~ </strong></h1>
  </div>
  <div class="owl-carousel owl-theme">
      @foreach($hotDeals as $favsty)
      <div class="item">
        <div class="card card-cascade narrower">
        <div class="view view-cascade overlay">
          <img class="card-img-top" src="{{asset('storage/'.$favsty->image)}}"
            alt="Card image cap">
          <a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
        <!-- Grid column -->
        <div>
          <nav>
            <ol class="breadcrumb text-color">
              <li class="breadcrumb-item">
                @if($favsty->type==1) Hotel
              @elseif($favsty->type==2)  Villas
              @elseif($favsty->type==3) Disney
              @elseif($favsty->type==4) Universal
              @endif
              </li>
              <li class="breadcrumb-item">
                @if(!empty($favsty->location->name)) <span>{{$favsty->location->name}}</span> @endif
              </li>
              <li class="breadcrumb-item">
                @php
                $avg_rating = $favsty->rating;
                $non_rating = 5-$favsty->rating;
              @endphp
              @if(isset($avg_rating) && $avg_rating>0)
                @for($i=0; $i<$avg_rating; $i++)
                  <span class="fa fa-star checked"></span>
                @endfor
                @for($j=0; $j<$non_rating; $j++)
                  <span class="fa fa-star unchecked"></span>
                @endfor
              @endif
              </li>
            </ol>
          </nav>
        </div>
        <div class="card-body card-body-cascade">
          <h3 class="text-color-red strong-title"><i class="fas fa-wallet"></i> From £{{$favsty->price}} pp </h3>
          <h4 class="font-weight-bold card-title">{{ $favsty->name }}</h4>
          <p class="card-text">
            @php 
            $facility = json_decode($favsty->hotelDet->facility);
          @endphp
          @foreach($facility as $key=>$faci)
            @if($key<=3){{ $faci }}<strong> , </strong>@endif
          @endforeach ....
        </p>
          <a class="btn bg-color text-white rounded" href="{{route('moreDetails',$favsty->id)}}">More Details-</a>
        </div>
      </div>
      <!-- Card Narrower -->
      </div>
      @endforeach
  </div>
  <div class="flex-center m-auto">
      <a href="{{ route('showAllHotDeal') }}" class="btn bg-color text-white rounded">View all our Hot Deals</a>
  </div>
</div>
@endsection