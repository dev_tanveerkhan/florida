@extends('layouts.subPageBase')
@section('title','Disney')
@section('content')
	<div class="content">
		<div class="banner disney-bnr">
		  	<div class="centered">
		  		<h2>Mix It Up</h2>
		  	</div>
			<img src="{{asset('front_component/images/contact-banner.jpg')}}" style="max-height: 300px;width: 100%;" alt=""/> 
		</div>
		<div class="container">
			<div class="common-section mixit">
	      		<div class="hotel-filter-box" style="background: white;border: 1px solid white;border-radius: 7px;">
	        	<form action="#" id="hotel-search-form" method="get" style="padding: 15px;">
		          	<div class="row">
			            <div class="col-sm-12 col-md-4 col-lg-4">
			              <div class="form_feild">
			                <select name="location" id="location" onchange="submitLocation()">
			                  <option value="0">Anywhere in Florida</option>
			                  <option value="1">Orlando</option>
			                  <option value="2">Miami</option>
			                  <option value="3">Florida Keys</option>
			                  <option value="4">Fort Lauderdale</option>
			                  <option value="5">West Palm Beach</option>
			                  <option value="6">Gulf Coast</option>
			                  <option value="14">Universal</option>
			                </select>
			                <span class="down-arrow"><i class="fa fa-chevron-down"></i></span>
			              </div>
			            </div>
			            <div class="col-sm-12 col-md-4 col-lg-4 filter_right">
			              <div class="rating-box btn-group btn-group-toggle" data-toggle="buttons">
			                <label class="btn btn-secondary active">
			                  <input type="radio" name="options" id="option1" autocomplete="off" checked> All
			                </label>
			                <label class="btn btn-secondary ">
			                  <input type="radio" name="options" id="option2" autocomplete="off"> 5 <i class="fa fa-star"></i>
			                </label>
			                <label class="btn btn-secondary ">
			                  <input type="radio" name="options" id="option3" autocomplete="off"> 4 <i class="fa fa-star"></i>
			                </label>
			                <label class="btn btn-secondary ">
			                  <input type="radio" name="options" id="option4" autocomplete="off"> 3 <i class="fa fa-star"></i>
			                </label>
			                <input type="hidden" name="rating" id="rating" value="0">
			              </div>
			            </div>
			            <div class="col-sm-12 col-md-4 col-lg-4 filter_right">
			              <div class="form_feild">
			                <form name="sortprice">
			                    <select class="form-control sortpriceDrop" name="sortpricename" onchange="searchprice()">
			                      <option class="sortpricedropMain" value="" selected>Sort by Recomanded</option>
			                      <option class="sortpricedropMain" value="{{route('sortPrice',['name'=>'hot-deals','for'=> 'low-to-high']) }}">Price Low to High</option>
			                      <option class="sortpricedropMain" value="{{route('sortPrice',['name'=>'hot-deals','for'=> 'high-to-low']) }}">Price High to Low</option>
			                    </select>
			                </form>
			              </div>
			            </div>
		          	</div>
	        	</form>
	    		</div>
	    		<div id="mixItUpListing">
	    			@foreach($mixItUp as $getData)
	                <div class="place-cell clearfix">
	            		<div class="place-slider ">
	              			<div class="owl-carousel owl-prod">
	                        	@php 
					             	$aGetHotelImage = json_decode($getData->fHotelDet->images);
					            @endphp
					            @foreach($aGetHotelImage as $imageVal)
	                        	<div class="item">
			                        <div class="mix-it-up-caption">
			                          <div class="mix-it-up-text">
			                            <p>{{ $getData->fHotel->name }}</p>
			                          </div>
			                        </div>
	                                <img src="{{asset('storage/'.$imageVal)}}" alt=""/>
	                    		</div>
	                    		@endforeach

	                    		@php 
					             	$sGetHotelImage = json_decode($getData->sHotelDet->images);
					            @endphp
					            @foreach($sGetHotelImage as $simageVal)
	                        	<div class="item">
			                        <div class="mix-it-up-caption">
			                          <div class="mix-it-up-text">
			                            <p>{{ $getData->sHotel->name }}</p>
			                          </div>
			                        </div>
	                                <img src="{{asset('storage/'.$simageVal)}}" alt=""/>
	                    		</div>
	                    		@endforeach
	                    	</div>
	            		</div>
	            		<!--end place-sliderer-->
	            
			            <div class="place-crumb">
			              	<div class="place-left"> 
			                	<div class="hotel-crumb">
				                	<span></span>
			                	</div>
			                	<h4>{{ $getData->name }}</h4>
			                	<div class="hotel-crumb-list">
				                  	<figure>
				                        <img src="{{asset('storage/'.$getData->locationId->image)}}" alt="">
				                  	</figure>
			                  		<h6>
				                  		<span>
				                  			@if($getData->fhotel->type==1) Hotel
							                @elseif($getData->fhotel->type==2)  Villas
							                @elseif($getData->fhotel->type==3) Disney
							                @elseif($getData->fhotel->type==4) Universal
							                @endif
				                  		</span>
				                  		<span>
											@if(!empty($getData->location_id)) {{$getData->locationId->name}} @endif -
				                  			@if(!empty($getData->sublocation_id)) {{$getData->sublocationId->name}} @endif
				                  		</span><br> 
				                  		<span>
							              @php
							                $avg_rating = $getData->fhotel->rating;
							                $non_rating = 5-$getData->fhotel->rating;
							              @endphp
							              @if(isset($avg_rating) && $avg_rating>0)
							                @for($i=0; $i<$avg_rating; $i++)
							                  <i class="fa fa-star checked"></i>
							                @endfor
							                @for($j=0; $j<$non_rating; $j++)
							                  <i class="fa fa-star"></i>
							                @endfor
							              @endif
							            </span>
				                  	</h6>
			                 	 	<h5>{{ $getData->fhotel->name }}</h5>
			                	</div>
			                	<div class="hotel-crumb-list">
				                  	<figure>
				                        <img src="{{asset('storage/'.$getData->slocationId->image)}}" alt="">
				                  	</figure>
			                  		<h6>
				                  		<span>
				                  			@if($getData->shotel->type==1) Hotel
							                @elseif($getData->shotel->type==2)  Villas
							                @elseif($getData->shotel->type==3) Disney
							                @elseif($getData->shotel->type==4) Universal
							                @endif
				                  		</span>
				                  		<span>
											@if(!empty($getData->s_location_id)) {{$getData->slocationId->name}} @endif -
				                  			@if(!empty($getData->s_sublocation_id)) {{$getData->ssublocationId->name}} @endif
				                  		</span>
				                  		<span><br>
							              @php
							                $avg_rating = $getData->shotel->rating;
							                $non_rating = 5-$getData->shotel->rating;
							              @endphp
							              @if(isset($avg_rating) && $avg_rating>0)
							                @for($i=0; $i<$avg_rating; $i++)
							                  <i class="fa fa-star checked"></i>
							                @endfor
							                @for($j=0; $j<$non_rating; $j++)
							                  <i class="fa fa-star"></i>
							                @endfor
							              @endif
							            </span> 
				                  	</h6>
			                 	 	<h5>{{ $getData->shotel->name }}</h5>
			                	</div>
			              	</div>
			              	<div class="place-right"> 
			              		<span class="text-red">Save up to £{{ $getData->save }} pp</span><br><br>
			                	  	<div class="price">
				                    	<h6>{{ $getData->nights }} nights from</h6>
				                    	<h4> <span>£{{ $getData->price }}</span><sub> pp</sub></h4>
				                    	<p class="inc">Inc. Flights</p>
				                  	</div>
			                  		<a href="{{route('moreMixItDetail',$getData->id)}}" class="btn btn-blue ">More Details</a> 
			                  	
			              	</div><!--end place-bottom--> 
			            </div><!--end place-crumb--> 
	          		</div>
	          		@endforeach
	       		</div>
	       		{{$mixItUp->links()}}
	   		</div>
		</div>
	</div>
@endsection