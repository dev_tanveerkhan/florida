@extends('layouts.subPageBase')
@section('title','Details')
@section('content')
<br><br>
<div class="content">
  <div class="container common-section">
    <div class="mix-it-up-section clearfix">
      <div class="row">
        <div class="mix-it-up-section-left col-md-8">
          	<div class="hotel-crumb"> 
	          	<span>
	          		@if($mixItUp->fhotel->type==1) Hotel
	                @elseif($mixItUp->fhotel->type==2)  Villas
	                @elseif($mixItUp->fhotel->type==3) Disney
	                @elseif($mixItUp->fhotel->type==4) Universal
	                @endif
	          	</span> 
	          	@if(!empty($getData->location_id)) <span>{{$getData->locationId->name}}</span> @endif 
	          	@if(!empty($getData->sublocation_id))<span> {{$getData->sublocationId->name}}</span>@endif 
	            <span>
                @php
                  $avg_rating = $mixItUp->fhotel->rating;
                  $non_rating = 5-$mixItUp->fhotel->rating;
                @endphp
                @if(isset($avg_rating) && $avg_rating>0)
                  @for($i=0; $i<$avg_rating; $i++)
                    <i class="fa fa-star checked"></i>
                  @endfor
                  @for($j=0; $j<$non_rating; $j++)
                    <i class="fa fa-star"></i>
                  @endfor
                @endif
              </span>
        	</div>
	        <h2 class="title title-md title-left">
	        	{{$mixItUp->name}}
	        </h2>
        </div>
      </div>
    </div>
    <!--end mix-it-up-section-->
    
    <div class="row">
      <div class="col-md-8 mix-it-up-det-left">
        <div class="mix-it-up-det-slider">
          <div class="place-hotel-crumb"> </div>
          	<div class="owl-carousel owl-prod">
               @php 
	             	$aGetHotelImage = json_decode($mixItUp->fHotelDet->images);
	            @endphp
	            @foreach($aGetHotelImage as $imageVal)
	        	<div class="item">
	                <div class="mix-it-up-caption">
	                  <div class="mix-it-up-text">
	                    <p>{{ $mixItUp->fHotel->name }}</p>
	                  </div>
	                </div>
	                <img src="{{asset('storage/'.$imageVal)}}" alt=""/>
	    		</div>
	    		@endforeach
	    		@php 
	             	$sGetHotelImage = json_decode($mixItUp->sHotelDet->images);
	            @endphp
	            @foreach($sGetHotelImage as $simageVal)
            	<div class="item">
                    <div class="mix-it-up-caption">
                      <div class="mix-it-up-text">
                        <p>{{ $mixItUp->sHotel->name }}</p>
                      </div>
                    </div>
                    <img src="{{asset('storage/'.$simageVal)}}" alt=""/>
        		</div>
        		@endforeach
			</div>
        </div>
        <!--end mix-it-up-det-sliderer-->
        <div class="accordion cust-accord" id="accordionExample">
          <div class="card cust-card">
            <div class="card-body">
              <div class="bookin">
                <figure>
                  <h2>{{ $mixItUp->fh_night }}</h2>
                  <span>Nights</span>
                </figure>
                <h6 class="hotel-crumb">
                	<span>
		          		@if($mixItUp->fhotel->type==1) Hotel
		                @elseif($mixItUp->fhotel->type==2)  Villas
		                @elseif($mixItUp->fhotel->type==3) Disney
		                @elseif($mixItUp->fhotel->type==4) Universal
		                @endif
		          	</span> 
		          	@if(!empty($mixItUp->location_id)) <span>{{$mixItUp->locationId->name}}</span> @endif 
		          	@if(!empty($mixItUp->sublocation_id))<span> {{$mixItUp->sublocationId->name}}</span>@endif 
		            <span>
                  @php
                    $avg_rating = $mixItUp->fhotel->rating;
                    $non_rating = 5-$mixItUp->fhotel->rating;
                  @endphp
                  @if(isset($avg_rating) && $avg_rating>0)
                    @for($i=0; $i<$avg_rating; $i++)
                      <i class="fa fa-star checked"></i>
                    @endfor
                    @for($j=0; $j<$non_rating; $j++)
                      <i class="fa fa-star"></i>
                    @endfor
                  @endif
                </span>
		        </h6>
                <h2>{{ $mixItUp->fhotel->name }}</h2>
                
               </div>
            </div>
          </div>
          <div class="card cust-card">
         	<div class="card-header top-card">
              <p>{{ $mixItUp->desc_title }}</p>
            </div>       
            <div class="card-body">
            	<p>{{ $mixItUp->description }}</p>
            </div>
          </div>
			
          <div class="card">
            <div class="card-header" id="headingOne">
              <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> Facilities </button>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body">
                <ul class="card-list">
                    @php 
			          	$aGetHotelFacility = json_decode($mixItUp->fhotelDet->facility);
			        @endphp
			        @foreach($aGetHotelFacility as $facility)
		            	<li><span class="fa fa-check"></span>{{$facility}}</li>
		            @endforeach
                </ul>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> Map View </button>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
              <div class="card-body">
                <div class="card-map">
                  <div style="width: 100%"><iframe width="100%" height="600" src="https://maps.google.com/maps?{{ $mixItUp->fHotel->map_view }}" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/map-my-route/">Draw map route</a></iframe></div><br />                
              	</div>
              </div>
            </div>
          </div>

          <div class="card">
              <div class="card-header" id="headingFour">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour"> About Disney Moderate Resort </button>
              </div>
              <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                <div class="card-body">
                  <div class="card-map">
                    <p>{{ $mixItUp->fhotelDet->description }}</p>
                  </div>
                </div>
              </div>
            </div>
         </div>
		  
		  
  		  <div class="accordion cust-accord" id="accordionExample">
          <div class="card cust-card">
            <div class="card-body">
              <div class="bookin">
                <figure>
                  <h2>{{ $mixItUp->sh_night }}</h2>
                  <span>Nights</span>
                </figure>
                <h6 class="hotel-crumb">
                	<span>
		          		@if($mixItUp->shotel->type==1) Hotel
		                @elseif($mixItUp->shotel->type==2)  Villas
		                @elseif($mixItUp->shotel->type==3) Disney
		                @elseif($mixItUp->shotel->type==4) Universal
		                @endif
		          	</span> 
		          	@if(!empty($mixItUp->s_location_id)) <span>{{$mixItUp->slocationId->name}}</span> @endif 
		          	@if(!empty($mixItUp->s_sublocation_id))<span> {{$mixItUp->ssublocationId->name}}</span>@endif 
		            <span>
                    @php
                      $avg_rating = $mixItUp->shotel->rating;
                      $non_rating = 5-$mixItUp->shotel->rating;
                    @endphp
                    @if(isset($avg_rating) && $avg_rating>0)
                      @for($i=0; $i<$avg_rating; $i++)
                        <i class="fa fa-star checked"></i>
                      @endfor
                      @for($j=0; $j<$non_rating; $j++)
                        <i class="fa fa-star"></i>
                      @endfor
                    @endif
                  </span>
                </h6>
                <h2>{{ $mixItUp->shotel->name }}</h2>
                                      
             </div>
            </div>
          </div>
          <div class="card cust-card">
            <div class="card-body">
              <p>{{ $mixItUp->description }}</p>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingOne">
              <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> Facilities </button>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body">
                <ul class="card-list">
                	@php 
			          	$aGetHotelFacility = json_decode($mixItUp->shotelDet->facility);
			        @endphp
			        @foreach($aGetHotelFacility as $facility)
		            	<li><span class="fa fa-check"></span>{{$facility}}</li>
		            @endforeach
                </ul>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> What’s nearby? </button>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
              <div class="card-body">
                <ul class="card-list">
                	@php 
			          	$aGetHotelNearBy = json_decode($mixItUp->shotelDet->near_by);
			        @endphp
			        @foreach($aGetHotelNearBy as $nearBy)
		            	<li><span class="fa fa-check"></span>{{$nearBy}}</li>
		            @endforeach
                </ul>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> Map View </button>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
              <div class="card-body">
                <div class="card-map">
                  <div style="width: 100%"><iframe width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="">Create a route on google maps</a></iframe></div><br />                </div>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card-header" id="headingFour">
              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour"> About {{ $mixItUp->shotel->name }} </button>
            </div>
            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
              <div class="card-body">
                <div class="card-map">
                  <p>{{ $mixItUp->shotelDet->description }}</p>
                </div>
              </div>
            </div>
          </div>
         
        </div>
      </div>
      
      <!--end mix-it-up-det-left-->
      
      <div class="col-md-4 mix-it-up-detail-right right-bar">
        <div class="mix-it-up-offer">
          <div class="offer-price price">
            <h6>{{ $mixItUp->sh_night + $mixItUp->fh_night }} nights from</h6>
            <h2><span>£{{ $mixItUp->price }}</span><sub> pp</sub></h2>
            <p>Save up to £{{ $mixItUp->save }} pp</p>
            <a href="{{route('contactUs')}}" class="btn btn-blue ">Enquire now</a>
            <p>You wont be charged yet</p>
          </div>
        </div>
        <div class="widget-holder">
          <div class="widget-listing">
            <ul>
              	@php 
              		$adepartureDate = json_decode($mixItUp->shotelDet->departure_date)
              	@endphp
              	@foreach($adepartureDate as $dateVal)
	              	<li>
	              		<span>Departure Date</span> {{ date("jS F Y", strtotime($dateVal)) }}
	             	</li>
              	@endforeach
              <li><span>Hotel Nights</span> {{  $mixItUp->sh_night + $mixItUp->fh_night  }} Nights</li>
              <li><span>Board</span>{{ $mixItUp->shotelDet->board }}</li>
              <li><span>Guests</span> {{ $mixItUp->shotelDet->guests }}</li>
            </ul>
          </div>
          <div class="widget-request"> <a href="{{route('contactUs')}}" class="btn btn-blue ">Request this holiday</a>
            <p>You wont be charged yet</p>
          </div>
        </div>
        <!--end widget-holder--> 
        
      </div>
      <!--end mix-it-up-detail-right--> 
    </div>
    <!--end row--> 
    
  </div>
  <!--end common-section--> 
</div>
@endsection