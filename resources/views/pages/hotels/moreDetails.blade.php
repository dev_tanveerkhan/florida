@extends('layouts.subPageBase')
@section('title','Details')
@section('content')
<br><br>
<div class="container more-det-section">
	<div class="text-color text-left">
		<h1>{{$hotel->name}}</h1>
	</div>
	<div>
		<nav>
            <ol class="breadcrumb text-color">
              <li class="breadcrumb-item">
              @if($hotel->type==1) Hotel
              @elseif($hotel->type==2)  Villas
              @elseif($hotel->type==3) Disney
              @elseif($hotel->type==4) Universal
              @endif
              </li>
              <li class="breadcrumb-item">
                @if(!empty($hotel->location->name)) <span>{{$hotel->location->name}}</span> @endif
              </li>
              <li class="breadcrumb-item">
                @php
                $avg_rating = $hotel->rating;
                $non_rating = 5-$hotel->rating;
              @endphp
              @if(isset($avg_rating) && $avg_rating>0)
                @for($i=0; $i<$avg_rating; $i++)
                  <span class="fa fa-star checked"></span>
                @endfor
                @for($j=0; $j<$non_rating; $j++)
                  <span class="fa fa-star unchecked"></span>
                @endfor
              @endif
              </li>
            </ol>
        </nav>
	</div>
    @php 
      $aGetHotelImage = json_decode($hotel->hotelDet->images);
    @endphp
  	<div class="view view-cascade overlay">
        <div class="owl-carousel owl-card-img-slider">
        	@foreach($aGetHotelImage as $allimage)
          		<img class="card-img-top" src="{{ asset('storage/'.$allimage)}}" alt="Card image cap">
          	@endforeach
      	</div>
    	<a><div class="mask rgba-white-slight"></div></a>
  	</div><br>
    <div class="text-color text-left">
		<h3>More Details about {{$hotel->name}}:</h3>
	</div>
    <!--  ------- More Details about hotel --------  -->
	<div class="row det-collapse">
		<div class="col-md-9">
			<div class="card cust-card">
	            <div class="card-body desc-text-color">
	    			<p>{{$hotel->hotelDet->description}}</p>
	          	</div>
	        </div>
			<!--Accordion wrapper-->
			<div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
			  <!-- Accordion card -->
			  <div class="card">
			    <!-- Card header -->
			    <div class="card-header" role="tab" id="headingOne1">
			      <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true"
			        aria-controls="collapseOne1">
			        <h5 class="mb-0 text-color">
			          Facilities <i class="fas fa-angle-down rotate-icon"></i>
			        </h5>
			      </a>
			    </div>
			    <!-- Card body -->
			    <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx">
			      <div class="card-body desc-text-color">
			        @php 
			          	$aGetHotelFacility = json_decode($hotel->hotelDet->facility);
			        @endphp
			        @foreach($aGetHotelFacility as $facility)
                    	<span class="fa fa-check"> </span> {{$facility}} &nbsp;&nbsp;
                    @endforeach
			      </div>
			    </div>
			  </div>
			  <!-- Accordion card -->
			  <!-- Accordion card -->
			  <div class="card">
			    <!-- Card header -->
			    <div class="card-header" role="tab" id="headingTwo2">
			      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
			        aria-expanded="false" aria-controls="collapseTwo2">
			        <h5 class="mb-0 text-color">
			          What’s nearby? <i class="fas fa-angle-down rotate-icon"></i>
			        </h5>
			      </a>
			    </div>
			    <!-- Card body -->
			    <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2" data-parent="#accordionEx">
			      <div class="card-body desc-text-color">
			        @php 
			          	$aGetHotelnear_by = json_decode($hotel->hotelDet->near_by);
			        @endphp
			        @foreach($aGetHotelnear_by as $facility)
                    	<span class="fa fa-check"></span> {{$facility}}  &nbsp;&nbsp;
                    @endforeach
			      </div>
			    </div>
			  </div>
			  <!-- Accordion card -->
			  <!-- Accordion card -->
			  <div class="card">
			    <!-- Card header -->
			    <div class="card-header" role="tab" id="headingThree3">
			      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3"
			        aria-expanded="false" aria-controls="collapseThree3">
			        <h5 class="mb-0 text-color">
			           Map View <i class="fas fa-angle-down rotate-icon"></i>
			        </h5>
			      </a>
			    </div>
			    <!-- Card body -->
			    <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3" data-parent="#accordionEx">
			      <div class="card-body desc-text-color">
			        <div style="width: 100%; height: 300px;"><iframe width="100%" height="600" src="" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="">Create a route on google maps</a></iframe></div><br/>
			      </div>
			    </div>
			  </div>
			  <!-- Accordion card -->
			  <!-- Accordion card -->
			  <div class="card">
			    <!-- Card header -->
			    <div class="card-header" role="tab" id="headingThree3">
			      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseFour4"
			        aria-expanded="false" aria-controls="collapseFour4">
			        <h5 class="mb-0 text-color">
			           About @if(!empty($hotel->sublocation)) {{$hotel->subLoca->name}} @else {{$hotel->location->name}} @endif <i class="fas fa-angle-down rotate-icon"></i>
			        </h5>
			      </a>
			    </div>
			    <!-- Card body -->
			    <div id="collapseFour4" class="collapse" role="tabpanel" aria-labelledby="headingThree3" data-parent="#accordionEx">
			      <div class="card-body desc-text-color">
			      	{{$hotel->location->description}}
			      </div>
			    </div>
			  </div>
			  <!-- Accordion card -->
			</div>
			<!-- Accordion wrapper -->
		</div>
		<div class="col-md-3">
			<div class="card card-body bg-color text-white">
				<div class="card-header text-center">
		          	<h6>{{$hotel->days}} nights from</h6>
		          	<h2><span>£{{$hotel->price}}</span><sub> pp</sub></h2>
					<h6>Save up to £{{$hotel->saving}} pp</h6>	
					<h6>Inc. Flights</h6> 
		        </div>
	        	<a href="{{route('contactUs')}}" class="btn bg-btn-color text-white">Enquire now</a>
	        </div>
		</div>
	</div>    
</div>
@endsection