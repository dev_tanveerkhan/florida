@extends('layouts.subPageBase')
@section('title','Parks & Passes')
@section('content')
<div class="container top-head-bgimg">
    <div class="view zoom">
        <img src="{{asset('front_component/image/contact-banner.jpg')}}" class="img-fluid" alt="placeholder">
        <div class="mask flex-center waves-effect waves-light">
          <p class="white-text h1 strong-title">Park and Passes</p>
        </div>
    </div>
</div>
<br><br>
<div class="container parksanticket">
  @foreach($parkTicket as $pt)
    <div class="park-main-image">
          <img src="{{asset('storage/'.$pt->image)}}" class="img-fluid" alt="Park and ticket image">
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="card card-body bg-color text-white">
          <div class="card-header text-center">
            <h6>{{$pt->days}} days access from </h6>
                <h2 class="strong-title"><span>£{{$pt->price}}</span><sub>pp</sub></h2>
                <p>{{ substr($pt->title,0,20) }} for 0{{$pt->no_of_passes}} Ultimate Pass</p>
                <a href="{{route('contactUsRef',['ref'=>$pt->id,'name'=>'parks'])}}" class="btn btn-blue ">Enquire</a>
                @if(!empty($pt->location) && $pt->location=='universal')
                  <a href="{{route('universalHoliday')}}" class="btn btn-red ">View Our Universal Holidays</a> 
                @elseif(!empty($pt->location) && $pt->location=='disney')
                  <a href="{{route('disneyHoidays')}}" class="btn btn-red ">View Our Disney Holidays</a> 
                @else
                  <a href="{{route('showAllHotels')}}" class="btn btn-red ">View Our {{ $pt->location }} Holidays</a> 
                @endif
          </div>
        </div>
      </div>
      <div class="col-md-8">
        <div class="strong-title p-2 text-left">
          <h2>{{$pt->title}}</h2>
        </div>
        <div class="p-2 text-left">
          <p>{{$pt->description}}</p>
        </div>
        <div class="whatsinclude-box">
          <p class="strong-title">What's Include</p>
          <div class="row whatsinclude-sub">
            @php
              $whatin = json_decode($pt->what_included);
            @endphp
            @foreach($whatin as $key=>$whatinVal)
              <div class="col-md-3 h6">{{$whatinVal}}</div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  @endforeach
  {{ $parkTicket->links() }}
</div>
@endsection