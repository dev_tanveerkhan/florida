@extends('layouts.subPageBase')
@section('title','Universal')
@section('content')
<div class="container top-head-bgimg">
    <div class="view zoom">
        <img src="{{asset('front_component/image/disney-bnr.jpg')}}" class="img-fluid" alt="placeholder">
        <div class="mask flex-center waves-effect waves-light">
          <p class="white-text h1 strong-title">Universal Holidays</p>
        </div>
    </div>
</div>

<br><br>
<div class="container item-listing">
  <!--Carousel Wrapper-->
  <div class="row m-0">
    <!-- Card Narrower -->
    @forelse($universal as $sAllHotl)
    <div class="col-md-4">
      <div class="card card-cascade narrower">
        @php 
          $aGetHotelImage = json_decode($sAllHotl->hotelDet->images);
        @endphp
        <div class="view view-cascade overlay">
        <div class="owl-carousel owl-card-img-slider">
          @foreach($aGetHotelImage as $allimage)
              <img class="card-img-top" src="{{ asset('storage/'.$allimage)}}" alt="Card image cap">
            @endforeach
        </div>
          <a><div class="mask rgba-white-slight"></div></a>
        </div>
        <!-- Grid column -->
        <div>
          <nav>
            <ol class="breadcrumb text-color">
              <li class="breadcrumb-item">
              @if($sAllHotl->type==1) Hotel
              @elseif($sAllHotl->type==2)  Villas
              @elseif($sAllHotl->type==3) Disney
              @elseif($sAllHotl->type==4) Universal
              @endif
              </li>
              <li class="breadcrumb-item">
                @if(!empty($sAllHotl->location->name)) <span>{{$sAllHotl->location->name}}</span> @endif
              </li>
              <li class="breadcrumb-item">
                @php
                $avg_rating = $sAllHotl->rating;
                $non_rating = 5-$sAllHotl->rating;
              @endphp
              @if(isset($avg_rating) && $avg_rating>0)
                @for($i=0; $i<$avg_rating; $i++)
                  <span class="fa fa-star checked"></span>
                @endfor
                @for($j=0; $j<$non_rating; $j++)
                  <span class="fa fa-star unchecked"></span>
                @endfor
              @endif
              </li>
            </ol>
          </nav>
        </div>
        <div class="card-body card-body-cascade">
          <h3 class="text-color-red strong-title"><i class="fas fa-wallet"></i> From £{{$sAllHotl->price}} pp </h3>
          <h4 class="font-weight-bold card-title">{{ $sAllHotl->name }}</h4>
          <p class="card-text">
            @php 
              $facility = json_decode($sAllHotl->hotelDet->facility);
            @endphp
            @foreach($facility as $key=>$faci)
              @if($key<=3){{ $faci }}<strong> , </strong>@endif
            @endforeach ....
          </p>
          <a class="btn bg-color text-white rounded" href="{{route('moreDetails',$sAllHotl->id)}}">More Details-</a>
        </div>
      </div>
      </div>
    @endforeach
    <!-- Card Narrower -->
  </div>
  <div class="pagination-links">
    
  </div>
</div>
@endsection