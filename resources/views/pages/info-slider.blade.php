<div class="title" style="padding-top:20px;">
	<h4>For The Joy  of Florida</h4>
</div>
<div id="scroll-mouse" class="common-section" style="background:#e3e9ef; padding: 20px; margin-bottom:20px; ">
	<div class="owl-carousel owl-caro">
		@foreach($SliderInfo as $slider)
		<div class="item card" style="height: 300px; padding-top: 15px;">
			<div class="caro-box">
				<a href="javascript:void(0)">
					<span>
						<img class="card-img-top img-thumbnail rounded-circle" src="{{asset('storage/'.$slider->image)}}" alt="Slider Image" / style="height: 100px; width:100px;">
					</span>
					<div class="card-body">
					    <h5 class="card-title">{{$slider->title}}</h5>
					    <p class="card-text">{{$slider->description}}</p>
					 </div>
				</a>
			</div>
		</div>
		@endforeach
	</div>
</div>