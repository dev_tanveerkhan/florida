<div class="deal-section common-section"><br>
	<h1 class="title">For The Joy of Florida</h1>
	<div id="scroll-mouse" class="common-section" style="background:#e3e9ef; padding: 20px; margin:20px 0px 20px 0px; ">
		<div class="owl-carousel owl-caro">
			@foreach($loveFlorida as $luvf)
			<div class="item card" style="height: 300px; padding-top: 15px;">
				<div class="caro-box">
					<a href="javascript:void(0)">
						<span>
							<img class="card-img-top img-thumbnail rounded-circle" src="{{asset('storage/'.$luvf->image)}}" alt="Slider Image" / style="height: 100px; width:100px;">
						</span>
						<div class="card-body">
						    <h5 class="card-title">{{$luvf->title}}</h5>
						    <p class="card-text">{{$luvf->description}}</p>
						 </div>
					</a>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>