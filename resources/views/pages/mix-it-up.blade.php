<div class="container-fluid" style="padding: 0;">
	<div class="mix-it-up-sec">
		<h2 class="title">Multi City!</h2>
		<p>Considering Florida as part of an epic holiday? You should, we make it easy by helping to connect the dots
			letting you see and do more on holiday.</p>
		<a href="{{route('showAllMixItUp')}}" class="btn btn-red">Check our favourite combos</a>
	</div>
</div>