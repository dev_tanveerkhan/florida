<!-- Get Quate Modal -->
<div class="modal fade" id="fcm-getQuote" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header p-0 pb-2 pr-3">
		<div class="fcm-title bg-color text-white p-3">
			<h5 class="modal-title" id="exampleModalLabel">Get a Quote</h5>
		</div>
        <button type="button" class="close p-3" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="h2">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- Form -->
        <form class="text-center" style="color: #757575;" method="get" id="enquiry-form" action="{{route('contactUs')}}">
            <!-- Name -->
            <input type="text" name="name" class="form-control mb-4" placeholder="Name">
		    <!-- Email -->
		    <input type="email" name="email" class="form-control mb-4" placeholder="E-mail">
		    <!-- Number -->
		    <input type="number" name="phone_number" class="form-control mb-4" placeholder="Phone Number">
            <!-- Sign in button -->
            <button class="btn text-white bg-btn-color btn-block rounded" type="submit">Enquire Now</button>
        </form>
        <!-- Form -->
      </div>
    </div>
  </div>
</div>