<!------------ WEBSITE MAIN SLIDER -------------->
	<div class="container">
		<!--Carousel Wrapper-->
		<div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
		  <!--Indicators-->
		  <ol class="carousel-indicators">
			@foreach($slider as $key=>$slidResult)
		  	<li data-target="#carousel-example-2" data-slide-to="{{$key}}"></li>
		  	@endforeach
		  </ol>
		  <!--/.Indicators-->
		  <!--Slides-->
		  <div class="carousel-inner" role="listbox">
		  		<?php $a=0; ?>
		  	  	@foreach($slider as $key=>$slidResult)
		  	  		@if($a==0)
				    <div class="carousel-item active">
				      <div class="view">
				        <img class="d-block w-100" src="{{asset('storage/'.$slidResult->image)}}" alt="slide" style="max-height: 400px;">
				        <div class="mask rgba-black-light"></div>
				      </div>
				      <div class="carousel-caption">
				        <h3 class="h3-responsive strong-title">{{$slidResult->title}}</h3>
				        <p>{{$slidResult->sub_title}}</p>
				        <p> Starting from<span>£{{$slidResult->start_price}}<sub> pp</sub></span></p><br>
				        <a href="{{route('moreDetails',$slidResult->hotel_id)}}" class="btn text-white bg-btn-color rounded">Discover Now</a>
				      </div>
				    </div>
				    {{ $a++ }}
				    @else
				    <div class="carousel-item">
				      <div class="view">
				        <img class="d-block w-100" src="{{asset('storage/'.$slidResult->image)}}" alt="slide" style="max-height: 400px;">
				        <div class="mask rgba-black-light"></div>
				      </div>
				      <div class="carousel-caption">
				        <h3 class="h3-responsive strong-title">{{$slidResult->title}}</h3>
				        <p>{{$slidResult->sub_title}}</p>
				        <p> Starting from<span>£{{$slidResult->start_price}}<sub> pp</sub></span></p><br>
				        <a href="{{route('moreDetails',$slidResult->hotel_id)}}" class="btn text-white bg-btn-color rounded">Discover Now</a>
				      </div>
				    </div>
				    @endif
			    @endforeach
			
		  </div>
		  <!--/.Slides-->
		  <!--Controls-->
		  <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		  <!--/.Controls-->
		</div>
		<!--/.Carousel Wrapper-->
	</div>