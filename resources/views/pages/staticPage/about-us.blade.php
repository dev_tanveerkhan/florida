	@extends('layouts.subPageBase')
	@section('title','About Us')
	@section('content')

	<div class="container aboutUs-section">
			<div class="view about-head-bgimg zoom">
		  		<img src="{{asset('front_component/image/contact-banner.jpg')}}" class="img-fluid" alt="placeholder">
		  		<div class="mask flex-center waves-effect waves-light">
		    		<p class="white-text h1 strong-title text-center">~For The Joy of Florida~</p>
		  		</div>
			</div>
			<!-- ABOUT US Text --><br><br>
			<div class="row about-text text-center p-4 brown-text">
				<h3 class="m-auto text-center strong-title">About Us</h3>
				Get ready to discover the best of Florida with us! We are determined to make sure you have the best holiday of your lives. As a team, our priority is to find a tailor made flight, hotel, car hire and attraction combination for you, that suits all your needs without costing a fortune – actually can be much more budget friendly than you have ever imagined. As our tagline suggests, we love everything about Florida. Whether it’s fun filled holidays to Walt Disney World, a value for money Villa holiday in Kissimmee or that super luxury holiday you have always wanted, we have it all in Florida Dreams.
			</div>
			<!-- SAFETY GUID --><br><br>
			<div class="row about-safty brown-text">
				<div class="col-md-6">
		  			<img class="img-fluid rounded-circle z-depth-2 mx-auto d-block m-4" alt="100x100" src="{{asset('img/icon_comment.png')}}">
					<div class="text-center strong-title h4">Our Commitment to you</div>
					<div class="text-center h6">We take great pride in our customer service, aiming to provide an outstanding quality of service, value for money and try hard to make each and every customer a lifelong one.</div>
				</div>
				<div class="col-md-6 p-2">
		  			<img class="img-fluid rounded-circle z-depth-2 mx-auto d-block m-4" alt="100x100" src="{{ asset('img/safehand.jpg') }}">
					<div class="text-center strong-title h4">In safe hands</div>
					<div class="text-center h6">Your safety is our number one priority.</div>
				</div>
			</div>
			<br><br>		
				
			<div class="row about-ad-det brown-text">
				<div class="text-center strong-title h4 m-auto">We get by with a lil help from our friends</div>
				<div class="text-center h6 m-auto">We work hard to hand pick the best value holidays from select partners and pass the savings to you.</div>
				<br><br>
				<div class="col-md-8">
					<div class="text-center strong-title h3 mb-4">For the Joy of Florida</div>
					@foreach($loveFlorida as $luvflo)
						<div class="row mb-5">
							<div class="col-3 col-md-2">
					  			<img class="img-fluid rounded-circle z-depth-2 mx-auto d-block" alt="100x100" src="{{ asset('storage/'.$luvflo->image) }}">
							</div>
							<div class="col-9 col-md-9">
								<div class="text-left strong-title h4">{{ $luvflo->title }}</div>
								<div class="text-left h6">{{$luvflo->description}}</div>
							</div>
						</div>
					@endforeach
				</div>
				<div class="col-md-4">
					<img src="{{ asset('img/beachimg.jpg') }}" style="width: 100%;height: 100%;">
				</div>
			</div>
	</div>
@endsection