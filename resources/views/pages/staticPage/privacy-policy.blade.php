@extends('layouts.subPageBase')
@section('title','Privacy Policy')
@section('content')
<div class="container terms-condition">
	<div class="view about-head-bgimg zoom">
  		<img src="{{asset('front_component/image/contact-banner.jpg')}}" class="img-fluid" alt="placeholder">
  		<div class="mask flex-center waves-effect waves-light">
    		<p class="white-text h1 strong-title text-center">~Privacy Policy~</p>
  		</div>
	</div>
	<div class="row">
		<div class="col-2 col-md-3"></div>
		<div class="col-8 col-md-6 text-center top-text-head">
			<div class="strong-title h3">~ Need Help ? ~</div>
		    <div class="h5"><span>Call {{ $contact->phone_number }}</span> or <a href="mailto:{{$contact->email}}">{{$contact->email}}</a></div>
		</div>
		<div class="col-2 col-md-3"></div>
	</div>
					
	<div class="row">
		<p class="text-center strong-title h5 m-auto grey-text"><a href="{{url('/')}}">Floridacalling.co.uk </a>is a trading website of florida dream holidays Ltd. </p>
	</div><br><br>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="false">
				@foreach($staticPage as $key=>$val)
					<div class="card mt-3">
						<div class="card-header" role="tab" id="headingTwo{{$key}}">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo{{$key}}" aria-expanded="false" aria-controls="collapseTwo{{$key}}">
								<h5 class="mb-0 text-color strong-title">{{ $val->title }}<i class="fas fa-angle-down rotate-icon"></i></h5>
							</a>
						</div>
						<div id="collapseTwo{{$key}}" class="collapse" role="tabpanel" aria-labelledby="headingTwo{{$key}}" data-parent="#accordionEx">
							<div class="card-body grey-text">
								{{ $val->description }}
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
</div>
@endsection


