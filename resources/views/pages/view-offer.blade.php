@extends('layouts.subPageBase')
@section('title','Offers')
@section('content')
<style>
/* The hero image */
.banner-hero-image {
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  opacity: 0.90;
  transition: 0.60s;
  padding: 0px 0px 10px 0px;
}
.banner-hero-image:hover {
  opacity: 0.7;
}

/* Place text in the middle of the image */
.banner-hero-text{
  text-align: center;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  color: white !important;
}
.banner-btn-css {
  border-radius: 8px;
  background-color: #f4511e;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 28px;
  padding: 10px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.banner-btn-css span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.banner-btn-css span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.banner-btn-css:hover span {
  padding-right: 25px;
}

.banner-btn-css:hover span:after {
  opacity: 1;
  right: 0;
}
</style>
<div class="content">
	<div class="container">
    <h1 class="title" style="text-transform: capitalize;"><i class="fa fa-thumb-tack" style="color: #dc3545;"></i> {{$name}} Offers</h1>
	  <div class="row">
      @foreach($offer as $offerResult)
      @if($name=='universal' && $offerResult->hotel->type==4)
	    <a href="#">
	    <div class="banner-hero-image col-sm-12 col-xl-12 col-md-12 col-lg-12">
	      <img class="img-fluid rounded" src="{{asset('storage/'.$offerResult->image)}}" style="min-height: 400px;max-height: 420px; width: 100%;">
	      <div class="banner-hero-text">
	      	<h2 style="color: #f2f2f2;"><b>{{ $offerResult->title }}</b></h2>
	        <h3 style="color: ghostwhite;">@if(!empty($offerResult->hotel)) {{ $offerResult->hotel->name }} for {{ $offerResult->hotel->days}} Days in €{{$offerResult->hotel->price}}@endif</h3>
	        <p style="color: lightgray;">{{ $offerResult->description }}</p>
	        <button class="banner-btn-css" onclick="window.location=''"><span>GO TO OFFER</span></button>
	      </div>
	    </div>
		</a>
    @elseif($name=='disney' && $offerResult->hotel->type==3)
    <a href="">
      <div class="banner-hero-image col-sm-12 col-xl-12 col-md-12 col-lg-12">
        <img class="img-fluid rounded" src="{{asset('storage/'.$offerResult->image)}}" style="min-height: 400px;max-height: 420px; width: 100%;">
        <div class="banner-hero-text">
          <h2 style="color: #f2f2f2;"><b>{{ $offerResult->title }}</b></h2>
          <h3 style="color: ghostwhite;">@if(!empty($offerResult->hotel)) {{ $offerResult->hotel->name }} for {{ $offerResult->hotel->days}} Days in €{{$offerResult->hotel->price}}@endif</h3>
          <p style="color: lightgray;">{{ $offerResult->description }}</p>
          <button class="banner-btn-css" onclick="window.location=''"><span>GO TO OFFER</span></button>
        </div>
      </div>
    </a>
    @endif
		@endforeach
	  </div>
	</div>
</div>
@endsection
