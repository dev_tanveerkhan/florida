<div class="explore-flo-list common-section">
	<h2 class="title">Top Regions</h2>
	<div class="row">
		@foreach($whereTOStay as $whereTo)
		<div class="col-lg-4 col-sm-6 col-6 same-hight">
			<div class="explore-flo-box">
				@foreach($whereToLoc as $werToS)
					@if($whereTo->title == $werToS->name)
						<a href="{{ route('viewAllFlorida',['id'=>$werToS->id,'name'=> preg_replace('/\s+/', '-',$werToS->name)]) }}">
					@endif
				@endforeach
					<div class="overlay centered">
						<h2>{{$whereTo->title}}</h2>
					</div>
					<img src="{{asset('storage/'.$whereTo->image)}}" alt="" />
				</a> <span class="explore-flo-price">from £{{$whereTo->price}} pp</span> </div>
		</div>
		@endforeach
	</div>
</div>
<!--end explore-flo-list-->