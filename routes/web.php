<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
	Route::get('/','FrontViewController@index');
	Route::get('hotels','FrontViewController@showAllHotels')->name('showAllHotels');
	Route::get('villas','FrontViewController@showAllVillas')->name('showAllVillas');
	Route::get('mix-it-up','FrontViewController@showAllMixItUp')->name('showAllMixItUp');
	Route::get('hot-deals','FrontViewController@showAllHotDeal')->name('showAllHotDeal');
	Route::get('parks-passes','FrontViewController@showAllParksTicket')->name('showAllParksTicket');
	Route::get('explore-florida','FrontViewController@showAllExploreFlorida')->name('showAllExploreFlorida');
	Route::get('universal-holidays','FrontViewController@showAllUniversalHoliday')->name('universalHoliday');
	Route::get('disney-holidays','FrontViewController@showAllDisneyHolidays')->name('disneyHoidays');
	Route::get('more-details/{hotel}','FrontViewController@moreDetails')->name('moreDetails');
	Route::get('mix-it-up-detail/{mixItUp}','FrontViewController@moreMixItDetail')->name('moreMixItDetail');
	Route::get('contact-us','FrontViewController@contactUs')->name('contactUs');
	Route::get('contact-us/{name}/{ref}','FrontViewController@contactUsRef')->name('contactUsRef');
	Route::get('get-a-quote','FrontViewController@getAquote')->name('getAquote');
	
	/*QUOTE*/
	Route::post('get-a-quote','QuoteController@storeAllQuateInfo')->name('storeAllQuateInfo');

	Route::get('florida/{locname}/{name}/{id}','FrontViewController@viewSubFloridaDeta')->name('viewSubFloridaDeta');
	Route::get('rest-usa/{name}/{id}','FrontViewController@viewAllUsa')->name('viewAllUsa');
	Route::get('rest-florida/{name}/{id}','FrontViewController@viewAllUsa')->name('viewAllFlorida');
	Route::get('disney-holiday/hotels/{name}/{id}','FrontViewController@disneyHolidaysHotels')->name('disneyHolidaysHotels');
	
	Route::get('hotels/{name}/{id}','FrontViewController@viewAllTypeHotels')->name('viewAllTypeHotels');
	Route::get('hot-deal/{name}/{id}','FrontViewController@viewAllTypeHotDeal')->name('viewAllTypeHotDeal');

	Route::get('about-us','FrontViewController@aboutUs')->name('aboutUs');
	Route::get('privacy-policy','FrontViewController@privacyPolicy')->name('privacyPolicy');
	Route::get('terms-conditions','FrontViewController@termsConditions')->name('termsConditions');

	/*RATING*/
	Route::get('search-rating/{star}/{for}','FrontViewController@searchRating')->name('searchRating');
	/*SEARCH LOCATION*/
	Route::get('search/{name}/{for}/{locId}','FrontViewController@searchLocation')->name('searchLocation');
	/*SEARCH PRICE*/
	Route::get('sort-price/{name}/{for}','FrontViewController@sortPrice')->name('sortPrice');

	/*PARKS AND TICKETS*/
	Route::get('parks-tickets/{name}','FrontViewController@parksTickets')->name('parksTickets');

	/*OTHERS OFFERS*/
	Route::get('other-offers/{name}','FrontViewController@otherOffers')->name('otherOffers');

	Auth::routes();
	
/*============ADMIN ROUTES============*/
Route::group(['middleware'=>'auth','prefix'=>'admin'],function(){
	Route::get('/', 'HomeController@index')->name('admin.home');
	/*MANAGE PASSWORD ROUTE*/
	Route::get('/manage-password', 'HomeController@managePassword')->name('admin.manage-password');
	Route::post('/password/update/{user}', 'HomeController@PasswordUpdate')->name('admin.passwordUpdate');
	/*CONTACT US ROUTE*/
	Route::get('/contact-info', 'ContactController@index')->name('admin.contactus');
	Route::post('/contact-us/update/{contact}', 'ContactController@update')->name('admin.contactUpdate');
	/*ADD LOCATION ROUTE*/
	Route::get('/location', 'LocationController@index')->name('admin.location');
	Route::get('/location/create', 'LocationController@create')->name('admin.create.location');
	Route::post('/location/create', 'LocationController@store');
	Route::get('/location/edit/{location}', 'LocationController@edit')->name('admin.location.edit');
	Route::post('/location/edit/{location}', 'LocationController@update');
	Route::get('/location/status/{location}', 'LocationController@status')->name('admin.location.status');
	Route::get('/location/destroy/{location}', 'LocationController@destroy')->name('admin.location.destroy');
	/*ADD SUB LOCATION*/
	Route::get('/location/sub', 'LocationController@subLovcationIndex')->name('admin.subLocation');
	Route::get('/location/sub/create', 'LocationController@subLovcationCreate')->name('admin.subLocation.create');
	Route::post('/location/sub/create', 'LocationController@subLovcationStore');
	Route::get('/location/sub/status/{subLoc}', 'LocationController@subLovcStatus')->name('admin.sublocation.status');
	Route::get('/location/sub/destroy/{subLoc}', 'LocationController@subLovcdestroy')->name('admin.sublocation.destroy');


	/*HOTELS*/
	Route::get('/hotels', 'HotelController@index')->name('admin.hotels');
	Route::get('/hotels/create', 'HotelController@create')->name('admin.hotel.create');	
	Route::post('/hotels/create', 'HotelController@store');	
	Route::get('/hotels/edit/{hotel}', 'HotelController@edit')->name('admin.hotel.edit');	
	Route::post('/hotels/edit/{hotel}', 'HotelController@update');	
	Route::get('/hotels/destroy/{hotel}', 'HotelController@destroy')->name('admin.hotel.destroy');	
	Route::get('/hotels/status/{hotel}', 'HotelController@status')->name('admin.hotel.status');	
	Route::get('/hotels/hot-deals/{hotel}', 'HotelController@hotDeals')->name('admin.hotel.hotDeals');	
	Route::get('/hotels/fav/{hotel}', 'HotelController@fav')->name('admin.hotel.fav');
	/*Route::get('/hotels/view-detail/{id}', 'HotelController@viewHotelDet')->name('admin.hotel.view');*/
	/*HOTEL DETAILS*/
	Route::post('/hotels/Singleimage', 'HotelController@singleImageDestroy')->name('singleImageDestroy');
	/* GET QUOTE */
	Route::get('/quote', 'QuoteController@index')->name('admin.quote');
	Route::get('/quote/destroy/{quote}', 'QuoteController@destroy')->name('admin.quote.destroy');
	Route::get('/quote/status/{quote}', 'QuoteController@status')->name('admin.quote.status');
	Route::get('/quote/view/{quote}', 'QuoteController@show')->name('admin.quote.viewAllData');
	/*CALL BACK*/
	Route::get('/call-back','ContactController@callBack')->name('admin.callBack');
	Route::get('/call-back/status/{callback}','ContactController@callBackStatus')->name('admin.callback.status');
	Route::get('/call-back/destroy/{callback}','ContactController@callBackDestroy')->name('admin.callback.destroy');
	/*FOLLOW ON*/
	Route::get('/follow-on','ContactController@followOn')->name('admin.follow-on');
	Route::post('/follow-on/update/{followOn}','ContactController@followOnUpdate')->name('admin.follow-on.update');
	/*SLIDER*/
	Route::get('/slider','SliderController@index')->name('admin.slider');
	Route::get('/slider/create','SliderController@create')->name('admin.slider.create');
	Route::post('/slider/create','SliderController@store');
	Route::get('/slider/edit/{slider}','SliderController@edit')->name('admin.slider.edit');
	Route::post('/slider/update/{slider}','SliderController@update')->name('admin.slider.update');
	Route::get('/slider/destroy/{slider}','SliderController@destroy')->name('admin.slider.destroy');
	Route::get('/slider/status/{slider}','SliderController@status')->name('admin.slider.status');
	/*FACIITY*/
	Route::get('/facility','FacilityController@index')->name('admin.facility');
	Route::get('/facility/create','FacilityController@create')->name('admin.facility.create');
	Route::post('/facility/create','FacilityController@store');
	Route::get('/facility/destroy/{facility}','FacilityController@destroy')->name('admin.facility.destroy');
	Route::get('/facility/status/{facility}','FacilityController@status')->name('admin.facility.status');
	/*FREE DISNEY DINING*/
	Route::get('/free-disney-dining','StaticContentController@freeDisneyDining')->name('admin.freeDisneyDining');
	Route::get('/free-disney-dining/edit/{freeddc}','StaticContentController@freeDisneyDiningEdit')->name('admin.freeDisneyDining.edit');
	Route::post('/free-disney-dining/edit/{freeddc}','StaticContentController@freeDisneyDiningUpdate');
	Route::get('/free-disney-dining/status/{freeddc}','StaticContentController@freeDisneyDiningStatus')->name('admin.freeDisneyDining.status');
	/*DISNEY RESORT*/
	Route::get('/disney-resort','DisneyResortController@index')->name('admin.disneyResort');
	Route::get('/disney-resort/create','DisneyResortController@create')->name('admin.disneyResort.create');
	Route::post('/disney-resort/create','DisneyResortController@store');
	Route::get('/disney-resort/edit/{disneyResort}','DisneyResortController@edit')->name('admin.disneyResort.edit');
	Route::post('/disney-resort/edit/{disneyResort}','DisneyResortController@update');
	Route::get('/disney-resort/destroy/{disneyResort}','DisneyResortController@destroy')->name('admin.disneyResort.destroy');
	Route::get('/disney-resort/status/{disneyResort}','DisneyResortController@status')->name('admin.disneyResort.status');
	/*BOOKING ADD*/
	Route::get('/book-ad','BookingAddController@edit')->name('admin.bookingAdd');
	Route::post('/book-ad/{book}','BookingAddController@update')->name('admin.bookingAdd.update');
	/*SLIDER INFO*/
	Route::get('/slider-info','SliderInfoController@index')->name('admin.sliderinfo');
	Route::get('/slider-info/create','SliderInfoController@create')->name('admin.sliderinfo.create');
	Route::post('/slider-info/create','SliderInfoController@store');
	Route::get('/slider-info/edit/{slider}','SliderInfoController@edit')->name('admin.sliderinfo.edit');
	Route::post('/slider-info/edit/{slider}','SliderInfoController@update');
	Route::get('/slider-info/destroy/{slider}','SliderInfoController@destroy')->name('admin.sliderinfo.destroy');
	Route::get('/slider-info/status/{slider}','SliderInfoController@status')->name('admin.sliderinfo.status');
	/*EXPLORE FLORIDA*/
	Route::get('/explore-florida','HotelController@exploreFlorida')->name('admin.exploreFlorida');
	Route::get('/explore-florida/create','HotelController@exploreFloridaCreate')->name('admin.exploreFlorida.create');
	Route::post('/explore-florida/create','HotelController@exploreFloridaStore');
	Route::get('/explore-florida/edit/{id}','HotelController@exploreFloridaEdit')->name('admin.exploreFlorida.edit');
	Route::post('/explore-florida/edit/{id}','HotelController@exploreFloridaUpdate');
	Route::get('/explore-florida/status/{id}','HotelController@exploreFloridaStatus')->name('admin.exploreFlorida.status');
	Route::get('/explore-florida/destroy/{explor}','HotelController@exploreFloridaDestroy')->name('admin.exploreFlorida.destroy');
	/*WHERE TO STAY */
	Route::get('/where-to-stay','HotelController@whereToStay')->name('admin.whereToStay');
	Route::get('/where-to-stay/create','HotelController@whereToStayCreate')->name('admin.whereToStay.create');
	Route::post('/where-to-stay/create','HotelController@whereToStayStore');
	Route::get('/where-to-stay/edit/{whereTo}','HotelController@whereToStayEdit')->name('admin.whereToStay.edit');
	Route::post('/where-to-stay/edit/{whereTo}','HotelController@whereToStayUpdate');
	Route::get('/where-to-stay/status/{whereToStay}','HotelController@whereToStayStatus')->name('admin.whereToStay.status');
	Route::get('/where-to-stay/destroy/{whereToStay}','HotelController@whereToStayDestroy')->name('admin.whereToStay.destroy');
	/*Parks And Passes*/
	Route::get('/parks-passes','ParkTicketController@index')->name('admin.parksPasses');
	Route::get('/parks-passes/create','ParkTicketController@create')->name('admin.parksPasses.create');
	Route::post('/parks-passes/create','ParkTicketController@store');
	Route::get('/parks-passes/edit/{passes}','ParkTicketController@edit')->name('admin.parksPasses.edit');
	Route::post('/parks-passes/edit/{passes}','ParkTicketController@update');
	Route::get('/parks-passes/status/{passes}','ParkTicketController@status')->name('admin.parksPasses.status');
	Route::get('/parks-passes/destroy/{passes}','ParkTicketController@destroy')->name('admin.parksPasses.destroy');
	
	/*STATIC PAGES*/
	Route::get('static-pages','StaticPagesController@index')->name('admin.staticPages');
	Route::get('static-pages/create','StaticPagesController@create')->name('admin.staticPages.create');
	Route::post('static-pages/create','StaticPagesController@store');
	Route::get('static-pages/edit/{page}','StaticPagesController@edit')->name('admin.staticPages.edit');
	Route::post('static-pages/edit/{page}','StaticPagesController@update');
	Route::get('static-pages/destroy/{page}','StaticPagesController@destroy')->name('admin.staticPages.destroy');
	Route::get('static-pages/status/{page}','StaticPagesController@status')->name('admin.staticPages.status');

	/*Mix It Up*/
	Route::get('mix-it-up','MixItUpController@index')->name('admin.mixItUp');
	Route::get('mix-it-up/create','MixItUpController@create')->name('admin.mixItUp.create');
	Route::post('mix-it-up/create','MixItUpController@store');
	Route::get('mix-it-up/edit/{mixItUp}','MixItUpController@edit')->name('admin.mixItUp.edit');
	Route::post('mix-it-up/edit/{mixItUp}','MixItUpController@update');
	Route::get('mix-it-up/destroy/{mixItUp}','MixItUpController@destroy')->name('admin.mixItUp.destroy');
	Route::get('mix-it-up/status/{mixItUp}','MixItUpController@status')->name('admin.mixItUp.status');

	/*OFFERS*/
	Route::get('offers','OfferController@index')->name('admin.offer');
	Route::get('offers/create','OfferController@create')->name('admin.offer.create');
	Route::post('offers/create','OfferController@store');
	Route::get('offers/edit/{offer}','OfferController@edit')->name('admin.offer.edit');
	Route::post('offers/edit/{offer}','OfferController@update');
	Route::get('offers/destroy/{offer}','OfferController@destroy')->name('admin.offer.destroy');
	Route::get('offers/status/{offer}','OfferController@status')->name('admin.offer.status');

});
